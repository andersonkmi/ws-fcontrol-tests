package com.fcontrol.data;

public enum WSFControlResponseCode {
	
	TRANSACAO_ENFILEIRADA_SUCESSO(0, "Transação enfileirada com sucesso"),
	TRANSACAO_ANALISADA_SUCESSO(0, "Transacão analisada com sucesso."),
    FALHA_INSTRUCAO_INSERIR_TRANSACAO(11, "Falha em instrução ocorreu ao inserir a transação."),
	TRANSACAO_JA_ENFILEIRADO(8, "Esta transação já foi enfileirada"),
	CEP_COMPRADOR_INVALIDO(109, "109|O campo CEP do comprador é inválido.Verifique se o mesmo contém no máximo (10) caracteres.O campo deve ser numérico.Este campo é obrigatório.Valor do parâmetro"),
	PAIS_COMPRADOR_INVALIDO(110, "O campo País do comprador é inválido.Este campo é obrigatório.Valor do parâmetro"),
	DDD_TELEFONE_COMPRADOR_INVALIDO(111, "O campo DDD do telefone do comprador é inválido.Este campo é obrigatório.Verifique se o mesmo contém no máximo (3) caracteres."),
	TELEFONE_COMPRADOR_INVALIDO(112, "O campo Telefone do comprador é inválido.Este campo é obrigatório.Verifique se o mesmo contém no máximo (9) caracteres."),
	DDD_CELULAR_INVALIDO(113, "O campo DDD do celular do comprador é inválido.Verifique se o mesmo contém no máximo (3) caracteres."),
	CELULAR_COMPRADOR_INVALIDO(114, "O campo Celular do comprador é inválido.Verifique se o mesmo contém no máximo (9) caracteres."),
    EMAIL_COMPRADOR_INVALIDO(115, "O campo Email do comprador é inválido.Verifique se o mesmo contém no máximo (255) caracteres.Informe o caracter * quando não houver e-mail."),
	CNPJ_CPF_INVALIDO(116, "O campo CPF ou CNPJ do Comprador é inválido"),
	QUANTIDADE_ITENS_DISTINTOS_INVALIDO(131, "O campo Quantidade de itens distintos é inválido.Verifique se o valor é maior que 0 (zero).Este campo é obrigatório."),
	VALOR_PEDIDO_INVALIDO(132, "O campo Valor do pedido é inválido.Verifique se o valor do pedido é maior que 100 (cem). O valor do pedido deve ser o valor em Reais do pedido  * 100). Exemplo R$122,99 = 12299.Este campo é obrigatório."),
	QUANTIDADE_TOTAL_ITENS_INVALIDO(133, "O campo Quantidade total de itens é inválido.Verifique se o valor é maior que 0 (zero).Este campo é obrigatório."),
	METODO_PAGAMENTO_INVALIDO(134, "O campo Método de pagamento é inválido.O campo deve ser numérico.Este campo é obrigatório.Valor do parâmetro (Nulo)."),
	VALOR_FRETE_INVALIDO(137, "O campo Valor frete é inválido.Verifique se o valor é maior ou igual a 0 (zero)."),
	PRAZO_ENTREGA_INVALIDO(139, "O campo Prazo entrega é inválido.Verifique se o valor é maior ou igual a 0 (zero)."),
	QUANTIDADE_PRODUTO_INVALIDO(150, "O campo Quantidade produto é inválido.Verifique se o valor é maior que 0 (zero)."),
	DATA_COMPRA_INVALIDA(153, "O campo Data Compra é inválido.Formatos válidos (dd/mm/aaaa hh:mm:ss ou dd/mm/aaaa). A data informada deverá ser maior que 01/01/1753 e menor que a data atual + 1 dia.Este campo é obrigatório."),
	VALOR_UNITARIO_PRODUTO_INVALIDO(151, "O campo Valor produto é inválido.Verifique se o valor é maior ou igual a 0 (zero)."),
	USUARIO_SENHA_INVALIDOS(154, "Usuário ou senha inválido(s)."),
	DDD_TELEFONE2_INVALIDO(169, "O campo DDD do telefone comprador 2 é inválido.Verifique se o mesmo contém no máximo (3) caracteres."),
	TELEFONE2_INVALIDO(170, "O campo Telefone comprador 2 é inválido.Verifique se o mesmo contém no máximo (9) caracteres."),
	METODO_ENFILEIRAMENTO_INVALIDO(242, "Método de enfileiramento inválido para o seu tipo de usuário. Entre em contato com suporte@fcontrol.com.br"),
	FALHA_TEMPORARIA(989, "Ocorreu uma falha temporária de execução. Tente novamente.");
	
	private int responseCode;
	private String description;

	private WSFControlResponseCode(int responseCode, String description) {
		this.responseCode = responseCode;
		this.description = description;
	}
	
	public int getResponseCode() {
		return responseCode;
	}
	
	public String getStringResponseCode() {
		return String.format("%d|", responseCode);
	}
	
	public String getDescription() {
		return description;
	}
}
