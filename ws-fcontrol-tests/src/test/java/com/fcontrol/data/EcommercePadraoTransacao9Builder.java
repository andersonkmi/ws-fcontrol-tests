package com.fcontrol.data;

import com.fcontrol.ArrayOfWsPagamento2;
import com.fcontrol.ArrayOfWsProduto3;
import com.fcontrol.MetodoPagamento;
import com.fcontrol.WsCartao;
import com.fcontrol.WsComprador2;
import com.fcontrol.WsEndereco;
import com.fcontrol.WsEntrega3;
import com.fcontrol.WsExtra;
import com.fcontrol.WsPagamento2;
import com.fcontrol.WsProduto3;
import com.fcontrol.WsTransacao9;
import com.fcontrol.WsUsuario;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import static com.fcontrol.MetodoPagamento.*;
import static com.fcontrol.Status.PENDENTE;
import static java.util.Calendar.*;

@Component
public class EcommercePadraoTransacao9Builder extends BaseEcommerceTransactionBuilder {
    private static final Logger logger = Logger.getLogger(EcommercePadraoTransacao9Builder.class);

    @Resource
    private AddressBuilder addressBuilder;

    @Resource
    private BuyerBuilder buyerBuilder;

    @Resource
    private DeliveryInformationBuilder deliveryInformationBuilder;

    @Resource
    private ExtraInformationBuilder extraInformationBuilder;

    @Resource
    private AuthenticationBuilder authenticationBuilder;

    @Resource
    private ProductBuilder productBuilder;

    @Resource
    private PaymentBuilder paymentBuilder;

    @Resource
    private CreditCardBuilder creditCardBuilder;


	public WsTransacao9 buildTransacao(MetodoPagamento method) {
		WsTransacao9 transaction = new WsTransacao9();
		configureBasicInformation(transaction);
		configureBuyer(transaction);
		configureDelivery(transaction);
		configureExtra(transaction);
		configureAuthentication(transaction);
        configureProduct(transaction);
        configurePayment(transaction, method);
		return transaction;
	}

	private void configureBasicInformation(WsTransacao9 transaction) {
		transaction.setCanalVenda("API");
		transaction.setCodigoIntegrador(0);
		transaction.setCodigoPedido(generateRandomOrderCode());
		transaction.setCodigoPedido2(generateRandomOrderCode());
		transaction.setFormaEntrega("SEDEX");
		transaction.setObservacao("Standard ecommerce order");
		transaction.setPedidoDeTeste(false);
		transaction.setPrazoEntregaDias(10);
		transaction.setQuantidadeItensDistintos(1);
		transaction.setQuantidadeTotalItens(1);
		transaction.setStatusFinalizador(PENDENTE);
		transaction.setValorTotalCompra(15534);
		transaction.setValorTotalFrete(1000);
		try {
			DatatypeFactory factory = DatatypeFactory.newInstance();
			GregorianCalendar transactionCalendar = new GregorianCalendar();
			transactionCalendar.set(YEAR, 2014);
			transactionCalendar.set(MONTH, DECEMBER);
			transactionCalendar.set(DATE, 20);
			transaction.setDataEntrega(factory.newXMLGregorianCalendar(transactionCalendar));
			
			GregorianCalendar now = new GregorianCalendar();
			transaction.setDataCompra(factory.newXMLGregorianCalendar(now));
		} catch (DatatypeConfigurationException exception) {
			logger.error(String.format("Exception raised when converting dates: '%s'", exception.getMessage()), exception);
		}
	}
	
	private void configureBuyer(WsTransacao9 transaction) {
        GregorianCalendar dob = new GregorianCalendar();
        dob.set(DATE, 10);
        dob.set(MONTH, MARCH);
        dob.set(YEAR, 1977);
        dob.set(HOUR, 0);
        dob.set(MINUTE, 0);
        dob.set(SECOND, 0);
        dob.set(MILLISECOND, 0);

        GregorianCalendar signupDate = new GregorianCalendar();
        signupDate.set(DATE, 20);
        signupDate.set(MONTH, MAY);
        signupDate.set(YEAR, 2013);
        signupDate.set(HOUR, 0);
        signupDate.set(MINUTE, 0);
        signupDate.set(SECOND, 0);
        signupDate.set(MILLISECOND, 0);

		WsComprador2 buyer = this.buyerBuilder.build("Comprador Teste Unitário",
                                                     "buyer@fcontrol.com",
                                                     "senha",
                                                     "68726345455",
                                                      "M",
                                                      "code",
                                                      "127.0.0.1",
                                                      "11",
                                                      "965653232",
                                                      "11",
                                                      "38488700",
                                                      "11",
                                                      "23232323",
                                                      dob,
                                                      signupDate);

		WsEndereco buyerAddress = addressBuilder.buildWsEndereco("Avenida Paulista", "1776", "10o. andar", "São Paulo", "Bela Vista", "SP", "Brasil", "01310200");
		buyer.setEndereco(buyerAddress);
		transaction.setDadosComprador(buyer);
	}
	
	private void configureDelivery(WsTransacao9 transaction) {
        GregorianCalendar dob = new GregorianCalendar();
        dob.set(DATE, 10);
        dob.set(MONTH, MARCH);
        dob.set(YEAR, 1977);
        dob.set(HOUR, 0);
        dob.set(MINUTE, 0);
        dob.set(SECOND, 0);
        dob.set(MILLISECOND, 0);

        WsEntrega3 delivery = deliveryInformationBuilder.build("Teste entrega",
                "delivery@fcontrol.com",
                dob,
                "68726345455",
                "11",
                "958585858",
                "11",
                "23242526",
                "11",
                "23566532",
                "M");
		WsEndereco deliveryAddress = addressBuilder.buildWsEndereco("Avenida Paulista", "1776", "10o. andar", "São Paulo", "Bela Vista", "SP", "Brasil", "01310200");
		delivery.setEndereco(deliveryAddress);
		transaction.setDadosEntrega(delivery);

	}
	
	private void configureExtra(WsTransacao9 transaction) {
		WsExtra extra = extraInformationBuilder.build("Extra 1", "Extra 2", "Extra 3", "Extra 4");
		transaction.setDadosExtra(extra);
	}
	
	private void configureAuthentication(WsTransacao9 transaction) {		
		WsUsuario user = authenticationBuilder.buildWsUsuario(getUserName(), getPassword(), getIdSubLoja());
		transaction.setDadosUsuario(user);		
	}

    private void configureProduct(WsTransacao9 transaction) {
        WsProduto3 produto = productBuilder.buildWsProduto3("prod1425", "Produto de teste", "Livro", 1, 3456, false, false);
        List<WsProduto3> products = new ArrayList<WsProduto3>();
        products.add(produto);

        ArrayOfWsProduto3 productArray = productBuilder.buildWsProduto3Array(products);
        transaction.setProdutos(productArray);
    }

    private void configurePayment(WsTransacao9 transaction, MetodoPagamento method) {
        if(method == CARTAO_CREDITO ||
           method == CARTAO_AMERICAN_EXPRESS ||
           method == CARTAO_AURA ||
           method == CARTAO_DINERS ||
           method == CARTAO_VISA ||
           method == CARTAO_MASTER_CARD ||
           method == CARTAO_ELO ||
           method == CARTAO_EXTRA ||
           method == CARTAO_HIPER_CARD ||
           method == CARTAO_PAO_ACUCAR ||
           method == CARTAO_MARISA ||
           method == CARTAO_PONTO_FRIO ||
           method == CARTAO_SENDAS ||
           method == CARTAO_SORO_CRED) {
            WsCartao card = creditCardBuilder.build("539058", "", "", "", "5390583280099112", "Teste FControl", "", "68726345455", "02/2015", "11", "38488700", "9112");
            WsPagamento2 payment = paymentBuilder.build(1, 12545, method, 123456, card);
            List<WsPagamento2> items = new ArrayList<WsPagamento2>();
            items.add(payment);
            ArrayOfWsPagamento2 elements = paymentBuilder.build(items);
            transaction.setPagamentos(elements);
        } else {
            WsPagamento2 payment = paymentBuilder.build(1, 12545, method);
            List<WsPagamento2> items = new ArrayList<WsPagamento2>();
            items.add(payment);
            ArrayOfWsPagamento2 elements = paymentBuilder.build(items);
            transaction.setPagamentos(elements);
        }
    }
}
