package com.fcontrol.data;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fcontrol.ArrayOfWsProdutoRessarcidoTroca;
import com.fcontrol.WsProdutoRessarcidoTroca;

@Component
public class RefundedProductBuilder {
	public WsProdutoRessarcidoTroca build(boolean modified, String salesChannel, String description, String line, int quantity, double amount, double unitAmount) {
		WsProdutoRessarcidoTroca refundedProduct = new WsProdutoRessarcidoTroca();
		refundedProduct.setAlterado(modified);
		refundedProduct.setCodigoVenda(salesChannel);
		refundedProduct.setDescricao(description);
		refundedProduct.setLinha(line);
		refundedProduct.setQuantidade(quantity);
		refundedProduct.setValorTotal(amount);
		refundedProduct.setValorUnitario(unitAmount);
		return refundedProduct;
	}
	
	public ArrayOfWsProdutoRessarcidoTroca build(List<WsProdutoRessarcidoTroca> products) {
		ArrayOfWsProdutoRessarcidoTroca items = new ArrayOfWsProdutoRessarcidoTroca();
		for(WsProdutoRessarcidoTroca product : products) {
			items.getWsProdutoRessarcidoTroca().add(product);
		}
		return items;
	}
}
