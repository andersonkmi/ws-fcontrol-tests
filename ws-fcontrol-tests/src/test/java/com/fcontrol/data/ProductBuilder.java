package com.fcontrol.data;

import com.fcontrol.ArrayOfWsProduto3;
import com.fcontrol.ArrayOfWsProduto4;
import com.fcontrol.ArrayOfWsProdutoTroca;
import com.fcontrol.WsProduto3;
import com.fcontrol.WsProduto4;
import com.fcontrol.WsProdutoTroca;
import org.springframework.stereotype.Component;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.GregorianCalendar;
import java.util.List;

@Component
class ProductBuilder {
    public WsProduto3 buildWsProduto3(String codigo, String descricao, String categoria, int quantidade, int valorUnitario, boolean isWeddingList, boolean isGift) {
        WsProduto3 product = new WsProduto3();
        product.setCodigo(codigo);
        product.setDescricao(descricao);
        product.setCategoria(categoria);
        product.setParaPresente(isGift);
        product.setListaDeCasamento(isWeddingList);
        product.setQuantidade(quantidade);
        product.setValorUnitario(valorUnitario);
        return product;
    }

    public ArrayOfWsProduto3 buildWsProduto3Array(List<WsProduto3> products) {
        ArrayOfWsProduto3 productArray = new ArrayOfWsProduto3();
        for(WsProduto3 product : products) {
            productArray.getWsProduto3().add(product);
        }
        return productArray;
    }

    public WsProduto4 buildWsProduto4(String codigo, String descricao, String categoria, int quantidade, int valorUnitario, boolean isWeddingList, boolean isGift, String salesPersonName, String mktPlaceTransactionCode) {
        WsProduto4 product = new WsProduto4();
        product.setCodigo(codigo);
        product.setDescricao(descricao);
        product.setCategoria(categoria);
        product.setParaPresente(isGift);
        product.setListaDeCasamento(isWeddingList);
        product.setQuantidade(quantidade);
        product.setValorUnitario(valorUnitario);
        product.setNomeVendedorMarketPlace(salesPersonName);
        product.setCodigoTransacaoMarketPlace(mktPlaceTransactionCode);
        return product;
    }

    public ArrayOfWsProduto4 buildWsProduto4Array(List<WsProduto4> products) {
        ArrayOfWsProduto4 productArray = new ArrayOfWsProduto4();
        for(WsProduto4 product : products) {
            productArray.getWsProduto4().add(product);
        }
        return productArray;
    }

    public WsProdutoTroca buildWsProdutoTroca(boolean alterado,
                                              String codigo,
                                              String condicaoUso,
                                              GregorianCalendar dataValidadeLote,
                                              boolean descontinuado,
                                              String descricao,
                                              String destino,
                                              String grupoReclamacao,
                                              String linha,
                                              String lote,
                                              boolean loteComDefeito,
                                              String motivoLoteEmBranco,
                                              boolean precoNaoPadrao,
                                              int quantidadeReclamada,
                                              int quantidadeRessarcir,
                                              String reclamacao,
                                              boolean ressarcir,
                                              String situacao) {
        WsProdutoTroca produtoTroca = new WsProdutoTroca();
        produtoTroca.setAlterado(alterado);
        produtoTroca.setCodigo(codigo);
        produtoTroca.setCondicaoUso(condicaoUso);
        if(dataValidadeLote != null) {
            try {
                DatatypeFactory factory = DatatypeFactory.newInstance();
                produtoTroca.setDataValidadeLote(factory.newXMLGregorianCalendar(dataValidadeLote));
            } catch (DatatypeConfigurationException exception) {}
        }

        produtoTroca.setDescontinuado(descontinuado);
        produtoTroca.setDescricao(descricao);
        produtoTroca.setDestino(destino);
        produtoTroca.setGrupoReclamacao(grupoReclamacao);
        produtoTroca.setLinha(linha);
        produtoTroca.setLote(lote);
        produtoTroca.setLoteComDefeito(loteComDefeito);
        produtoTroca.setMotivoLoteEmBranco(motivoLoteEmBranco);
        produtoTroca.setPrecoNaoPadrao(precoNaoPadrao);
        produtoTroca.setQuantidadeReclamada(quantidadeReclamada);
        produtoTroca.setQuantidadeRessarcir(quantidadeRessarcir);
        produtoTroca.setReclamacao(reclamacao);
        produtoTroca.setRessarcir(ressarcir);
        produtoTroca.setSituacao(situacao);
        return produtoTroca;
    }

    public ArrayOfWsProdutoTroca buildWsProdutoTrocaArray(List<WsProdutoTroca> products) {
        ArrayOfWsProdutoTroca items = new ArrayOfWsProdutoTroca();
        for(WsProdutoTroca product : products) {
            items.getWsProdutoTroca().add(product);
        }
        return items;
    }
}
