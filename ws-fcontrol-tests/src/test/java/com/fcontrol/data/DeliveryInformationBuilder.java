package com.fcontrol.data;

import com.fcontrol.WsEntrega3;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.GregorianCalendar;

@Component
class DeliveryInformationBuilder {
    private static final Logger logger = Logger.getLogger(DeliveryInformationBuilder.class);

    public WsEntrega3 build(String nomeRecebedor, String emailRecebedor, GregorianCalendar dataNascimento, String documento, String dddCelular, String numeroCelular, String dddFone1, String numeroFone1, String dddFone2, String numeroFone2, String sexo) {
        WsEntrega3 delivery = new WsEntrega3();
        delivery.setSexo(sexo);
        delivery.setNomeEntrega(nomeRecebedor);
        delivery.setEmail(emailRecebedor);
        if(dataNascimento != null) {
            try {
                DatatypeFactory factory = DatatypeFactory.newInstance();
                delivery.setDataNascimento(factory.newXMLGregorianCalendar(dataNascimento));
            } catch (DatatypeConfigurationException exception) {
                logger.error(String.format("Error when setting up the DOB: '%s'", exception.getMessage()), exception);
            }
        }
        delivery.setCpfCnpj(documento);
        delivery.setDddCelular(dddCelular);
        delivery.setNumeroCelular(numeroCelular);
        delivery.setDddTelefone(dddFone1);
        delivery.setNumeroTelefone(numeroFone1);
        delivery.setDddTelefone2(dddFone2);
        delivery.setNumeroTelefone2(numeroFone2);
        return delivery;
    }
}
