package com.fcontrol.data;

import com.fcontrol.WsUsuario;
import com.fcontrol.WsUsuarioTroca;
import org.springframework.stereotype.Component;

@Component
class AuthenticationBuilder {

    public WsUsuario buildWsUsuario(String user, String password, String identificadorSubLoja) {
        WsUsuario usuario = new WsUsuario();
        usuario.setLogin(user);
        usuario.setSenha(password);
        usuario.setIdentificadorLojaFilho(identificadorSubLoja);
        return usuario;
    }

    public WsUsuario buildWsUsuario(String user, String password) {
        WsUsuario usuario = new WsUsuario();
        usuario.setLogin(user);
        usuario.setSenha(password);
        return usuario;
    }
    
    public WsUsuarioTroca buildWsUsuarioTroca(String userName, String password) {
    	WsUsuarioTroca usuario = new WsUsuarioTroca();
    	usuario.setLogin(userName);
    	usuario.setSenha(password);
    	return usuario;
    }
}
