package com.fcontrol.data;

public enum WSFControlTrocaResponseCode {
	
	TIPO_MANIFESTACAO_INVALIDO(1012, "Campo (TipoManifestacao) é inválido, preenchimento é obrigatório. Campo de tipo String, tamanho máximo 100 caracteres."),
	RESSARCIMENTO_TIPO_INVALIDO(1082, "Campo (Ressarcimento.Tipo) é inválido, Preenchimento é obrigatório (Indique 'T'=Troca ou 'R'=Reembolso)");
		
	private int responseCode;
	private String description;

	private WSFControlTrocaResponseCode(int responseCode, String description) {
		this.responseCode = responseCode;
		this.description = description;
	}
	
	public int getResponseCode() {
		return responseCode;
	}

	public String getDescription() {
		return description;
	}
}
