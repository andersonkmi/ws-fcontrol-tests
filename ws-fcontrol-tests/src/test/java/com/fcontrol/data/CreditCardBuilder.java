package com.fcontrol.data;

import org.springframework.stereotype.Component;

import com.fcontrol.WsCartao;

@Component
class CreditCardBuilder {
    public WsCartao build(String bin, String binBanco, String binPais, String binBandeira, String numeroCartao, String titularCartao, String nomeBancoEmissor, String cpfTitularCartao, String dataValidadeCartao, String ddd, String telefone, String digitosFinais) {
        WsCartao card = new WsCartao();
        card.setBin(bin);
        card.setBinBanco(binBanco);
        card.setBinBandeira(binBandeira);
        card.setBinPais(binPais);
        card.setNumeroCartao(numeroCartao);
        card.setNomeTitularCartao(titularCartao);
        card.setNomeBancoEmissor(nomeBancoEmissor);
        card.setCpfTitularCartao(cpfTitularCartao);
        card.setDataValidadeCartao(dataValidadeCartao);
        card.setDddTelefone2(ddd);
        card.setNumeroTelefone2(telefone);
        card.setQuatroUltimosDigitosCartao(digitosFinais);
        return card;
    }
}
