package com.fcontrol.data;

import com.fcontrol.ArrayOfWsPagamento2;
import com.fcontrol.MetodoPagamento;
import com.fcontrol.WsCartao;
import com.fcontrol.WsPagamento2;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by andersonkmi on 24/08/2014.
 */
@Component
class PaymentBuilder {
    public WsPagamento2 build(int numeroParcelas, int valor, MetodoPagamento method) {
        WsPagamento2 payment = new WsPagamento2();
        payment.setNumeroParcelas(numeroParcelas);
        payment.setValor(valor);
        payment.setMetodoPagamento(method);
        return payment;
    }

    public WsPagamento2 build(int numeroParcelas, int valor, MetodoPagamento method, long nsu, WsCartao cartao) {
        WsPagamento2 payment = new WsPagamento2();
        payment.setNumeroParcelas(numeroParcelas);
        payment.setValor(valor);
        payment.setMetodoPagamento(method);
        payment.setNsu(nsu);
        payment.setCartao(cartao);
        return payment;
    }

    public ArrayOfWsPagamento2 build(List<WsPagamento2> items) {
        ArrayOfWsPagamento2 payments = new ArrayOfWsPagamento2();
        for(WsPagamento2 item : items) {
            payments.getWsPagamento2().add(item);
        }
        return payments;
    }
}
