package com.fcontrol.data;

import com.fcontrol.WsEnderecoTroca;
import org.springframework.stereotype.Component;

import com.fcontrol.WsEndereco;

@Component
class AddressBuilder {
    public WsEndereco buildWsEndereco(String endereco, String numero, String complemento, String cidade, String bairro, String estado, String pais, String cep) {
        WsEndereco address = new WsEndereco();
        address.setBairro(bairro);
        address.setCep(cep);
        address.setCidade(cidade);
        address.setComplemento(complemento);
        address.setEstado(estado);
        address.setNumero(numero);
        address.setPais(pais);
        address.setRua(endereco);
        return address;
    }

    public WsEnderecoTroca buildWsEnderecoTroca(String bairro,
                                                String cep,
                                                String cidade,
                                                String complemento,
                                                String estado,
                                                String numero,
                                                String pais,
                                                String referencia,
                                                String rua) {
        WsEnderecoTroca addr = new WsEnderecoTroca();
        addr.setBairro(bairro);
        addr.setCep(cep);
        addr.setCidade(cidade);
        addr.setComplemento(complemento);
        addr.setEstado(estado);
        addr.setNumero(numero);
        addr.setPais(pais);
        addr.setReferencia(referencia);
        addr.setRua(rua);
        return addr;
    }
}
