package com.fcontrol.data;

import com.fcontrol.ArrayOfWsPagador;
import com.fcontrol.WsEndereco;
import com.fcontrol.WsPagador;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.GregorianCalendar;
import java.util.List;

@Component
public class SponsorBuilder {
    private static final Logger logger = Logger.getLogger(SponsorBuilder.class);

    @Resource
    private AddressBuilder addressBuilder;

    public WsPagador build(String name, String email, String taxpayerId, String mobileAreaCode, String mobilePhone, String areaCode, String phoneNumber, GregorianCalendar registrationDate) {
        WsPagador sponsor = new WsPagador();
        sponsor.setCpfCnpj(taxpayerId);
        sponsor.setNomePagador(name);
        sponsor.setEmail(email);
        sponsor.setDddCelular(mobileAreaCode);
        sponsor.setNumeroCelular(mobilePhone);
        sponsor.setDddTelefone(areaCode);
        sponsor.setNumeroTelefone(phoneNumber);

        WsEndereco sponsorAddress = addressBuilder.buildWsEndereco("Avenida Paulista", "1776", "10o. andar", "São Paulo", "Bela Vista", "SP", "Brasil", "01310200");
        sponsor.setEndereco(sponsorAddress);
        try {
            DatatypeFactory factory = DatatypeFactory.newInstance();
            sponsor.setDataCadastro(factory.newXMLGregorianCalendar(registrationDate));
        } catch (DatatypeConfigurationException exception) {
            logger.error(String.format("Exception raised when converting dates: '%s'", exception.getMessage()), exception);
        }
        return sponsor;
    }

    public ArrayOfWsPagador buildSponsors(List<WsPagador> items) {
        ArrayOfWsPagador sponsors = new ArrayOfWsPagador();
        sponsors.getWsPagador().addAll(items);
        return sponsors;
    }
}
