package com.fcontrol.data;

import com.fcontrol.WsPessoaTroca;
import org.springframework.stereotype.Component;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.GregorianCalendar;

@Component
class PessoaTrocaBuilder {
	
	public WsPessoaTroca build(String codigo,
                               String cpf,
                               String email,
                               GregorianCalendar dob,
                               String emailPrincipal,
                               String estadoCivil,
                               String nome,
                               String nomeEmpresa,
                               String orgaoEmissor,
                               String rg,
                               String sexo,
                               String tipoPessoa,
                               String tipoPublico,
                               boolean cpfIgualConsultor,
                               boolean enderecoIgualConsultor,
                               boolean existeCadastroFuncionario) {
		WsPessoaTroca pessoa = new WsPessoaTroca();
        pessoa.setCodigo(codigo);
        pessoa.setCPF(cpf);
        pessoa.setCPFIgualConsultor(cpfIgualConsultor);
        pessoa.setEmail(email);
        if(dob != null) {
            try {
                DatatypeFactory factory = DatatypeFactory.newInstance();
                pessoa.setDataNascimento(factory.newXMLGregorianCalendar(dob));
            } catch (DatatypeConfigurationException exception) {
                // No action performed
            }
        }
        pessoa.setEmailPrincipal(emailPrincipal);
        pessoa.setEnderecoIgualConsultor(enderecoIgualConsultor);
        pessoa.setEstadoCivil(estadoCivil);
        pessoa.setExisteCadastroFuncionario(existeCadastroFuncionario);
        pessoa.setNome(nome);
        pessoa.setNomeEmpresa(nomeEmpresa);
        pessoa.setOrgaoEmissor(orgaoEmissor);
        pessoa.setRG(rg);
        pessoa.setSexo(sexo);
        pessoa.setTipoPessoa(tipoPessoa);
        pessoa.setTipoPublico(tipoPublico);
		return pessoa;
	}
}
