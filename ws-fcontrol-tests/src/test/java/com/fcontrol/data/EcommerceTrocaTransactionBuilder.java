package com.fcontrol.data;

import static java.util.Calendar.DATE;
import static java.util.Calendar.DECEMBER;
import static java.util.Calendar.HOUR;
import static java.util.Calendar.JANUARY;
import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.springframework.stereotype.Component;

import com.fcontrol.ArrayOfWsProdutoRessarcidoTroca;
import com.fcontrol.ArrayOfWsProdutoTroca;
import com.fcontrol.ArrayOfWsTelefoneTroca;
import com.fcontrol.WsContaBancariaTroca;
import com.fcontrol.WsEnderecoTroca;
import com.fcontrol.WsFuncionarioTroca;
import com.fcontrol.WsPessoaTroca;
import com.fcontrol.WsProdutoRessarcidoTroca;
import com.fcontrol.WsProdutoTroca;
import com.fcontrol.WsRessarcimentoTroca;
import com.fcontrol.WsTelefoneTroca;
import com.fcontrol.WsTransacaoTroca;
import com.fcontrol.WsUsuarioTroca;

@Component
public class EcommerceTrocaTransactionBuilder extends BaseEcommerceTransactionBuilder {

    @Resource
	private AuthenticationBuilder authenticationBuilder;

    @Resource
    private PessoaTrocaBuilder pessoaTrocaBuilder;

    @Resource
    private BankingAccountBuilder bankingAccountBuilder;

    @Resource
    private ReplacementEmployeeBuilder replacementEmployeeBuilder;

    @Resource
    private ReplacementTelephoneBuilder replacementTelephoneBuilder;

    @Resource
    private AddressBuilder addressBuilder;    
    
    @Resource
    private ProductBuilder productBuilder;
    
    @Resource
    private ReplacementRefundBuilder replacementRefundBuilder;
    
    @Resource
    private RefundedProductBuilder refundedProductBuilder;

	public WsTransacaoTroca build() {
		WsTransacaoTroca transacao = new WsTransacaoTroca();
		configureAuthentication(transacao);
		configureBasicInformation(transacao);
		configurePerson(transacao);
		configureProduct(transacao);
		configureRefund(transacao);
		return transacao;
	}
	
	private void configureAuthentication(final WsTransacaoTroca transacao) {
		WsUsuarioTroca usuario = authenticationBuilder.buildWsUsuarioTroca(getUserName(), getPassword());
		transacao.setUsuario(usuario);
	}
	
	private void configureBasicInformation(final WsTransacaoTroca transacao) {
		transacao.setEmHomologacao(false);
		transacao.setExigeColetaDestino(false);
		transacao.setGrupoManifestacao("Vivo");
		transacao.setLinha("Perfumes");
		transacao.setNumeroChamado(Calendar.getInstance().getTimeInMillis());
		transacao.setProdutoAssunto("Teste unitário troca");
		transacao.setReAnalise(false);
		transacao.setResponsavelAbertura("Anderson Ito");
		transacao.setTipoManifestacao("Troca");
		transacao.setManifestacao("TesteFControl");
		
		try {
			DatatypeFactory factory = DatatypeFactory.newInstance();
			GregorianCalendar transactionCalendar = new GregorianCalendar();
			transactionCalendar.set(2014, DECEMBER, 15, 10, 10, 10);
			
			transacao.setDataAbertura(factory.newXMLGregorianCalendar(transactionCalendar));
			transacao.setDataConclusao(factory.newXMLGregorianCalendar(transactionCalendar));
			transacao.setDataPrevista(factory.newXMLGregorianCalendar(transactionCalendar));
		} catch (DatatypeConfigurationException exception) {
			// No action performed
		}
	}
	
	private void configurePerson(final WsTransacaoTroca transacao) {
		GregorianCalendar dob = new GregorianCalendar();
		dob.set(YEAR, 1983);
		dob.set(MONTH, 1);
		dob.set(DATE, 10);
		dob.set(HOUR, 0);
		dob.set(MINUTE, 0);
		dob.set(SECOND, 0);
		dob.set(MILLISECOND, 0);
		WsPessoaTroca pessoa = this.pessoaTrocaBuilder.build("1425", 
									                      "73786123241", 
									                      "teste@fcontrol.com", 
									                      dob, 
									                      "teste@fcontrol.com", 
									                      "casado", 
									                      "Teste Troca", 
									                      "Empresa Troca", 
									                      "SSP", 
									                      "25362514", 
									                      "M", 
									                      "F", 
									                      "publico", 
									                      true, 
									                      true, 
									                      true);
		

		configureBankingAccount(pessoa);
		configureEmployee(pessoa);
		configureTelephones(pessoa);
        configureAddress(pessoa);
		transacao.setPessoa(pessoa);
	}
	
	private void configureBankingAccount(WsPessoaTroca person) {
		WsContaBancariaTroca conta = this.bankingAccountBuilder.buildWsContaBancariaTroca(399, 
				"25365", 
				"122", 
				"73786123241", 
				"Teste Troca", 
				"Teste Troca", 
				"251425147");
		person.setContaBancaria(conta);		
	}
	
	private void configureTelephones(WsPessoaTroca person) {
        WsTelefoneTroca phone = replacementTelephoneBuilder.buildWsTelefoneTroca("38488700", "11", "residencial");
        List<WsTelefoneTroca> phoneList = new ArrayList<WsTelefoneTroca>();
        phoneList.add(phone);
        ArrayOfWsTelefoneTroca phones = replacementTelephoneBuilder.buildwsTelefoneTrocaArray(phoneList);
        person.setTelefones(phones);
		
	}
	
	private void configureEmployee(WsPessoaTroca person) {
        WsEnderecoTroca addr = addressBuilder.buildWsEnderecoTroca("Bela Vista", "01310200", "São Paulo", "", "SP", "1776", "Brasil", "", "Avenida Paulista");
        WsFuncionarioTroca employee = this.replacementEmployeeBuilder.buildWsFuncionarioTroca("73786123241", addr, true);
        person.setFuncionario(employee);		
	}
	
	private void configureAddress(WsPessoaTroca person) {
		WsEnderecoTroca addr = addressBuilder.buildWsEnderecoTroca("Bela Vista", "01310200", "São Paulo", "", "SP", "1776", "Brasil", "", "Avenida Paulista");
		person.setEndereco(addr);
	}
	
	private void configureProduct(final WsTransacaoTroca transacao) {
		GregorianCalendar dataValidadeLote = new GregorianCalendar();
		dataValidadeLote.set(YEAR, 2015);
		dataValidadeLote.set(MONTH, JANUARY);
		dataValidadeLote.set(DATE, 10);
		dataValidadeLote.set(HOUR, 0);
		dataValidadeLote.set(MINUTE, 0);
		dataValidadeLote.set(SECOND, 0);
		dataValidadeLote.set(MILLISECOND, 0);
		
		WsProdutoTroca product = productBuilder.buildWsProdutoTroca(false, "05841", "Danificado", dataValidadeLote, false, "Perfume Kaiak Aventura", "Troca", "Produto defeituoso", "Perfumes", "65568", false, "", false, 1, 1, "A embalagem chegou quebrada", false, "Quebrado");
		List<WsProdutoTroca> items = new ArrayList<WsProdutoTroca>();
		items.add(product);
		
		ArrayOfWsProdutoTroca products = productBuilder.buildWsProdutoTrocaArray(items);
		transacao.setProdutosReclamados(products);
	}
	
	private void configureRefund(final WsTransacaoTroca transacao) {
		WsEnderecoTroca addr = addressBuilder.buildWsEnderecoTroca("Bela Vista", "01310200", "São Paulo", "", "SP", "1776", "Brasil", "", "Avenida Paulista");
		WsContaBancariaTroca conta = this.bankingAccountBuilder.buildWsContaBancariaTroca(399, 
				"25365", 
				"122", 
				"73786123241", 
				"Teste Troca", 
				"Teste Troca", 
				"251425147");
		
		WsProdutoRessarcidoTroca refundedProduct = refundedProductBuilder.build(false, "web", "descricao", "Perfumes",  1, 100, 100);
		List<WsProdutoRessarcidoTroca> products = new ArrayList<WsProdutoRessarcidoTroca>();
		products.add(refundedProduct);		
		ArrayOfWsProdutoRessarcidoTroca refundedProducts = refundedProductBuilder.build(products);
		
		WsRessarcimentoTroca refund = replacementRefundBuilder.build(false, "Depósito em conta", "Troca", 100, true, addr, conta, refundedProducts);
		transacao.setRessarcimento(refund);
	}
}
