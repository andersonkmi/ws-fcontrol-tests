package com.fcontrol.data;

import org.springframework.stereotype.Component;

import com.fcontrol.ArrayOfWsProdutoRessarcidoTroca;
import com.fcontrol.WsContaBancariaTroca;
import com.fcontrol.WsEnderecoTroca;
import com.fcontrol.WsRessarcimentoTroca;

@Component
class ReplacementRefundBuilder {
	public WsRessarcimentoTroca build(boolean enderecoTrocaAlterado, String refundMethod, String type, double refundAmount, boolean refundAmountModified, WsEnderecoTroca address, WsContaBancariaTroca bankingAccount, ArrayOfWsProdutoRessarcidoTroca refundedProducts) {
		WsRessarcimentoTroca refund = new WsRessarcimentoTroca();
		refund.setEnderecoTrocaAlterado(false);
		refund.setFormaReembolso("Depósito bancário");
		refund.setTipo("T");
		refund.setValorReembolso(100);
		refund.setValorReembolsoAlterado(true);
		refund.setEnderecoTroca(address);		
		refund.setDadosReembolso(bankingAccount);
		refund.setProdutosRessarcidos(refundedProducts);
		return refund;
	}
}
