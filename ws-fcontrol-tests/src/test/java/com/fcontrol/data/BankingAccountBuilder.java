package com.fcontrol.data;

import com.fcontrol.WsContaBancariaTroca;
import org.springframework.stereotype.Component;

@Component
class BankingAccountBuilder {

    public WsContaBancariaTroca buildWsContaBancariaTroca(int codigoBanco,
                                                          String numeroConta,
                                                          String codigoAgencia,
                                                          String cpf,
                                                          String nome,
                                                          String nomeTitular,
                                                          String rg) {
        WsContaBancariaTroca acct = new WsContaBancariaTroca();
        acct.setCodigoBanco(codigoBanco);
        acct.setCodigoAgencia(codigoAgencia);
        acct.setNumero(numeroConta);
        acct.setCPFTitular(cpf);
        acct.setNomeBanco(nome);
        acct.setNomeTitular(nomeTitular);
        acct.setRGTitular(rg);
        return acct;
    }
}
