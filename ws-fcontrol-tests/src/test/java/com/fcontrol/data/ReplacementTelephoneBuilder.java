package com.fcontrol.data;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fcontrol.ArrayOfWsTelefoneTroca;
import com.fcontrol.WsTelefoneTroca;

@Component
class ReplacementTelephoneBuilder {
    public WsTelefoneTroca buildWsTelefoneTroca(String numero, String ddd, String tipo) {
        WsTelefoneTroca telefone = new WsTelefoneTroca();
        telefone.setNumero(numero);
        telefone.setDdd(ddd);
        telefone.setTipo(tipo);
        return telefone;
    }
    
    public ArrayOfWsTelefoneTroca buildwsTelefoneTrocaArray(List<WsTelefoneTroca> items) {
    	ArrayOfWsTelefoneTroca elements = new ArrayOfWsTelefoneTroca();
    	for(WsTelefoneTroca item : items) {
    		elements.getWsTelefoneTroca().add(item);    		
    	}
    	return elements;
    }
}
