package com.fcontrol.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BaseEcommerceTransactionBuilder {
	private String userName;
	private String password;
	private String idSubLoja;

	protected String getUserName() {
		return userName;
	}
	
	protected String getPassword() {
		return password;
	}

	protected String getIdSubLoja() {
		return idSubLoja;
	}

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public void setIdSubLoja(String id) {
		idSubLoja = id;
	}

	protected String generateRandomOrderCode() {
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
		return String.format("std-order-%s", formatter.format(Calendar.getInstance().getTime()));
	}
}
