package com.fcontrol.data;

import com.fcontrol.WsExtra;
import org.springframework.stereotype.Component;

/**
 * Created by andersonkmi on 24/08/2014.
 */
@Component
class ExtraInformationBuilder {

    public WsExtra build(String extra1, String extra2, String extra3, String extra4) {
        WsExtra extra = new WsExtra();
        extra.setExtra1(extra1);
        extra.setExtra2(extra2);
        extra.setExtra3(extra3);
        extra.setExtra4(extra4);
        return extra;
    }
}
