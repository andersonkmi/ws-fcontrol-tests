package com.fcontrol.data;

import com.fcontrol.WsEnderecoTroca;
import com.fcontrol.WsFuncionarioTroca;
import org.springframework.stereotype.Component;

@Component
class ReplacementEmployeeBuilder {
    public WsFuncionarioTroca buildWsFuncionarioTroca(String cpf, WsEnderecoTroca endereco, boolean naturaEps) {
        WsFuncionarioTroca employee = new WsFuncionarioTroca();
        employee.setCPF(cpf);
        employee.setEndereco(endereco);
        employee.setNaturaEPS(naturaEps);
        return employee;
    }
}
