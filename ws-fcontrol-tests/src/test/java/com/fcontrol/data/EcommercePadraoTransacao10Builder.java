package com.fcontrol.data;

import static com.fcontrol.MetodoPagamento.CARTAO_AMERICAN_EXPRESS;
import static com.fcontrol.MetodoPagamento.CARTAO_AURA;
import static com.fcontrol.MetodoPagamento.CARTAO_CREDITO;
import static com.fcontrol.MetodoPagamento.CARTAO_DINERS;
import static com.fcontrol.MetodoPagamento.CARTAO_ELO;
import static com.fcontrol.MetodoPagamento.CARTAO_EXTRA;
import static com.fcontrol.MetodoPagamento.CARTAO_HIPER_CARD;
import static com.fcontrol.MetodoPagamento.CARTAO_MARISA;
import static com.fcontrol.MetodoPagamento.CARTAO_MASTER_CARD;
import static com.fcontrol.MetodoPagamento.CARTAO_PAO_ACUCAR;
import static com.fcontrol.MetodoPagamento.CARTAO_PONTO_FRIO;
import static com.fcontrol.MetodoPagamento.CARTAO_SENDAS;
import static com.fcontrol.MetodoPagamento.CARTAO_SORO_CRED;
import static com.fcontrol.MetodoPagamento.CARTAO_VISA;
import static com.fcontrol.Status.PENDENTE;
import static java.util.Calendar.DATE;
import static java.util.Calendar.DECEMBER;
import static java.util.Calendar.HOUR;
import static java.util.Calendar.MARCH;
import static java.util.Calendar.MAY;
import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fcontrol.ArrayOfWsPagamento2;
import com.fcontrol.ArrayOfWsProduto4;
import com.fcontrol.MetodoPagamento;
import com.fcontrol.WsCartao;
import com.fcontrol.WsComprador2;
import com.fcontrol.WsEndereco;
import com.fcontrol.WsEntrega3;
import com.fcontrol.WsExtra;
import com.fcontrol.WsPagamento2;
import com.fcontrol.WsProduto4;
import com.fcontrol.WsTransacao10;
import com.fcontrol.WsUsuario;

@Component
public class EcommercePadraoTransacao10Builder extends BaseEcommerceTransactionBuilder {
    private static final Logger logger = Logger.getLogger(EcommercePadraoTransacao10Builder.class);

    @Resource
    private AddressBuilder addressBuilder;

    @Resource
    private BuyerBuilder buyerBuilder;

    @Resource
    private DeliveryInformationBuilder deliveryInformationBuilder;

    @Resource
    private ExtraInformationBuilder extraInformationBuilder;

    @Resource
    private AuthenticationBuilder authenticationBuilder;

    @Resource
    private ProductBuilder productBuilder;

    @Resource
    private PaymentBuilder paymentBuilder;

    @Resource
    private CreditCardBuilder creditCardBuilder;


    public WsTransacao10 buildTransacao(MetodoPagamento method) {
        WsTransacao10 transaction = new WsTransacao10();
        configureBasicInformation(transaction);
        configureBuyer(transaction);
        configureDelivery(transaction);
        configureExtra(transaction);
        configureAuthentication(transaction);
        configureProduct(transaction);
        configurePayment(transaction, method);
        return transaction;
    }

    private void configureBasicInformation(WsTransacao10 transaction) {
        transaction.setCanalVenda("API");
        transaction.setCodigoIntegrador(0);
        transaction.setCodigoPedido(generateRandomOrderCode());
        transaction.setCodigoPedido2(generateRandomOrderCode());
        transaction.setFormaEntrega("SEDEX");
        transaction.setObservacao("Standard ecommerce order");
        transaction.setPedidoDeTeste(false);
        transaction.setPrazoEntregaDias(10);
        transaction.setQuantidadeItensDistintos(1);
        transaction.setQuantidadeTotalItens(1);
        transaction.setStatusFinalizador(PENDENTE);
        transaction.setValorTotalCompra(15534);
        transaction.setValorTotalFrete(1000);
        transaction.setIsTransacaoMarketPlace(true);
        
        try {
            DatatypeFactory factory = DatatypeFactory.newInstance();
            GregorianCalendar transactionCalendar = new GregorianCalendar();
            transactionCalendar.set(YEAR, 2014);
            transactionCalendar.set(MONTH, DECEMBER);
            transactionCalendar.set(DATE, 20);
            transaction.setDataEntrega(factory.newXMLGregorianCalendar(transactionCalendar));

            GregorianCalendar now = new GregorianCalendar();
            transaction.setDataCompra(factory.newXMLGregorianCalendar(now));
        } catch (DatatypeConfigurationException exception) {
            logger.error(String.format("Exception raised when converting dates: '%s'", exception.getMessage()), exception);
        }
    }

    private void configureBuyer(WsTransacao10 transaction) {
        GregorianCalendar dob = new GregorianCalendar();
        dob.set(DATE, 10);
        dob.set(MONTH, MARCH);
        dob.set(YEAR, 1977);
        dob.set(HOUR, 0);
        dob.set(MINUTE, 0);
        dob.set(SECOND, 0);
        dob.set(MILLISECOND, 0);

        GregorianCalendar signupDate = new GregorianCalendar();
        signupDate.set(DATE, 20);
        signupDate.set(MONTH, MAY);
        signupDate.set(YEAR, 2013);
        signupDate.set(HOUR, 0);
        signupDate.set(MINUTE, 0);
        signupDate.set(SECOND, 0);
        signupDate.set(MILLISECOND, 0);

        WsComprador2 buyer = this.buyerBuilder.build("Comprador Teste Unitário",
                "buyer@fcontrol.com",
                "senha",
                "68726345455",
                "M",
                "code",
                "127.0.0.1",
                "11",
                "965653232",
                "11",
                "38488700",
                "11",
                "23232323",
                dob,
                signupDate);

        WsEndereco buyerAddress = addressBuilder.buildWsEndereco("Avenida Paulista", "1776", "10o. andar", "São Paulo", "Bela Vista", "SP", "Brasil", "01310200");
        buyer.setEndereco(buyerAddress);
        transaction.setDadosComprador(buyer);
    }

    private void configureDelivery(WsTransacao10 transaction) {
        GregorianCalendar dob = new GregorianCalendar();
        dob.set(DATE, 10);
        dob.set(MONTH, MARCH);
        dob.set(YEAR, 1977);
        dob.set(HOUR, 0);
        dob.set(MINUTE, 0);
        dob.set(SECOND, 0);
        dob.set(MILLISECOND, 0);

        WsEntrega3 delivery = deliveryInformationBuilder.build("Teste entrega",
                "delivery@fcontrol.com",
                dob,
                "68726345455",
                "11",
                "958585858",
                "11",
                "23242526",
                "11",
                "23566532",
                "M");
        WsEndereco deliveryAddress = addressBuilder.buildWsEndereco("Avenida Paulista", "1776", "10o. andar", "São Paulo", "Bela Vista", "SP", "Brasil", "01310200");
        delivery.setEndereco(deliveryAddress);
        transaction.setDadosEntrega(delivery);

    }

    private void configureExtra(WsTransacao10 transaction) {
        WsExtra extra = extraInformationBuilder.build("Extra 1", "Extra 2", "Extra 3", "Extra 4");
        transaction.setDadosExtra(extra);
    }

    private void configureAuthentication(WsTransacao10 transaction) {
        WsUsuario user = authenticationBuilder.buildWsUsuario(getUserName(), getPassword(), getIdSubLoja());
        transaction.setDadosUsuario(user);
    }

    private void configureProduct(WsTransacao10 transaction) {
        WsProduto4 produto = productBuilder.buildWsProduto4("prod1425", "Produto de teste", "Livro", 1, 3456, false, false, "Mktplace Sales", "101010");
        List<WsProduto4> products = new ArrayList<>();
        products.add(produto);

        ArrayOfWsProduto4 productArray = productBuilder.buildWsProduto4Array(products);
        transaction.setProdutos(productArray);
    }

    private void configurePayment(WsTransacao10 transaction, MetodoPagamento method) {
        if(method == CARTAO_CREDITO ||
                method == CARTAO_AMERICAN_EXPRESS ||
                method == CARTAO_AURA ||
                method == CARTAO_DINERS ||
                method == CARTAO_VISA ||
                method == CARTAO_MASTER_CARD ||
                method == CARTAO_ELO ||
                method == CARTAO_EXTRA ||
                method == CARTAO_HIPER_CARD ||
                method == CARTAO_PAO_ACUCAR ||
                method == CARTAO_MARISA ||
                method == CARTAO_PONTO_FRIO ||
                method == CARTAO_SENDAS ||
                method == CARTAO_SORO_CRED) {
            WsCartao card = creditCardBuilder.build("539058", "", "", "", "5390583280099112", "Teste FControl", "", "68726345455", "02/2015", "11", "38488700", "9112");
            WsPagamento2 payment = paymentBuilder.build(1, 12545, method, 123456, card);
            List<WsPagamento2> items = new ArrayList<WsPagamento2>();
            items.add(payment);
            ArrayOfWsPagamento2 elements = paymentBuilder.build(items);
            transaction.setPagamentos(elements);
        } else {
            WsPagamento2 payment = paymentBuilder.build(1, 12545, method);
            List<WsPagamento2> items = new ArrayList<WsPagamento2>();
            items.add(payment);
            ArrayOfWsPagamento2 elements = paymentBuilder.build(items);
            transaction.setPagamentos(elements);
        }
    }

}
