package com.fcontrol.data;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fcontrol.WsComprador2;

@Component
class BuyerBuilder {
    private static final Logger logger = Logger.getLogger(BuyerBuilder.class);

    public WsComprador2 build(String nome, String email, String senha, String documento, String sexo, String codigo, String enderecoIp, String dddCelular, String numeroCelular, String dddFone1, String numeroFone1, String dddFone2, String numeroFone2, GregorianCalendar dataNascimento, GregorianCalendar dataCadastro) {
        WsComprador2 comprador2 = new WsComprador2();
        comprador2.setSenha(senha);
        comprador2.setCodigo(codigo);

        if(dataCadastro != null) {
            try {
                DatatypeFactory factory = DatatypeFactory.newInstance();
                comprador2.setDataCadastro(factory.newXMLGregorianCalendar(dataCadastro));
            } catch (DatatypeConfigurationException exception) {
                logger.error(String.format("Error when setting up the registration date: '%s'", exception.getMessage()), exception);
            }
        }

        if(dataNascimento != null) {
            try {
                DatatypeFactory factory = DatatypeFactory.newInstance();
                comprador2.setDataNascimento(factory.newXMLGregorianCalendar(dataNascimento));
            } catch (DatatypeConfigurationException exception) {
                logger.error(String.format("Error when setting up the DOB date: '%s'", exception.getMessage()), exception);
            }
        }

        comprador2.setCpfCnpj(documento);
        comprador2.setDddCelular(dddCelular);
        comprador2.setDddTelefone(dddFone1);
        comprador2.setDddTelefone2(dddFone2);
        comprador2.setEmail(email);
        comprador2.setIP(enderecoIp);
        comprador2.setNomeComprador(nome);
        comprador2.setNumeroCelular(numeroCelular);
        comprador2.setNumeroTelefone(numeroFone1);
        comprador2.setNumeroTelefone2(numeroFone2);
        comprador2.setSexo(sexo);
        return comprador2;
    }
}
