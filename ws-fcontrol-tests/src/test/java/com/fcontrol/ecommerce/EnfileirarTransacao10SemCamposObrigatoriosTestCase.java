package com.fcontrol.ecommerce;

import static com.fcontrol.MetodoPagamento.BOLETO_BANCARIO;
import static com.fcontrol.MetodoPagamento.CARTAO_MASTER_CARD;
import static com.fcontrol.data.WSFControlResponseCode.CELULAR_COMPRADOR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.CEP_COMPRADOR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.CNPJ_CPF_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.DATA_COMPRA_INVALIDA;
import static com.fcontrol.data.WSFControlResponseCode.DDD_CELULAR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.DDD_TELEFONE2_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.DDD_TELEFONE_COMPRADOR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.METODO_PAGAMENTO_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.PAIS_COMPRADOR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.PRAZO_ENTREGA_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.QUANTIDADE_ITENS_DISTINTOS_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.QUANTIDADE_PRODUTO_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.QUANTIDADE_TOTAL_ITENS_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.TELEFONE2_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.TELEFONE_COMPRADOR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.TRANSACAO_ENFILEIRADA_SUCESSO;
import static com.fcontrol.data.WSFControlResponseCode.TRANSACAO_JA_ENFILEIRADO;
import static com.fcontrol.data.WSFControlResponseCode.USUARIO_SENHA_INVALIDOS;
import static com.fcontrol.data.WSFControlResponseCode.VALOR_FRETE_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.VALOR_PEDIDO_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.VALOR_UNITARIO_PRODUTO_INVALIDO;
import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fcontrol.WsResultado;
import com.fcontrol.WsTransacao10;
import com.fcontrol.data.EcommercePadraoTransacao10Builder;
import com.fcontrol.data.WSFControlResponseCode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:**/applicationContext.xml"})
public class EnfileirarTransacao10SemCamposObrigatoriosTestCase extends BaseSoapWebServiceTestCase {
	private static final Logger logger = Logger.getLogger(EnfileirarTransacao10SemCamposObrigatoriosTestCase.class);

    @Resource
    private EcommercePadraoTransacao10Builder builder;
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
        builder.setUserName("fc-ut-enfileirar10");
        builder.setPassword("142557");
	}

	@Test
	public void enfileirarTransacao() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacao.");
		}
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }
	
	@Test
	public void enfileirarTransacaoCpfInvalido() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCpfInvalido.");
		}
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getDadosComprador().setCpfCnpj("25233688457");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CNPJ_CPF_INVALIDO.getStringResponseCode())).and(containsString(CNPJ_CPF_INVALIDO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCpfBranco() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCpfBranco.");
		}
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getDadosComprador().setCpfCnpj("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }
	
	@Test
	public void enfileirarTransacaoCpfNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCpfNulo.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getDadosComprador().setCpfCnpj(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }
	

	@Test
	public void enfileirarTransacaoCanalVendaVazio() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCanalVendaVazio.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setCanalVenda("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCanalVendaNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCanalVendaNulo.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setCanalVenda(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCodigoPedido2Nulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCodigoPedido2Nulo.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setCodigoPedido2(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoCodigoPedido2Branco() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCodigoPedido2Branco.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setCodigoPedido2("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoDadosCompradorNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosCompradorNulo.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setDadosComprador(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoDadosEntregaNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosEntregaNulo.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setDadosEntrega(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoDadosExtraNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosExtraNulo.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setDadosExtra(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	
	@Test
	public void enfileirarTransacaoPedidoRepetido() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoPedidoRepetido.");
		}
		
        WsTransacao10 transacao1 = builder.buildTransacao(BOLETO_BANCARIO);
        WsResultado response = soapService.enfileirarTransacao10(transacao1);
        assertThat(response.isSucesso(), equalTo(true));
        
        WsTransacao10 transacao2 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao2.setCodigoPedido(transacao1.getCodigoPedido());
        transacao2.setCodigoPedido2(transacao1.getCodigoPedido2());
        
        WsResultado response2 = soapService.enfileirarTransacao10(transacao2);
        assertThat(response2.isSucesso(), equalTo(false));
        assertThat(response2.getMensagem(), both(containsString(TRANSACAO_JA_ENFILEIRADO.getStringResponseCode())).and(containsString(TRANSACAO_JA_ENFILEIRADO.getDescription())));
	}	

	@Test
	public void enfileirarTransacaoAutenticacaoNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoNulo.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setDadosUsuario(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoAutenticacaoSenhaInvalida() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoSenhaInvalida.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getDadosUsuario().setSenha("bling");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoAutenticacaoUsuarioInexistente() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoUsuarioInexistente.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getDadosUsuario().setLogin("_lojademo_");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoAutenticacaoUsuarioInvalido() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoUsuarioInvalido.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getDadosUsuario().setLogin("lojademotroca");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoAutenticacaoUsuarioBranco() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoUsuarioBranco.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getDadosUsuario().setLogin("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoAutenticacaoSenhaBranco() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoSenhaBranco.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getDadosUsuario().setSenha("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}	

	@Test
	public void enfileirarTransacaoDadosPagamentoNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosPagamentoNulo.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setPagamentos(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(METODO_PAGAMENTO_INVALIDO.getStringResponseCode())).and(containsString(METODO_PAGAMENTO_INVALIDO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoDadosPagamentoVazio() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosPagamentoVazio.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getPagamentos().getWsPagamento2().clear();
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(METODO_PAGAMENTO_INVALIDO.getStringResponseCode())).and(containsString(METODO_PAGAMENTO_INVALIDO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoDadosPagamentoNumeroParcelasZero() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosPagamentoNumeroParcelasZero.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getPagamentos().getWsPagamento2().get(0).setNumeroParcelas(0);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoDadosPagamentoValorZero() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosPagamentoValorZero.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getPagamentos().getWsPagamento2().get(0).setValor(0);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}			
	
	@Test
	public void enfileirarTransacaoDadosProdutoNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosProdutoNulo.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setProdutos(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoDadosProdutoVazio() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosProdutoVazio.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getProdutos().getWsProduto4().clear();
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoDadosProdutoCodigoNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosProdutoCodigoNulo.");
		}
		
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getProdutos().getWsProduto4().get(0).setCodigo(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}			
	
	@Test
	public void enfileirarTransacaoDadosProdutoCategoriaBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoCategoriaBranco.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getProdutos().getWsProduto4().get(0).setCategoria("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}				
	
	@Test
	public void enfileirarTransacaoDadosProdutoCategoriaNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoCategoriaNula.");
        }
        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getProdutos().getWsProduto4().get(0).setCategoria(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}				
	
	@Test
	public void enfileirarTransacaoDadosProdutoDescricaoBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoDescricaoBranco.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getProdutos().getWsProduto4().get(0).setDescricao("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}				
	
	@Test
	public void enfileirarTransacaoDadosProdutoDescricaoNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoDescricaoNula.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getProdutos().getWsProduto4().get(0).setDescricao(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}				
	
	@Test
	public void enfileirarTransacaoDadosProdutoQuantidadeZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoQuantidadeZero.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getProdutos().getWsProduto4().get(0).setQuantidade(0);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}					
	
	@Test
	public void enfileirarTransacaoDadosProdutoQuantidadeNegativa() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoQuantidadeNegativa.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getProdutos().getWsProduto4().get(0).setQuantidade(-1);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(QUANTIDADE_PRODUTO_INVALIDO.getStringResponseCode())).and(containsString(QUANTIDADE_PRODUTO_INVALIDO.getDescription())));
	}						
	
	@Test
	public void enfileirarTransacaoDadosProdutoValorUnitarioZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoValorUnitarioZero.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getProdutos().getWsProduto4().get(0).setValorUnitario(0);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}						
	
	@Test
	public void enfileirarTransacaoDadosProdutoValorUnitarioNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoValorUnitarioNegativo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.getProdutos().getWsProduto4().get(0).setValorUnitario(-1);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(VALOR_UNITARIO_PRODUTO_INVALIDO.getStringResponseCode())).and(containsString(VALOR_UNITARIO_PRODUTO_INVALIDO.getDescription())));
	}						
	
	@Test
	public void enfileirarTransacaoDataCompraNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDataCompraNula.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setDataCompra(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DATA_COMPRA_INVALIDA.getStringResponseCode())).and(containsString(DATA_COMPRA_INVALIDA.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoDataEntregaNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDataEntregaNula.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setDataEntrega(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoFormaEntregaBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoFormaEntregaBranco.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setFormaEntrega("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoFormaEntregaNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoFormaEntregaNula.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setFormaEntrega(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoPrazoEntregaZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPrazoEntregaZero.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setPrazoEntregaDias(0);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoPrazoEntregaNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPrazoEntregaNegativo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setPrazoEntregaDias(-2);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(PRAZO_ENTREGA_INVALIDO.getStringResponseCode())).and(containsString(PRAZO_ENTREGA_INVALIDO.getDescription())));
	}			
	
	@Test
	public void enfileirarTransacaoQuantidadeItensDistintosZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoQuantidadeItensDistintosZero.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setQuantidadeItensDistintos(0);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	

	@Test
	public void enfileirarTransacaoQuantidadeItensDistintosNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoQuantidadeItensDistintosNegativos.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setQuantidadeItensDistintos(-1);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(QUANTIDADE_ITENS_DISTINTOS_INVALIDO.getStringResponseCode())).and(containsString(QUANTIDADE_ITENS_DISTINTOS_INVALIDO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoQuantidadeTotalItensZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoQuantidadeTotalItensZero.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setQuantidadeTotalItens(0);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoQuantidadeTotalItensNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoQuantidadeTotalItensNegativo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setQuantidadeTotalItens(-1);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(QUANTIDADE_TOTAL_ITENS_INVALIDO.getStringResponseCode())).and(containsString(QUANTIDADE_TOTAL_ITENS_INVALIDO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoValorTotalCompraZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoValorTotalCompraZero.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setValorTotalCompra(0);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoValorTotalCompraNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoValorTotalCompraNegativo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setValorTotalCompra(-1);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(VALOR_PEDIDO_INVALIDO.getStringResponseCode())).and(containsString(VALOR_PEDIDO_INVALIDO.getDescription())));
	}
	
	
	@Test
	public void enfileirarTransacaoValorTotalFreteZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoValorTotalFreteZero.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setValorTotalFrete(0);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoValorTotalFreteNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoValorTotalFreteNegativo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao10.setValorTotalFrete(-1);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(VALOR_FRETE_INVALIDO.getStringResponseCode())).and(containsString(VALOR_FRETE_INVALIDO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoOk() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoOk.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNsuNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNsuNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).setNsu(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNsuZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNsuZero.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).setNsu(0l);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setBin(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinVazio.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setBin("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinBancoNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinBancoNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setBinBanco(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinBancoVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinBancoVazio.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setBinBanco("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinBandeiraVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinBandeiraVazio.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setBinBandeira("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinBandeiraNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinBandeiraNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setBinBandeira(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinPaisVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinPaisVazio.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setBinPais("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinPaisNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinPaisNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setBinPais(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoCpfTitularVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoCpfTitularVazio.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setCpfTitularCartao("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoCartaoCreditoCpfTitularNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoCpfTitularNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setCpfTitularCartao(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}			
	
	@Test
	public void enfileirarTransacaoCartaoCreditoDataValidadeVazia() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoDataValidadeVazia.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setDataValidadeCartao("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoDataValidadeNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoDataValidadeNula.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setDataValidadeCartao(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoDataValidadeFormatoInvalido() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoDataValidadeFormatoInvalido.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setDataValidadeCartao("abcdefg");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoCartaoCreditoDddVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoDddVazio.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setDddTelefone2("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoDddNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoDddNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setDddTelefone2(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNomeBancoEmissorBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNomeBancoEmissorBranco.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setNomeBancoEmissor("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNomeBancoEmissorNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNomeBancoEmissorNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setNomeBancoEmissor(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNomeTitularCartaoBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNomeTitularCartaoBranco.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setNomeTitularCartao("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNomeTitularCartaoNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNomeTitularCartaoNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setNomeTitularCartao(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroCartaoVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNumeroCartaoVazio.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroCartao("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroCartaoNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNumeroCartaoNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroCartao(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroCartaoInvalido() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNumeroCartaoInvalido.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroCartao("12345d55555");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroTelefoneVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosCartaoCreditoNumeroTelefoneVazio.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroTelefone2("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroTelefoneNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNumeroTelefoneNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroTelefone2(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroTelefoneInvalido() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNumeroTelefoneInvalido.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroTelefone2("a23");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoQuatroUltimosDigitosBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoQuatroUltimosDigitosBranco.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setQuatroUltimosDigitosCartao("");
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoQuatroUltimosDigitosNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoQuatroUltimosDigitosNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getPagamentos().getWsPagamento2().get(0).getCartao().setQuatroUltimosDigitosCartao(null);
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoPaisCompradorNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPaisCompradorNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().getEndereco().setPais(null);
        
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));		
	}
	
	@Test
	public void enfileirarTransacaoPaisCompradorVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPaisCompradorVazio.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().getEndereco().setPais("");
        
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));		
	}
	
	@Test
	public void enfileirarTransacaoPaisCompradorInvalidoBrasil() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPaisCompradorInvalidoBrasil.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().getEndereco().setPais("BR");
        
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(PAIS_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(PAIS_COMPRADOR_INVALIDO.getDescription())));		
	}
	
	@Test
	public void enfileirarTransacaoPaisCompradorInvalidoRandom() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPaisCompradorInvalidoRandom.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().getEndereco().setPais("ZAZ");
        
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(PAIS_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(PAIS_COMPRADOR_INVALIDO.getDescription())));		
	}	
	
	@Test
	public void enfileirarTransacaoCepCompradorNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCepCompradorNulo.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().getEndereco().setCep(null);
        
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));				
	}
	
	@Test
	public void enfileirarTransacaoCepCompradorVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCepCompradorVazio.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().getEndereco().setCep("");
        
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));				
	}
	
	@Test
	public void enfileirarTransacaoCepCompradorZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCepCompradorZero.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().getEndereco().setCep("00000-000");
        
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));				
	}	
	
	@Test
	public void enfileirarTransacaoCepCompradorLetras() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCepCompradorLetras.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().getEndereco().setCep("00000-XXX");
        
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CEP_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(CEP_COMPRADOR_INVALIDO.getDescription())));				
	}		

	@Test
	public void enfileirarTransacaoCepCompradorSemHifen() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCepCompradorSemHifen.");
        }

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().getEndereco().setCep("03146020");
        
        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));				
	}

    @Test
    public void enfileirarTransacaoCompradorDdd1Vazio() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd1Vazio");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddTelefone("");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd1Nulo() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd1Nulo");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddTelefone(null);

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd1AlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd1AlfaNumerico");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddTelefone("aaa");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd1TamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd1AlfaNumerico");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddTelefone("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123450");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone1Nulo() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone1Nulo");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroTelefone(null);

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }


    @Test
    public void enfileirarTransacaoCompradorTelefone1Vazio() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone1Vazio");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroTelefone(null);

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone1AlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone1AlfaNumerico");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroTelefone("asa");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone1TamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone1TamanhoExcedido");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroTelefone("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd2Vazio() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd2Vazio");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddTelefone2("");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd2Nulo() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd2Nulo");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddTelefone2(null);

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd2AlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd2AlfaNumerico");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddTelefone2("aaa");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd2TamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd2AlfaNumerico");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddTelefone2("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123450");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone2Nulo() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone2Nulo");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroTelefone2(null);

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE2_INVALIDO.getDescription())));
    }


    @Test
    public void enfileirarTransacaoCompradorTelefone2Vazio() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone2Vazio");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroTelefone2(null);

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone2AlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone2AlfaNumerico");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroTelefone2("asa");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone2TamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone2TamanhoExcedido");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroTelefone2("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE2_INVALIDO.getDescription())));
    }


    @Test
    public void enfileirarTransacaoCompradorDddCelularVazio() {
        logger.info("Running test: enfileirarTransacaoCompradorDddCelularVazio");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddCelular("");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDddCelularNulo() {
        logger.info("Running test: enfileirarTransacaoCompradorDddCelularNulo");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddCelular(null);

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDddCelularAlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorDddCelularAlfaNumerico");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddCelular("aaa");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_CELULAR_INVALIDO.getStringResponseCode())).and(containsString(DDD_CELULAR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDddCelularTamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorDddCelularAlfaNumerico");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setDddCelular("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123450");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_CELULAR_INVALIDO.getStringResponseCode())).and(containsString(DDD_CELULAR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefoneCelularNulo() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefoneCelularNulo");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroCelular(null);

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CELULAR_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(CELULAR_COMPRADOR_INVALIDO.getDescription())));
    }


    @Test
    public void enfileirarTransacaoCompradorTelefoneCelularVazio() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefoneCelularVazio");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroCelular(null);

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CELULAR_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(CELULAR_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefoneCelularAlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefoneCelularAlfaNumerico");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroCelular("asa");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CELULAR_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(CELULAR_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefoneCelularTamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefoneCelularTamanhoExcedido");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setNumeroCelular("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CELULAR_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(CELULAR_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorEmailNulo() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailNulo");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setEmail(null);

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorEmailBranco() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailBranco");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setEmail("");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorEmailFormatoInvalido() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailFormatoInvalido");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setEmail("anderson");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorEmailFormatoInvalidoSemDominio() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailFormatoInvalidoSemDominio");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setEmail("anderson@teste");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorEmailFormatoInvalidoSemDominio2() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailFormatoInvalidoSemDominio2");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setEmail("anderson@teste.");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getDescription())));
    }


    @Test
    public void enfileirarTransacaoCompradorEmailTamanhoInvalido() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailTamanhoInvalido");

        WsTransacao10 transacao10 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao10.getDadosComprador().setEmail("anderson@teste.com.brakskakskakskakskakskaskaksaksskaksaskskaskaskaskaksaskaskakskasksksksakskaskakskakskaaskskskakaakakakakaakaksksksssskskskskksskskskskksksksksksksksksksksksksskskskasasasjajasjajasjasjsjasjajjsjsjsaajjasjsjsakskasksksaksakaksakskskskskskkksksksk");

        WsResultado response = soapService.enfileirarTransacao10(transacao10);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getDescription())));
    }
}
