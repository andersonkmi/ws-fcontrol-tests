package com.fcontrol.ecommerce;

import java.net.URL;

import org.junit.Before;

import com.fcontrol.WSFControl2;
import com.fcontrol.WSFControl2Soap;

public class BaseSoapWebServiceTestCase {
	private URL wsdlUrl;
	private WSFControl2 service;
	protected WSFControl2Soap soapService;

	@Before
	public void setUp() throws Exception {
        service = new WSFControl2(getWsdlUrl());
        soapService = service.getWSFControl2Soap();
	}
	
	protected URL getWsdlUrl() {
		if(wsdlUrl == null) {
			wsdlUrl = BaseSoapWebServiceTestCase.class.getClassLoader().getResource("WSFControl2.wsdl");
		}
		return wsdlUrl;
	}
}
