package com.fcontrol.ecommerce;

import static com.fcontrol.MetodoPagamento.BOLETO_BANCARIO;
import static com.fcontrol.MetodoPagamento.CARTAO_MASTER_CARD;
import static com.fcontrol.data.WSFControlResponseCode.CELULAR_COMPRADOR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.CEP_COMPRADOR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.CNPJ_CPF_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.DATA_COMPRA_INVALIDA;
import static com.fcontrol.data.WSFControlResponseCode.DDD_CELULAR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.DDD_TELEFONE2_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.DDD_TELEFONE_COMPRADOR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.METODO_PAGAMENTO_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.PAIS_COMPRADOR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.PRAZO_ENTREGA_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.QUANTIDADE_ITENS_DISTINTOS_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.QUANTIDADE_PRODUTO_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.QUANTIDADE_TOTAL_ITENS_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.TELEFONE2_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.TELEFONE_COMPRADOR_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.TRANSACAO_ENFILEIRADA_SUCESSO;
import static com.fcontrol.data.WSFControlResponseCode.TRANSACAO_JA_ENFILEIRADO;
import static com.fcontrol.data.WSFControlResponseCode.USUARIO_SENHA_INVALIDOS;
import static com.fcontrol.data.WSFControlResponseCode.VALOR_FRETE_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.VALOR_PEDIDO_INVALIDO;
import static com.fcontrol.data.WSFControlResponseCode.VALOR_UNITARIO_PRODUTO_INVALIDO;
import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fcontrol.WsResultado;
import com.fcontrol.WsTransacao9;
import com.fcontrol.data.EcommercePadraoTransacao9Builder;
import com.fcontrol.data.WSFControlResponseCode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:**/applicationContext.xml"})
public class EnfileirarTransacao9SemCamposObrigatoriosTestCase extends BaseSoapWebServiceTestCase {
	private static final Logger logger = Logger.getLogger(EnfileirarTransacao9SemCamposObrigatoriosTestCase.class);

    @Resource
    private EcommercePadraoTransacao9Builder builder;
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
        builder.setUserName("fc-ut-enfileirar09");
        builder.setPassword("142557");
	}

	@Test
	public void enfileirarTransacao() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacao.");
		}
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }
	
	@Test
	public void enfileirarTransacaoCpfInvalido() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCpfInvalido.");
		}
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getDadosComprador().setCpfCnpj("25233688457");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CNPJ_CPF_INVALIDO.getStringResponseCode())).and(containsString(CNPJ_CPF_INVALIDO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCpfBranco() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCpfBranco.");
		}
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getDadosComprador().setCpfCnpj("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }
	
	@Test
	public void enfileirarTransacaoCpfNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCpfNulo.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getDadosComprador().setCpfCnpj(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }
	

	@Test
	public void enfileirarTransacaoCanalVendaVazio() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCanalVendaVazio.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setCanalVenda("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCanalVendaNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCanalVendaNulo.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setCanalVenda(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCodigoPedido2Nulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCodigoPedido2Nulo.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setCodigoPedido2(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoCodigoPedido2Branco() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoCodigoPedido2Branco.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setCodigoPedido2("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoDadosCompradorNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosCompradorNulo.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setDadosComprador(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoDadosEntregaNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosEntregaNulo.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setDadosEntrega(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoDadosExtraNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosExtraNulo.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setDadosExtra(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	
	@Test
	public void enfileirarTransacaoPedidoRepetido() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoPedidoRepetido.");
		}
		
        WsTransacao9 transacao1 = builder.buildTransacao(BOLETO_BANCARIO);
        WsResultado response = soapService.enfileirarTransacao9(transacao1);
        assertThat(response.isSucesso(), equalTo(true));
        
        WsTransacao9 transacao2 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao2.setCodigoPedido(transacao1.getCodigoPedido());
        transacao2.setCodigoPedido2(transacao1.getCodigoPedido2());
        
        WsResultado response2 = soapService.enfileirarTransacao9(transacao2);
        assertThat(response2.isSucesso(), equalTo(false));
        assertThat(response2.getMensagem(), both(containsString(TRANSACAO_JA_ENFILEIRADO.getStringResponseCode())).and(containsString(TRANSACAO_JA_ENFILEIRADO.getDescription())));
	}	

	@Test
	public void enfileirarTransacaoAutenticacaoNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoNulo.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setDadosUsuario(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoAutenticacaoSenhaInvalida() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoSenhaInvalida.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getDadosUsuario().setSenha("bling");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoAutenticacaoUsuarioInexistente() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoUsuarioInexistente.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getDadosUsuario().setLogin("_lojademo_");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoAutenticacaoUsuarioInvalido() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoUsuarioInvalido.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getDadosUsuario().setLogin("lojademotroca");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoAutenticacaoUsuarioBranco() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoUsuarioBranco.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getDadosUsuario().setLogin("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoAutenticacaoSenhaBranco() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoAutenticacaoSenhaBranco.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getDadosUsuario().setSenha("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(USUARIO_SENHA_INVALIDOS.getStringResponseCode())).and(containsString(USUARIO_SENHA_INVALIDOS.getDescription())));
	}	

	@Test
	public void enfileirarTransacaoDadosPagamentoNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosPagamentoNulo.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setPagamentos(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(METODO_PAGAMENTO_INVALIDO.getStringResponseCode())).and(containsString(METODO_PAGAMENTO_INVALIDO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoDadosPagamentoVazio() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosPagamentoVazio.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getPagamentos().getWsPagamento2().clear();
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(METODO_PAGAMENTO_INVALIDO.getStringResponseCode())).and(containsString(METODO_PAGAMENTO_INVALIDO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoDadosPagamentoNumeroParcelasZero() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosPagamentoNumeroParcelasZero.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getPagamentos().getWsPagamento2().get(0).setNumeroParcelas(0);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoDadosPagamentoValorZero() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosPagamentoValorZero.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getPagamentos().getWsPagamento2().get(0).setValor(0);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}			
	
	@Test
	public void enfileirarTransacaoDadosProdutoNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosProdutoNulo.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setProdutos(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoDadosProdutoVazio() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosProdutoVazio.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getProdutos().getWsProduto3().clear();
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoDadosProdutoCodigoNulo() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: enfileirarTransacaoDadosProdutoCodigoNulo.");
		}
		
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getProdutos().getWsProduto3().get(0).setCodigo(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}			
	
	@Test
	public void enfileirarTransacaoDadosProdutoCategoriaBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoCategoriaBranco.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getProdutos().getWsProduto3().get(0).setCategoria("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}				
	
	@Test
	public void enfileirarTransacaoDadosProdutoCategoriaNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoCategoriaNula.");
        }
        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getProdutos().getWsProduto3().get(0).setCategoria(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}				
	
	@Test
	public void enfileirarTransacaoDadosProdutoDescricaoBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoDescricaoBranco.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getProdutos().getWsProduto3().get(0).setDescricao("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}				
	
	@Test
	public void enfileirarTransacaoDadosProdutoDescricaoNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoDescricaoNula.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getProdutos().getWsProduto3().get(0).setDescricao(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}				
	
	@Test
	public void enfileirarTransacaoDadosProdutoQuantidadeZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoQuantidadeZero.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getProdutos().getWsProduto3().get(0).setQuantidade(0);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}					
	
	@Test
	public void enfileirarTransacaoDadosProdutoQuantidadeNegativa() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoQuantidadeNegativa.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getProdutos().getWsProduto3().get(0).setQuantidade(-1);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(QUANTIDADE_PRODUTO_INVALIDO.getStringResponseCode())).and(containsString(QUANTIDADE_PRODUTO_INVALIDO.getDescription())));
	}						
	
	@Test
	public void enfileirarTransacaoDadosProdutoValorUnitarioZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoValorUnitarioZero.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getProdutos().getWsProduto3().get(0).setValorUnitario(0);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}						
	
	@Test
	public void enfileirarTransacaoDadosProdutoValorUnitarioNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosProdutoValorUnitarioNegativo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.getProdutos().getWsProduto3().get(0).setValorUnitario(-1);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(VALOR_UNITARIO_PRODUTO_INVALIDO.getStringResponseCode())).and(containsString(VALOR_UNITARIO_PRODUTO_INVALIDO.getDescription())));
	}						
	
	@Test
	public void enfileirarTransacaoDataCompraNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDataCompraNula.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setDataCompra(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DATA_COMPRA_INVALIDA.getStringResponseCode())).and(containsString(DATA_COMPRA_INVALIDA.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoDataEntregaNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDataEntregaNula.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setDataEntrega(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoFormaEntregaBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoFormaEntregaBranco.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setFormaEntrega("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoFormaEntregaNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoFormaEntregaNula.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setFormaEntrega(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoPrazoEntregaZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPrazoEntregaZero.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setPrazoEntregaDias(0);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoPrazoEntregaNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPrazoEntregaNegativo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setPrazoEntregaDias(-2);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(PRAZO_ENTREGA_INVALIDO.getStringResponseCode())).and(containsString(PRAZO_ENTREGA_INVALIDO.getDescription())));
	}			
	
	@Test
	public void enfileirarTransacaoQuantidadeItensDistintosZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoQuantidadeItensDistintosZero.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setQuantidadeItensDistintos(0);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	

	@Test
	public void enfileirarTransacaoQuantidadeItensDistintosNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoQuantidadeItensDistintosNegativos.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setQuantidadeItensDistintos(-1);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(QUANTIDADE_ITENS_DISTINTOS_INVALIDO.getStringResponseCode())).and(containsString(QUANTIDADE_ITENS_DISTINTOS_INVALIDO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoQuantidadeTotalItensZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoQuantidadeTotalItensZero.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setQuantidadeTotalItens(0);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoQuantidadeTotalItensNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoQuantidadeTotalItensNegativo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setQuantidadeTotalItens(-1);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(QUANTIDADE_TOTAL_ITENS_INVALIDO.getStringResponseCode())).and(containsString(QUANTIDADE_TOTAL_ITENS_INVALIDO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoValorTotalCompraZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoValorTotalCompraZero.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setValorTotalCompra(0);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoValorTotalCompraNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoValorTotalCompraNegativo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setValorTotalCompra(-1);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(VALOR_PEDIDO_INVALIDO.getStringResponseCode())).and(containsString(VALOR_PEDIDO_INVALIDO.getDescription())));
	}
	
	
	@Test
	public void enfileirarTransacaoValorTotalFreteZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoValorTotalFreteZero.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setValorTotalFrete(0);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoValorTotalFreteNegativo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoValorTotalFreteNegativo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(BOLETO_BANCARIO);
        transacao9.setValorTotalFrete(-1);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(VALOR_FRETE_INVALIDO.getStringResponseCode())).and(containsString(VALOR_FRETE_INVALIDO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoOk() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoOk.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNsuNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNsuNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).setNsu(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNsuZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNsuZero.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).setNsu(0l);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setBin(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinVazio.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setBin("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinBancoNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinBancoNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setBinBanco(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinBancoVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinBancoVazio.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setBinBanco("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinBandeiraVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinBandeiraVazio.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setBinBandeira("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinBandeiraNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinBandeiraNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setBinBandeira(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinPaisVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinPaisVazio.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setBinPais("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoBinPaisNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoBinPaisNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setBinPais(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoCpfTitularVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoCpfTitularVazio.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setCpfTitularCartao("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoCartaoCreditoCpfTitularNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoCpfTitularNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setCpfTitularCartao(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}			
	
	@Test
	public void enfileirarTransacaoCartaoCreditoDataValidadeVazia() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoDataValidadeVazia.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setDataValidadeCartao("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoDataValidadeNula() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoDataValidadeNula.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setDataValidadeCartao(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoDataValidadeFormatoInvalido() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoDataValidadeFormatoInvalido.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setDataValidadeCartao("abcdefg");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}		
	
	@Test
	public void enfileirarTransacaoCartaoCreditoDddVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoDddVazio.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setDddTelefone2("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoDddNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoDddNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setDddTelefone2(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNomeBancoEmissorBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNomeBancoEmissorBranco.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setNomeBancoEmissor("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNomeBancoEmissorNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNomeBancoEmissorNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setNomeBancoEmissor(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNomeTitularCartaoBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNomeTitularCartaoBranco.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setNomeTitularCartao("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNomeTitularCartaoNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNomeTitularCartaoNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setNomeTitularCartao(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroCartaoVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNumeroCartaoVazio.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroCartao("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroCartaoNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNumeroCartaoNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroCartao(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroCartaoInvalido() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNumeroCartaoInvalido.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroCartao("12345d55555");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroTelefoneVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoDadosCartaoCreditoNumeroTelefoneVazio.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroTelefone2("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}	
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroTelefoneNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNumeroTelefoneNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroTelefone2(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoNumeroTelefoneInvalido() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoNumeroTelefoneInvalido.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setNumeroTelefone2("a23");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoQuatroUltimosDigitosBranco() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoQuatroUltimosDigitosBranco.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setQuatroUltimosDigitosCartao("");
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoCartaoCreditoQuatroUltimosDigitosNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCartaoCreditoQuatroUltimosDigitosNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getPagamentos().getWsPagamento2().get(0).getCartao().setQuatroUltimosDigitosCartao(null);
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
	}
	
	@Test
	public void enfileirarTransacaoPaisCompradorNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPaisCompradorNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().getEndereco().setPais(null);
        
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));		
	}
	
	@Test
	public void enfileirarTransacaoPaisCompradorVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPaisCompradorVazio.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().getEndereco().setPais("");
        
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));		
	}
	
	@Test
	public void enfileirarTransacaoPaisCompradorInvalidoBrasil() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPaisCompradorInvalidoBrasil.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().getEndereco().setPais("BR");
        
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(PAIS_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(PAIS_COMPRADOR_INVALIDO.getDescription())));		
	}
	
	@Test
	public void enfileirarTransacaoPaisCompradorInvalidoRandom() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoPaisCompradorInvalidoRandom.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().getEndereco().setPais("ZAZ");
        
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(PAIS_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(PAIS_COMPRADOR_INVALIDO.getDescription())));		
	}	
	
	@Test
	public void enfileirarTransacaoCepCompradorNulo() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCepCompradorNulo.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().getEndereco().setCep(null);
        
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));				
	}
	
	@Test
	public void enfileirarTransacaoCepCompradorVazio() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCepCompradorVazio.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().getEndereco().setCep("");
        
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));				
	}
	
	@Test
	public void enfileirarTransacaoCepCompradorZero() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCepCompradorZero.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().getEndereco().setCep("00000-000");
        
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));				
	}	
	
	@Test
	public void enfileirarTransacaoCepCompradorLetras() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCepCompradorLetras.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().getEndereco().setCep("00000-XXX");
        
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CEP_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(CEP_COMPRADOR_INVALIDO.getDescription())));				
	}		

	@Test
	public void enfileirarTransacaoCepCompradorSemHifen() {
        if(logger.isInfoEnabled()) {
            logger.info("Running test: enfileirarTransacaoCepCompradorSemHifen.");
        }

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().getEndereco().setCep("03146020");
        
        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));				
	}

    @Test
    public void enfileirarTransacaoCompradorDdd1Vazio() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd1Vazio");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddTelefone("");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd1Nulo() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd1Nulo");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddTelefone(null);

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd1AlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd1AlfaNumerico");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddTelefone("aaa");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd1TamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd1AlfaNumerico");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddTelefone("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123450");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone1Nulo() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone1Nulo");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroTelefone(null);

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }


    @Test
    public void enfileirarTransacaoCompradorTelefone1Vazio() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone1Vazio");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroTelefone(null);

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone1AlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone1AlfaNumerico");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroTelefone("asa");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone1TamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone1TamanhoExcedido");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroTelefone("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd2Vazio() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd2Vazio");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddTelefone2("");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd2Nulo() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd2Nulo");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddTelefone2(null);

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd2AlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd2AlfaNumerico");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddTelefone2("aaa");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDdd2TamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorDdd2AlfaNumerico");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddTelefone2("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123450");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(DDD_TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone2Nulo() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone2Nulo");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroTelefone2(null);

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE2_INVALIDO.getDescription())));
    }


    @Test
    public void enfileirarTransacaoCompradorTelefone2Vazio() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone2Vazio");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroTelefone2(null);

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone2AlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone2AlfaNumerico");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroTelefone2("asa");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE2_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefone2TamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefone2TamanhoExcedido");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroTelefone2("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(TELEFONE2_INVALIDO.getStringResponseCode())).and(containsString(TELEFONE2_INVALIDO.getDescription())));
    }


    @Test
    public void enfileirarTransacaoCompradorDddCelularVazio() {
        logger.info("Running test: enfileirarTransacaoCompradorDddCelularVazio");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddCelular("");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDddCelularNulo() {
        logger.info("Running test: enfileirarTransacaoCompradorDddCelularNulo");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddCelular(null);

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDddCelularAlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorDddCelularAlfaNumerico");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddCelular("aaa");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_CELULAR_INVALIDO.getStringResponseCode())).and(containsString(DDD_CELULAR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorDddCelularTamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorDddCelularAlfaNumerico");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setDddCelular("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123450");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(DDD_CELULAR_INVALIDO.getStringResponseCode())).and(containsString(DDD_CELULAR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefoneCelularNulo() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefoneCelularNulo");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroCelular(null);

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CELULAR_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(CELULAR_COMPRADOR_INVALIDO.getDescription())));
    }


    @Test
    public void enfileirarTransacaoCompradorTelefoneCelularVazio() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefoneCelularVazio");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroCelular(null);

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CELULAR_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(CELULAR_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefoneCelularAlfaNumerico() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefoneCelularAlfaNumerico");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroCelular("asa");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CELULAR_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(CELULAR_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorTelefoneCelularTamanhoExcedido() {
        logger.info("Running test: enfileirarTransacaoCompradorTelefoneCelularTamanhoExcedido");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setNumeroCelular("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(CELULAR_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(CELULAR_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorEmailNulo() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailNulo");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setEmail(null);

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorEmailBranco() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailBranco");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setEmail("");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getMensagem(), both(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getStringResponseCode())).and(containsString(TRANSACAO_ENFILEIRADA_SUCESSO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorEmailFormatoInvalido() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailFormatoInvalido");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setEmail("anderson");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorEmailFormatoInvalidoSemDominio() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailFormatoInvalidoSemDominio");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setEmail("anderson@teste");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getDescription())));
    }

    @Test
    public void enfileirarTransacaoCompradorEmailFormatoInvalidoSemDominio2() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailFormatoInvalidoSemDominio2");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setEmail("anderson@teste.");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getDescription())));
    }


    @Test
    public void enfileirarTransacaoCompradorEmailTamanhoInvalido() {
        logger.info("Running test: enfileirarTransacaoCompradorEmailTamanhoInvalido");

        WsTransacao9 transacao9 = builder.buildTransacao(CARTAO_MASTER_CARD);
        transacao9.getDadosComprador().setEmail("anderson@teste.com.brakskakskakskakskakskaskaksaksskaksaskskaskaskaskaksaskaskakskasksksksakskaskakskakskaaskskskakaakakakakaakaksksksssskskskskksskskskskksksksksksksksksksksksksskskskasasasjajasjajasjasjsjasjajjsjsjsaajjasjsjsakskasksksaksakaksakskskskskskkksksksk");

        WsResultado response = soapService.enfileirarTransacao9(transacao9);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getMensagem(), both(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getStringResponseCode())).and(containsString(WSFControlResponseCode.EMAIL_COMPRADOR_INVALIDO.getDescription())));
    }
}
