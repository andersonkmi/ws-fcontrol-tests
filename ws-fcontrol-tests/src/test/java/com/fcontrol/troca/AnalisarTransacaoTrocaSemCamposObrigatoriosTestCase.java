package com.fcontrol.troca;

import static com.fcontrol.data.WSFControlResponseCode.TRANSACAO_ANALISADA_SUCESSO;
import static com.fcontrol.data.WSFControlTrocaResponseCode.RESSARCIMENTO_TIPO_INVALIDO;
import static com.fcontrol.data.WSFControlTrocaResponseCode.TIPO_MANIFESTACAO_INVALIDO;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.annotation.Resource;

import com.fcontrol.ecommerce.BaseSoapWebServiceTestCase;
import com.fcontrol.ecommerce.EnfileirarTransacao9SemCamposObrigatoriosTestCase;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fcontrol.WsRetornoTroca;
import com.fcontrol.WsTransacaoTroca;
import com.fcontrol.data.EcommerceTrocaTransactionBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:**/applicationContext.xml"})
public class AnalisarTransacaoTrocaSemCamposObrigatoriosTestCase extends BaseSoapWebServiceTestCase {
	private static final Logger logger = Logger.getLogger(EnfileirarTransacao9SemCamposObrigatoriosTestCase.class);
	
	@Resource
	private EcommerceTrocaTransactionBuilder ecommerceTrocaTransactionBuilder;
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
        ecommerceTrocaTransactionBuilder.setUserName("fc-ut-analisar-troca");
        ecommerceTrocaTransactionBuilder.setPassword("142557");
	}
	
	@Test
	public void analisarTransacaoTrocaOk() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: analisarTransacaoTrocaOk.");
		}

		WsTransacaoTroca transacao = ecommerceTrocaTransactionBuilder.build();
        WsRetornoTroca response = soapService.analisarTransacaoTroca(transacao);
        assertThat(response.isSucesso(), equalTo(true));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getCodigo(), equalTo(0));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getMensagem(), containsString(TRANSACAO_ANALISADA_SUCESSO.getDescription()));
	}
	
	@Test
	public void analisarTransacaoTrocaTipoRessarcimentoNullFail() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: analisarTransacaoTrocaTipoRessarcimentoNullFail.");
		}

		WsTransacaoTroca transacao = ecommerceTrocaTransactionBuilder.build();
		transacao.getRessarcimento().setTipo(null);
        WsRetornoTroca response = soapService.analisarTransacaoTroca(transacao);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getCodigo(), equalTo(RESSARCIMENTO_TIPO_INVALIDO.getResponseCode()));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getMensagem(), containsString(RESSARCIMENTO_TIPO_INVALIDO.getDescription()));
	}	
	
	@Test
	public void analisarTransacaoTrocaTipoRessarcimentoVazioFail() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: analisarTransacaoTrocaTipoRessarcimentoVazioFail.");
		}

		WsTransacaoTroca transacao = ecommerceTrocaTransactionBuilder.build();
		transacao.getRessarcimento().setTipo("");
        WsRetornoTroca response = soapService.analisarTransacaoTroca(transacao);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getCodigo(), equalTo(RESSARCIMENTO_TIPO_INVALIDO.getResponseCode()));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getMensagem(), containsString(RESSARCIMENTO_TIPO_INVALIDO.getDescription()));
	}
	
	@Test
	public void analisarTransacaoTrocaTipoRessarcimentoInvalidoFail() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: analisarTransacaoTrocaTipoRessarcimentoInvalidoFail.");
		}

		WsTransacaoTroca transacao = ecommerceTrocaTransactionBuilder.build();
		transacao.getRessarcimento().setTipo("Xis");
        WsRetornoTroca response = soapService.analisarTransacaoTroca(transacao);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getCodigo(), equalTo(RESSARCIMENTO_TIPO_INVALIDO.getResponseCode()));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getMensagem(), containsString(RESSARCIMENTO_TIPO_INVALIDO.getDescription()));
	}	
	
	@Test
	public void analisarTransacaoTrocaTipoManifestacaoNuloFail() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: analisarTransacaoTrocaTipoManifestacaoNuloFail.");
		}

		WsTransacaoTroca transacao = ecommerceTrocaTransactionBuilder.build();
		transacao.setTipoManifestacao(null);
        WsRetornoTroca response = soapService.analisarTransacaoTroca(transacao);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getCodigo(), equalTo(TIPO_MANIFESTACAO_INVALIDO.getResponseCode()));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getMensagem(), containsString(TIPO_MANIFESTACAO_INVALIDO.getDescription()));
	}		
	
	@Test
	public void analisarTransacaoTrocaTipoManifestacaoVazioFail() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: analisarTransacaoTrocaTipoManifestacaoVazioFail.");
		}

		WsTransacaoTroca transacao = ecommerceTrocaTransactionBuilder.build();
		transacao.setTipoManifestacao("");
        WsRetornoTroca response = soapService.analisarTransacaoTroca(transacao);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getCodigo(), equalTo(TIPO_MANIFESTACAO_INVALIDO.getResponseCode()));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getMensagem(), containsString(TIPO_MANIFESTACAO_INVALIDO.getDescription()));
	}
	
	@Test
	public void analisarTransacaoTrocaTipoManifestacaoInvalidoFail() {
		if(logger.isInfoEnabled()) {
			logger.info("Running test: analisarTransacaoTrocaTipoManifestacaoInvalidoFail.");
		}

		WsTransacaoTroca transacao = ecommerceTrocaTransactionBuilder.build();
		transacao.setTipoManifestacao("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111110");
        WsRetornoTroca response = soapService.analisarTransacaoTroca(transacao);
        assertThat(response.isSucesso(), equalTo(false));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getCodigo(), equalTo(TIPO_MANIFESTACAO_INVALIDO.getResponseCode()));
        assertThat(response.getResultadoValidacao().getWsResultadoValidacaoTroca().get(0).getMensagem(), containsString(TIPO_MANIFESTACAO_INVALIDO.getDescription()));
	}	
}
