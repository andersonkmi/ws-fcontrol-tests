
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsAeroBilhete2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsAeroBilhete2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsAeroBilhete2" type="{http://tempuri.org/}WsAeroBilhete2" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsAeroBilhete2", propOrder = {
    "wsAeroBilhete2"
})
public class ArrayOfWsAeroBilhete2 {

    @XmlElement(name = "WsAeroBilhete2", nillable = true)
    protected List<WsAeroBilhete2> wsAeroBilhete2;

    /**
     * Gets the value of the wsAeroBilhete2 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsAeroBilhete2 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsAeroBilhete2().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsAeroBilhete2 }
     * 
     * 
     */
    public List<WsAeroBilhete2> getWsAeroBilhete2() {
        if (wsAeroBilhete2 == null) {
            wsAeroBilhete2 = new ArrayList<WsAeroBilhete2>();
        }
        return this.wsAeroBilhete2;
    }

}
