
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsAeroBilhete complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAeroBilhete">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ddd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Telefone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SomenteIda" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="AeroportoOrigem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AeroportoDestino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataPartida" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataRetorno" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DddTelefone2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroTelefone2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAeroBilhete", propOrder = {
    "nome",
    "ddd",
    "telefone",
    "somenteIda",
    "aeroportoOrigem",
    "aeroportoDestino",
    "dataPartida",
    "dataRetorno",
    "dddTelefone2",
    "numeroTelefone2"
})
@XmlSeeAlso({
    WsAeroBilhete2 .class
})
public class WsAeroBilhete {

    @XmlElement(name = "Nome")
    protected String nome;
    @XmlElement(name = "Ddd")
    protected String ddd;
    @XmlElement(name = "Telefone")
    protected String telefone;
    @XmlElement(name = "SomenteIda")
    protected boolean somenteIda;
    @XmlElement(name = "AeroportoOrigem")
    protected String aeroportoOrigem;
    @XmlElement(name = "AeroportoDestino")
    protected String aeroportoDestino;
    @XmlElement(name = "DataPartida", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataPartida;
    @XmlElement(name = "DataRetorno", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataRetorno;
    @XmlElement(name = "DddTelefone2")
    protected String dddTelefone2;
    @XmlElement(name = "NumeroTelefone2")
    protected String numeroTelefone2;

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the ddd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDdd() {
        return ddd;
    }

    /**
     * Sets the value of the ddd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDdd(String value) {
        this.ddd = value;
    }

    /**
     * Gets the value of the telefone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * Sets the value of the telefone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefone(String value) {
        this.telefone = value;
    }

    /**
     * Gets the value of the somenteIda property.
     * 
     */
    public boolean isSomenteIda() {
        return somenteIda;
    }

    /**
     * Sets the value of the somenteIda property.
     * 
     */
    public void setSomenteIda(boolean value) {
        this.somenteIda = value;
    }

    /**
     * Gets the value of the aeroportoOrigem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAeroportoOrigem() {
        return aeroportoOrigem;
    }

    /**
     * Sets the value of the aeroportoOrigem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAeroportoOrigem(String value) {
        this.aeroportoOrigem = value;
    }

    /**
     * Gets the value of the aeroportoDestino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAeroportoDestino() {
        return aeroportoDestino;
    }

    /**
     * Sets the value of the aeroportoDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAeroportoDestino(String value) {
        this.aeroportoDestino = value;
    }

    /**
     * Gets the value of the dataPartida property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataPartida() {
        return dataPartida;
    }

    /**
     * Sets the value of the dataPartida property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataPartida(XMLGregorianCalendar value) {
        this.dataPartida = value;
    }

    /**
     * Gets the value of the dataRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRetorno() {
        return dataRetorno;
    }

    /**
     * Sets the value of the dataRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRetorno(XMLGregorianCalendar value) {
        this.dataRetorno = value;
    }

    /**
     * Gets the value of the dddTelefone2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddTelefone2() {
        return dddTelefone2;
    }

    /**
     * Sets the value of the dddTelefone2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddTelefone2(String value) {
        this.dddTelefone2 = value;
    }

    /**
     * Gets the value of the numeroTelefone2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTelefone2() {
        return numeroTelefone2;
    }

    /**
     * Sets the value of the numeroTelefone2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTelefone2(String value) {
        this.numeroTelefone2 = value;
    }

}
