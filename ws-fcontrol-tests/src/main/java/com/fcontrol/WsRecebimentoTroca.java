
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsRecebimentoTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsRecebimentoTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumeroChamado" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="DataRecebimento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="NomeUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmAcordo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsRecebimentoTroca", propOrder = {
    "numeroChamado",
    "dataRecebimento",
    "nomeUsuario",
    "observacao",
    "emAcordo"
})
public class WsRecebimentoTroca {

    @XmlElement(name = "NumeroChamado")
    protected long numeroChamado;
    @XmlElement(name = "DataRecebimento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataRecebimento;
    @XmlElement(name = "NomeUsuario")
    protected String nomeUsuario;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(name = "EmAcordo")
    protected boolean emAcordo;

    /**
     * Gets the value of the numeroChamado property.
     * 
     */
    public long getNumeroChamado() {
        return numeroChamado;
    }

    /**
     * Sets the value of the numeroChamado property.
     * 
     */
    public void setNumeroChamado(long value) {
        this.numeroChamado = value;
    }

    /**
     * Gets the value of the dataRecebimento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRecebimento() {
        return dataRecebimento;
    }

    /**
     * Sets the value of the dataRecebimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRecebimento(XMLGregorianCalendar value) {
        this.dataRecebimento = value;
    }

    /**
     * Gets the value of the nomeUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeUsuario() {
        return nomeUsuario;
    }

    /**
     * Sets the value of the nomeUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeUsuario(String value) {
        this.nomeUsuario = value;
    }

    /**
     * Gets the value of the observacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Sets the value of the observacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Gets the value of the emAcordo property.
     * 
     */
    public boolean isEmAcordo() {
        return emAcordo;
    }

    /**
     * Sets the value of the emAcordo property.
     * 
     */
    public void setEmAcordo(boolean value) {
        this.emAcordo = value;
    }

}
