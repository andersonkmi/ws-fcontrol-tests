
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadoEspecificoSubLoja3Result" type="{http://tempuri.org/}WsAnalise3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadoEspecificoSubLoja3Result"
})
@XmlRootElement(name = "capturarResultadoEspecificoSubLoja3Response")
public class CapturarResultadoEspecificoSubLoja3Response {

    protected WsAnalise3 capturarResultadoEspecificoSubLoja3Result;

    /**
     * Gets the value of the capturarResultadoEspecificoSubLoja3Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsAnalise3 }
     *     
     */
    public WsAnalise3 getCapturarResultadoEspecificoSubLoja3Result() {
        return capturarResultadoEspecificoSubLoja3Result;
    }

    /**
     * Sets the value of the capturarResultadoEspecificoSubLoja3Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAnalise3 }
     *     
     */
    public void setCapturarResultadoEspecificoSubLoja3Result(WsAnalise3 value) {
        this.capturarResultadoEspecificoSubLoja3Result = value;
    }

}
