
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsAnaliseTodasSublojas2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsAnaliseTodasSublojas2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsAnaliseTodasSublojas2" type="{http://tempuri.org/}WsAnaliseTodasSublojas2" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsAnaliseTodasSublojas2", propOrder = {
    "wsAnaliseTodasSublojas2"
})
public class ArrayOfWsAnaliseTodasSublojas2 {

    @XmlElement(name = "WsAnaliseTodasSublojas2", nillable = true)
    protected List<WsAnaliseTodasSublojas2> wsAnaliseTodasSublojas2;

    /**
     * Gets the value of the wsAnaliseTodasSublojas2 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsAnaliseTodasSublojas2 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsAnaliseTodasSublojas2().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsAnaliseTodasSublojas2 }
     * 
     * 
     */
    public List<WsAnaliseTodasSublojas2> getWsAnaliseTodasSublojas2() {
        if (wsAnaliseTodasSublojas2 == null) {
            wsAnaliseTodasSublojas2 = new ArrayList<WsAnaliseTodasSublojas2>();
        }
        return this.wsAnaliseTodasSublojas2;
    }

}
