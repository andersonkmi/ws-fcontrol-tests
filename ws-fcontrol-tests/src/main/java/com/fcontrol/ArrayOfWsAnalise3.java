
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsAnalise3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsAnalise3">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsAnalise3" type="{http://tempuri.org/}WsAnalise3" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsAnalise3", propOrder = {
    "wsAnalise3"
})
public class ArrayOfWsAnalise3 {

    @XmlElement(name = "WsAnalise3", nillable = true)
    protected List<WsAnalise3> wsAnalise3;

    /**
     * Gets the value of the wsAnalise3 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsAnalise3 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsAnalise3().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsAnalise3 }
     * 
     * 
     */
    public List<WsAnalise3> getWsAnalise3() {
        if (wsAnalise3 == null) {
            wsAnalise3 = new ArrayList<WsAnalise3>();
        }
        return this.wsAnalise3;
    }

}
