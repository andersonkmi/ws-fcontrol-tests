
package com.fcontrol;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MetodoPagamento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MetodoPagamento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CartaoCredito"/>
 *     &lt;enumeration value="CartaoVisa"/>
 *     &lt;enumeration value="CartaoMasterCard"/>
 *     &lt;enumeration value="CartaoDiners"/>
 *     &lt;enumeration value="CartaoAmericanExpress"/>
 *     &lt;enumeration value="CartaoHiperCard"/>
 *     &lt;enumeration value="CartaoAura"/>
 *     &lt;enumeration value="PagamentoEntrega"/>
 *     &lt;enumeration value="DebitoTransferenciaEletronica"/>
 *     &lt;enumeration value="BoletoBancario"/>
 *     &lt;enumeration value="Financiamento"/>
 *     &lt;enumeration value="Cheque"/>
 *     &lt;enumeration value="Deposito"/>
 *     &lt;enumeration value="CartaoMarisa"/>
 *     &lt;enumeration value="BoletoGlobex"/>
 *     &lt;enumeration value="ValePresente"/>
 *     &lt;enumeration value="SecureCodeMasterCard"/>
 *     &lt;enumeration value="BoletoB2B"/>
 *     &lt;enumeration value="OiPaggo"/>
 *     &lt;enumeration value="CartaoPontoFrio"/>
 *     &lt;enumeration value="MultiCheque"/>
 *     &lt;enumeration value="CartaoPaoAcucar"/>
 *     &lt;enumeration value="CartaoSoroCred"/>
 *     &lt;enumeration value="MultiCash"/>
 *     &lt;enumeration value="BoletoAPrazo"/>
 *     &lt;enumeration value="ExtraPL"/>
 *     &lt;enumeration value="ExtraMasterCard"/>
 *     &lt;enumeration value="ExtraVisa"/>
 *     &lt;enumeration value="PontoCred"/>
 *     &lt;enumeration value="Loja"/>
 *     &lt;enumeration value="ItauCard"/>
 *     &lt;enumeration value="TransferenciaEletronicaBancoBrasil"/>
 *     &lt;enumeration value="TransferenciaEletronicaBradesco"/>
 *     &lt;enumeration value="TransferenciaEletronicaItau"/>
 *     &lt;enumeration value="DebitoContaItau"/>
 *     &lt;enumeration value="CartaoPresenteExtra"/>
 *     &lt;enumeration value="CasasBahiaVisa"/>
 *     &lt;enumeration value="CasasBahiaMasterCard"/>
 *     &lt;enumeration value="BoletoAVistaAtacado"/>
 *     &lt;enumeration value="PagamentoDigital"/>
 *     &lt;enumeration value="Dinheiro"/>
 *     &lt;enumeration value="ValeAlimentacao"/>
 *     &lt;enumeration value="PaoDeAcucarMais"/>
 *     &lt;enumeration value="CartaoELO"/>
 *     &lt;enumeration value="CartaoExtra"/>
 *     &lt;enumeration value="CartaoSendas"/>
 *     &lt;enumeration value="Comprebem"/>
 *     &lt;enumeration value="CartaoFidelidade"/>
 *     &lt;enumeration value="Multiplus"/>
 *     &lt;enumeration value="PrePaidCard"/>
 *     &lt;enumeration value="Alipay"/>
 *     &lt;enumeration value="DineroMail"/>
 *     &lt;enumeration value="Cielo"/>
 *     &lt;enumeration value="PayPal"/>
 *     &lt;enumeration value="Cupom"/>
 *     &lt;enumeration value="FlexCliente"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MetodoPagamento")
@XmlEnum
public enum MetodoPagamento {

    @XmlEnumValue("CartaoCredito")
    CARTAO_CREDITO("CartaoCredito"),
    @XmlEnumValue("CartaoVisa")
    CARTAO_VISA("CartaoVisa"),
    @XmlEnumValue("CartaoMasterCard")
    CARTAO_MASTER_CARD("CartaoMasterCard"),
    @XmlEnumValue("CartaoDiners")
    CARTAO_DINERS("CartaoDiners"),
    @XmlEnumValue("CartaoAmericanExpress")
    CARTAO_AMERICAN_EXPRESS("CartaoAmericanExpress"),
    @XmlEnumValue("CartaoHiperCard")
    CARTAO_HIPER_CARD("CartaoHiperCard"),
    @XmlEnumValue("CartaoAura")
    CARTAO_AURA("CartaoAura"),
    @XmlEnumValue("PagamentoEntrega")
    PAGAMENTO_ENTREGA("PagamentoEntrega"),
    @XmlEnumValue("DebitoTransferenciaEletronica")
    DEBITO_TRANSFERENCIA_ELETRONICA("DebitoTransferenciaEletronica"),
    @XmlEnumValue("BoletoBancario")
    BOLETO_BANCARIO("BoletoBancario"),
    @XmlEnumValue("Financiamento")
    FINANCIAMENTO("Financiamento"),
    @XmlEnumValue("Cheque")
    CHEQUE("Cheque"),
    @XmlEnumValue("Deposito")
    DEPOSITO("Deposito"),
    @XmlEnumValue("CartaoMarisa")
    CARTAO_MARISA("CartaoMarisa"),
    @XmlEnumValue("BoletoGlobex")
    BOLETO_GLOBEX("BoletoGlobex"),
    @XmlEnumValue("ValePresente")
    VALE_PRESENTE("ValePresente"),
    @XmlEnumValue("SecureCodeMasterCard")
    SECURE_CODE_MASTER_CARD("SecureCodeMasterCard"),
    @XmlEnumValue("BoletoB2B")
    BOLETO_B_2_B("BoletoB2B"),
    @XmlEnumValue("OiPaggo")
    OI_PAGGO("OiPaggo"),
    @XmlEnumValue("CartaoPontoFrio")
    CARTAO_PONTO_FRIO("CartaoPontoFrio"),
    @XmlEnumValue("MultiCheque")
    MULTI_CHEQUE("MultiCheque"),
    @XmlEnumValue("CartaoPaoAcucar")
    CARTAO_PAO_ACUCAR("CartaoPaoAcucar"),
    @XmlEnumValue("CartaoSoroCred")
    CARTAO_SORO_CRED("CartaoSoroCred"),
    @XmlEnumValue("MultiCash")
    MULTI_CASH("MultiCash"),
    @XmlEnumValue("BoletoAPrazo")
    BOLETO_A_PRAZO("BoletoAPrazo"),
    @XmlEnumValue("ExtraPL")
    EXTRA_PL("ExtraPL"),
    @XmlEnumValue("ExtraMasterCard")
    EXTRA_MASTER_CARD("ExtraMasterCard"),
    @XmlEnumValue("ExtraVisa")
    EXTRA_VISA("ExtraVisa"),
    @XmlEnumValue("PontoCred")
    PONTO_CRED("PontoCred"),
    @XmlEnumValue("Loja")
    LOJA("Loja"),
    @XmlEnumValue("ItauCard")
    ITAU_CARD("ItauCard"),
    @XmlEnumValue("TransferenciaEletronicaBancoBrasil")
    TRANSFERENCIA_ELETRONICA_BANCO_BRASIL("TransferenciaEletronicaBancoBrasil"),
    @XmlEnumValue("TransferenciaEletronicaBradesco")
    TRANSFERENCIA_ELETRONICA_BRADESCO("TransferenciaEletronicaBradesco"),
    @XmlEnumValue("TransferenciaEletronicaItau")
    TRANSFERENCIA_ELETRONICA_ITAU("TransferenciaEletronicaItau"),
    @XmlEnumValue("DebitoContaItau")
    DEBITO_CONTA_ITAU("DebitoContaItau"),
    @XmlEnumValue("CartaoPresenteExtra")
    CARTAO_PRESENTE_EXTRA("CartaoPresenteExtra"),
    @XmlEnumValue("CasasBahiaVisa")
    CASAS_BAHIA_VISA("CasasBahiaVisa"),
    @XmlEnumValue("CasasBahiaMasterCard")
    CASAS_BAHIA_MASTER_CARD("CasasBahiaMasterCard"),
    @XmlEnumValue("BoletoAVistaAtacado")
    BOLETO_A_VISTA_ATACADO("BoletoAVistaAtacado"),
    @XmlEnumValue("PagamentoDigital")
    PAGAMENTO_DIGITAL("PagamentoDigital"),
    @XmlEnumValue("Dinheiro")
    DINHEIRO("Dinheiro"),
    @XmlEnumValue("ValeAlimentacao")
    VALE_ALIMENTACAO("ValeAlimentacao"),
    @XmlEnumValue("PaoDeAcucarMais")
    PAO_DE_ACUCAR_MAIS("PaoDeAcucarMais"),
    @XmlEnumValue("CartaoELO")
    CARTAO_ELO("CartaoELO"),
    @XmlEnumValue("CartaoExtra")
    CARTAO_EXTRA("CartaoExtra"),
    @XmlEnumValue("CartaoSendas")
    CARTAO_SENDAS("CartaoSendas"),
    @XmlEnumValue("Comprebem")
    COMPREBEM("Comprebem"),
    @XmlEnumValue("CartaoFidelidade")
    CARTAO_FIDELIDADE("CartaoFidelidade"),
    @XmlEnumValue("Multiplus")
    MULTIPLUS("Multiplus"),
    @XmlEnumValue("PrePaidCard")
    PRE_PAID_CARD("PrePaidCard"),
    @XmlEnumValue("Alipay")
    ALIPAY("Alipay"),
    @XmlEnumValue("DineroMail")
    DINERO_MAIL("DineroMail"),
    @XmlEnumValue("Cielo")
    CIELO("Cielo"),
    @XmlEnumValue("PayPal")
    PAY_PAL("PayPal"),
    @XmlEnumValue("Cupom")
    CUPOM("Cupom"),
    @XmlEnumValue("FlexCliente")
    FLEX_CLIENTE("FlexCliente");
    private final String value;

    MetodoPagamento(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MetodoPagamento fromValue(String v) {
        for (MetodoPagamento c: MetodoPagamento.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
