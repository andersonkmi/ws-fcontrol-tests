
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsTransacao6 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsTransacao6">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DadosUsuario" type="{http://tempuri.org/}WsUsuario" minOccurs="0"/>
 *         &lt;element name="DadosComprador" type="{http://tempuri.org/}WsComprador" minOccurs="0"/>
 *         &lt;element name="DadosEntrega" type="{http://tempuri.org/}WsEntrega3" minOccurs="0"/>
 *         &lt;element name="Pagamentos" type="{http://tempuri.org/}ArrayOfWsPagamento2" minOccurs="0"/>
 *         &lt;element name="CodigoPedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoPedido2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataCompra" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataEntrega" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="QuantidadeItensDistintos" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="QuantidadeTotalItens" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ValorTotalCompra" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ValorTotalFrete" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PedidoDeTeste" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PrazoEntregaDias" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FormaEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CanalVenda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Produtos" type="{http://tempuri.org/}ArrayOfWsProduto3" minOccurs="0"/>
 *         &lt;element name="DadosExtra" type="{http://tempuri.org/}WsExtra" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsTransacao6", propOrder = {
    "dadosUsuario",
    "dadosComprador",
    "dadosEntrega",
    "pagamentos",
    "codigoPedido",
    "codigoPedido2",
    "dataCompra",
    "dataEntrega",
    "quantidadeItensDistintos",
    "quantidadeTotalItens",
    "valorTotalCompra",
    "valorTotalFrete",
    "pedidoDeTeste",
    "prazoEntregaDias",
    "formaEntrega",
    "observacao",
    "canalVenda",
    "produtos",
    "dadosExtra"
})
public class WsTransacao6 {

    @XmlElement(name = "DadosUsuario")
    protected WsUsuario dadosUsuario;
    @XmlElement(name = "DadosComprador")
    protected WsComprador dadosComprador;
    @XmlElement(name = "DadosEntrega")
    protected WsEntrega3 dadosEntrega;
    @XmlElement(name = "Pagamentos")
    protected ArrayOfWsPagamento2 pagamentos;
    @XmlElement(name = "CodigoPedido")
    protected String codigoPedido;
    @XmlElement(name = "CodigoPedido2")
    protected String codigoPedido2;
    @XmlElement(name = "DataCompra", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCompra;
    @XmlElement(name = "DataEntrega", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEntrega;
    @XmlElement(name = "QuantidadeItensDistintos")
    protected int quantidadeItensDistintos;
    @XmlElement(name = "QuantidadeTotalItens")
    protected int quantidadeTotalItens;
    @XmlElement(name = "ValorTotalCompra")
    protected int valorTotalCompra;
    @XmlElement(name = "ValorTotalFrete")
    protected int valorTotalFrete;
    @XmlElement(name = "PedidoDeTeste")
    protected boolean pedidoDeTeste;
    @XmlElement(name = "PrazoEntregaDias")
    protected int prazoEntregaDias;
    @XmlElement(name = "FormaEntrega")
    protected String formaEntrega;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(name = "CanalVenda")
    protected String canalVenda;
    @XmlElement(name = "Produtos")
    protected ArrayOfWsProduto3 produtos;
    @XmlElement(name = "DadosExtra")
    protected WsExtra dadosExtra;

    /**
     * Gets the value of the dadosUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link WsUsuario }
     *     
     */
    public WsUsuario getDadosUsuario() {
        return dadosUsuario;
    }

    /**
     * Sets the value of the dadosUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsUsuario }
     *     
     */
    public void setDadosUsuario(WsUsuario value) {
        this.dadosUsuario = value;
    }

    /**
     * Gets the value of the dadosComprador property.
     * 
     * @return
     *     possible object is
     *     {@link WsComprador }
     *     
     */
    public WsComprador getDadosComprador() {
        return dadosComprador;
    }

    /**
     * Sets the value of the dadosComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsComprador }
     *     
     */
    public void setDadosComprador(WsComprador value) {
        this.dadosComprador = value;
    }

    /**
     * Gets the value of the dadosEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link WsEntrega3 }
     *     
     */
    public WsEntrega3 getDadosEntrega() {
        return dadosEntrega;
    }

    /**
     * Sets the value of the dadosEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsEntrega3 }
     *     
     */
    public void setDadosEntrega(WsEntrega3 value) {
        this.dadosEntrega = value;
    }

    /**
     * Gets the value of the pagamentos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsPagamento2 }
     *     
     */
    public ArrayOfWsPagamento2 getPagamentos() {
        return pagamentos;
    }

    /**
     * Sets the value of the pagamentos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsPagamento2 }
     *     
     */
    public void setPagamentos(ArrayOfWsPagamento2 value) {
        this.pagamentos = value;
    }

    /**
     * Gets the value of the codigoPedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPedido() {
        return codigoPedido;
    }

    /**
     * Sets the value of the codigoPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPedido(String value) {
        this.codigoPedido = value;
    }

    /**
     * Gets the value of the codigoPedido2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPedido2() {
        return codigoPedido2;
    }

    /**
     * Sets the value of the codigoPedido2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPedido2(String value) {
        this.codigoPedido2 = value;
    }

    /**
     * Gets the value of the dataCompra property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCompra() {
        return dataCompra;
    }

    /**
     * Sets the value of the dataCompra property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCompra(XMLGregorianCalendar value) {
        this.dataCompra = value;
    }

    /**
     * Gets the value of the dataEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEntrega() {
        return dataEntrega;
    }

    /**
     * Sets the value of the dataEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEntrega(XMLGregorianCalendar value) {
        this.dataEntrega = value;
    }

    /**
     * Gets the value of the quantidadeItensDistintos property.
     * 
     */
    public int getQuantidadeItensDistintos() {
        return quantidadeItensDistintos;
    }

    /**
     * Sets the value of the quantidadeItensDistintos property.
     * 
     */
    public void setQuantidadeItensDistintos(int value) {
        this.quantidadeItensDistintos = value;
    }

    /**
     * Gets the value of the quantidadeTotalItens property.
     * 
     */
    public int getQuantidadeTotalItens() {
        return quantidadeTotalItens;
    }

    /**
     * Sets the value of the quantidadeTotalItens property.
     * 
     */
    public void setQuantidadeTotalItens(int value) {
        this.quantidadeTotalItens = value;
    }

    /**
     * Gets the value of the valorTotalCompra property.
     * 
     */
    public int getValorTotalCompra() {
        return valorTotalCompra;
    }

    /**
     * Sets the value of the valorTotalCompra property.
     * 
     */
    public void setValorTotalCompra(int value) {
        this.valorTotalCompra = value;
    }

    /**
     * Gets the value of the valorTotalFrete property.
     * 
     */
    public int getValorTotalFrete() {
        return valorTotalFrete;
    }

    /**
     * Sets the value of the valorTotalFrete property.
     * 
     */
    public void setValorTotalFrete(int value) {
        this.valorTotalFrete = value;
    }

    /**
     * Gets the value of the pedidoDeTeste property.
     * 
     */
    public boolean isPedidoDeTeste() {
        return pedidoDeTeste;
    }

    /**
     * Sets the value of the pedidoDeTeste property.
     * 
     */
    public void setPedidoDeTeste(boolean value) {
        this.pedidoDeTeste = value;
    }

    /**
     * Gets the value of the prazoEntregaDias property.
     * 
     */
    public int getPrazoEntregaDias() {
        return prazoEntregaDias;
    }

    /**
     * Sets the value of the prazoEntregaDias property.
     * 
     */
    public void setPrazoEntregaDias(int value) {
        this.prazoEntregaDias = value;
    }

    /**
     * Gets the value of the formaEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaEntrega() {
        return formaEntrega;
    }

    /**
     * Sets the value of the formaEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaEntrega(String value) {
        this.formaEntrega = value;
    }

    /**
     * Gets the value of the observacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Sets the value of the observacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Gets the value of the canalVenda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanalVenda() {
        return canalVenda;
    }

    /**
     * Sets the value of the canalVenda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanalVenda(String value) {
        this.canalVenda = value;
    }

    /**
     * Gets the value of the produtos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsProduto3 }
     *     
     */
    public ArrayOfWsProduto3 getProdutos() {
        return produtos;
    }

    /**
     * Sets the value of the produtos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsProduto3 }
     *     
     */
    public void setProdutos(ArrayOfWsProduto3 value) {
        this.produtos = value;
    }

    /**
     * Gets the value of the dadosExtra property.
     * 
     * @return
     *     possible object is
     *     {@link WsExtra }
     *     
     */
    public WsExtra getDadosExtra() {
        return dadosExtra;
    }

    /**
     * Sets the value of the dadosExtra property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsExtra }
     *     
     */
    public void setDadosExtra(WsExtra value) {
        this.dadosExtra = value;
    }

}
