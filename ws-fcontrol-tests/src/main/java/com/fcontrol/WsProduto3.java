
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsProduto3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsProduto3">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsProduto2">
 *       &lt;sequence>
 *         &lt;element name="ParaPresente" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsProduto3", propOrder = {
    "paraPresente"
})
@XmlSeeAlso({
    WsProduto4 .class
})
public class WsProduto3
    extends WsProduto2
{

    @XmlElement(name = "ParaPresente")
    protected boolean paraPresente;

    /**
     * Gets the value of the paraPresente property.
     * 
     */
    public boolean isParaPresente() {
        return paraPresente;
    }

    /**
     * Sets the value of the paraPresente property.
     * 
     */
    public void setParaPresente(boolean value) {
        this.paraPresente = value;
    }

}
