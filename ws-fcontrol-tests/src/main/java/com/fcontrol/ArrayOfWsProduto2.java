
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsProduto2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsProduto2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsProduto2" type="{http://tempuri.org/}WsProduto2" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsProduto2", propOrder = {
    "wsProduto2"
})
public class ArrayOfWsProduto2 {

    @XmlElement(name = "WsProduto2", nillable = true)
    protected List<WsProduto2> wsProduto2;

    /**
     * Gets the value of the wsProduto2 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsProduto2 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsProduto2().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsProduto2 }
     * 
     * 
     */
    public List<WsProduto2> getWsProduto2() {
        if (wsProduto2 == null) {
            wsProduto2 = new ArrayList<WsProduto2>();
        }
        return this.wsProduto2;
    }

}
