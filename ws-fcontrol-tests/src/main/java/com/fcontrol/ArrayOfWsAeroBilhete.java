
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsAeroBilhete complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsAeroBilhete">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsAeroBilhete" type="{http://tempuri.org/}WsAeroBilhete" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsAeroBilhete", propOrder = {
    "wsAeroBilhete"
})
public class ArrayOfWsAeroBilhete {

    @XmlElement(name = "WsAeroBilhete", nillable = true)
    protected List<WsAeroBilhete> wsAeroBilhete;

    /**
     * Gets the value of the wsAeroBilhete property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsAeroBilhete property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsAeroBilhete().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsAeroBilhete }
     * 
     * 
     */
    public List<WsAeroBilhete> getWsAeroBilhete() {
        if (wsAeroBilhete == null) {
            wsAeroBilhete = new ArrayList<WsAeroBilhete>();
        }
        return this.wsAeroBilhete;
    }

}
