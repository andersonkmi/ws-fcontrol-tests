
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadosTodasSubLojasResult" type="{http://tempuri.org/}ArrayOfWsAnaliseTodasSublojas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadosTodasSubLojasResult"
})
@XmlRootElement(name = "capturarResultadosTodasSubLojasResponse")
public class CapturarResultadosTodasSubLojasResponse {

    protected ArrayOfWsAnaliseTodasSublojas capturarResultadosTodasSubLojasResult;

    /**
     * Gets the value of the capturarResultadosTodasSubLojasResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsAnaliseTodasSublojas }
     *     
     */
    public ArrayOfWsAnaliseTodasSublojas getCapturarResultadosTodasSubLojasResult() {
        return capturarResultadosTodasSubLojasResult;
    }

    /**
     * Sets the value of the capturarResultadosTodasSubLojasResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsAnaliseTodasSublojas }
     *     
     */
    public void setCapturarResultadosTodasSubLojasResult(ArrayOfWsAnaliseTodasSublojas value) {
        this.capturarResultadosTodasSubLojasResult = value;
    }

}
