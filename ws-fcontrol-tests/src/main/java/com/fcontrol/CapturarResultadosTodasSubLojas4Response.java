
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadosTodasSubLojas4Result" type="{http://tempuri.org/}ArrayOfWsAnaliseTodasSublojas3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadosTodasSubLojas4Result"
})
@XmlRootElement(name = "capturarResultadosTodasSubLojas4Response")
public class CapturarResultadosTodasSubLojas4Response {

    protected ArrayOfWsAnaliseTodasSublojas3 capturarResultadosTodasSubLojas4Result;

    /**
     * Gets the value of the capturarResultadosTodasSubLojas4Result property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsAnaliseTodasSublojas3 }
     *     
     */
    public ArrayOfWsAnaliseTodasSublojas3 getCapturarResultadosTodasSubLojas4Result() {
        return capturarResultadosTodasSubLojas4Result;
    }

    /**
     * Sets the value of the capturarResultadosTodasSubLojas4Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsAnaliseTodasSublojas3 }
     *     
     */
    public void setCapturarResultadosTodasSubLojas4Result(ArrayOfWsAnaliseTodasSublojas3 value) {
        this.capturarResultadosTodasSubLojas4Result = value;
    }

}
