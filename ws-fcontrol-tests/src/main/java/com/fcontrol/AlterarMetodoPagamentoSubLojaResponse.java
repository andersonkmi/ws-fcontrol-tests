
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="alterarMetodoPagamentoSubLojaResult" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "alterarMetodoPagamentoSubLojaResult"
})
@XmlRootElement(name = "alterarMetodoPagamentoSubLojaResponse")
public class AlterarMetodoPagamentoSubLojaResponse {

    protected WsResultado alterarMetodoPagamentoSubLojaResult;

    /**
     * Gets the value of the alterarMetodoPagamentoSubLojaResult property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getAlterarMetodoPagamentoSubLojaResult() {
        return alterarMetodoPagamentoSubLojaResult;
    }

    /**
     * Sets the value of the alterarMetodoPagamentoSubLojaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setAlterarMetodoPagamentoSubLojaResult(WsResultado value) {
        this.alterarMetodoPagamentoSubLojaResult = value;
    }

}
