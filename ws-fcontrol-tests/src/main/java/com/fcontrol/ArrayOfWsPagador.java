
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsPagador complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsPagador">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsPagador" type="{http://tempuri.org/}WsPagador" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsPagador", propOrder = {
    "wsPagador"
})
public class ArrayOfWsPagador {

    @XmlElement(name = "WsPagador", nillable = true)
    protected List<WsPagador> wsPagador;

    /**
     * Gets the value of the wsPagador property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsPagador property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsPagador().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsPagador }
     * 
     * 
     */
    public List<WsPagador> getWsPagador() {
        if (wsPagador == null) {
            wsPagador = new ArrayList<WsPagador>();
        }
        return this.wsPagador;
    }

}
