
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsProdutoTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsProdutoTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Linha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Descontinuado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="GrupoReclamacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reclamacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CondicaoUso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Situacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoteComDefeito" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DataValidadeLote" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="MotivoLoteEmBranco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuantidadeReclamada" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="QuantidadeRessarcir" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Destino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrecoNaoPadrao" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Alterado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Ressarcir" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsProdutoTroca", propOrder = {
    "codigo",
    "linha",
    "descricao",
    "descontinuado",
    "grupoReclamacao",
    "reclamacao",
    "condicaoUso",
    "situacao",
    "lote",
    "loteComDefeito",
    "dataValidadeLote",
    "motivoLoteEmBranco",
    "quantidadeReclamada",
    "quantidadeRessarcir",
    "destino",
    "precoNaoPadrao",
    "alterado",
    "ressarcir"
})
public class WsProdutoTroca {

    @XmlElement(name = "Codigo")
    protected String codigo;
    @XmlElement(name = "Linha")
    protected String linha;
    @XmlElement(name = "Descricao")
    protected String descricao;
    @XmlElement(name = "Descontinuado")
    protected boolean descontinuado;
    @XmlElement(name = "GrupoReclamacao")
    protected String grupoReclamacao;
    @XmlElement(name = "Reclamacao")
    protected String reclamacao;
    @XmlElement(name = "CondicaoUso")
    protected String condicaoUso;
    @XmlElement(name = "Situacao")
    protected String situacao;
    @XmlElement(name = "Lote")
    protected String lote;
    @XmlElement(name = "LoteComDefeito")
    protected boolean loteComDefeito;
    @XmlElement(name = "DataValidadeLote", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataValidadeLote;
    @XmlElement(name = "MotivoLoteEmBranco")
    protected String motivoLoteEmBranco;
    @XmlElement(name = "QuantidadeReclamada")
    protected int quantidadeReclamada;
    @XmlElement(name = "QuantidadeRessarcir")
    protected int quantidadeRessarcir;
    @XmlElement(name = "Destino")
    protected String destino;
    @XmlElement(name = "PrecoNaoPadrao")
    protected boolean precoNaoPadrao;
    @XmlElement(name = "Alterado")
    protected boolean alterado;
    @XmlElement(name = "Ressarcir")
    protected boolean ressarcir;

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the linha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinha() {
        return linha;
    }

    /**
     * Sets the value of the linha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinha(String value) {
        this.linha = value;
    }

    /**
     * Gets the value of the descricao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Sets the value of the descricao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Gets the value of the descontinuado property.
     * 
     */
    public boolean isDescontinuado() {
        return descontinuado;
    }

    /**
     * Sets the value of the descontinuado property.
     * 
     */
    public void setDescontinuado(boolean value) {
        this.descontinuado = value;
    }

    /**
     * Gets the value of the grupoReclamacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupoReclamacao() {
        return grupoReclamacao;
    }

    /**
     * Sets the value of the grupoReclamacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupoReclamacao(String value) {
        this.grupoReclamacao = value;
    }

    /**
     * Gets the value of the reclamacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReclamacao() {
        return reclamacao;
    }

    /**
     * Sets the value of the reclamacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReclamacao(String value) {
        this.reclamacao = value;
    }

    /**
     * Gets the value of the condicaoUso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCondicaoUso() {
        return condicaoUso;
    }

    /**
     * Sets the value of the condicaoUso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCondicaoUso(String value) {
        this.condicaoUso = value;
    }

    /**
     * Gets the value of the situacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * Sets the value of the situacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSituacao(String value) {
        this.situacao = value;
    }

    /**
     * Gets the value of the lote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLote() {
        return lote;
    }

    /**
     * Sets the value of the lote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLote(String value) {
        this.lote = value;
    }

    /**
     * Gets the value of the loteComDefeito property.
     * 
     */
    public boolean isLoteComDefeito() {
        return loteComDefeito;
    }

    /**
     * Sets the value of the loteComDefeito property.
     * 
     */
    public void setLoteComDefeito(boolean value) {
        this.loteComDefeito = value;
    }

    /**
     * Gets the value of the dataValidadeLote property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataValidadeLote() {
        return dataValidadeLote;
    }

    /**
     * Sets the value of the dataValidadeLote property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataValidadeLote(XMLGregorianCalendar value) {
        this.dataValidadeLote = value;
    }

    /**
     * Gets the value of the motivoLoteEmBranco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoLoteEmBranco() {
        return motivoLoteEmBranco;
    }

    /**
     * Sets the value of the motivoLoteEmBranco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoLoteEmBranco(String value) {
        this.motivoLoteEmBranco = value;
    }

    /**
     * Gets the value of the quantidadeReclamada property.
     * 
     */
    public int getQuantidadeReclamada() {
        return quantidadeReclamada;
    }

    /**
     * Sets the value of the quantidadeReclamada property.
     * 
     */
    public void setQuantidadeReclamada(int value) {
        this.quantidadeReclamada = value;
    }

    /**
     * Gets the value of the quantidadeRessarcir property.
     * 
     */
    public int getQuantidadeRessarcir() {
        return quantidadeRessarcir;
    }

    /**
     * Sets the value of the quantidadeRessarcir property.
     * 
     */
    public void setQuantidadeRessarcir(int value) {
        this.quantidadeRessarcir = value;
    }

    /**
     * Gets the value of the destino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestino() {
        return destino;
    }

    /**
     * Sets the value of the destino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestino(String value) {
        this.destino = value;
    }

    /**
     * Gets the value of the precoNaoPadrao property.
     * 
     */
    public boolean isPrecoNaoPadrao() {
        return precoNaoPadrao;
    }

    /**
     * Sets the value of the precoNaoPadrao property.
     * 
     */
    public void setPrecoNaoPadrao(boolean value) {
        this.precoNaoPadrao = value;
    }

    /**
     * Gets the value of the alterado property.
     * 
     */
    public boolean isAlterado() {
        return alterado;
    }

    /**
     * Sets the value of the alterado property.
     * 
     */
    public void setAlterado(boolean value) {
        this.alterado = value;
    }

    /**
     * Gets the value of the ressarcir property.
     * 
     */
    public boolean isRessarcir() {
        return ressarcir;
    }

    /**
     * Sets the value of the ressarcir property.
     * 
     */
    public void setRessarcir(boolean value) {
        this.ressarcir = value;
    }

}
