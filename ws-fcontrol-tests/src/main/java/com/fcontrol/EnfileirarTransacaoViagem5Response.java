
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enfileirarTransacaoViagem5Result" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enfileirarTransacaoViagem5Result"
})
@XmlRootElement(name = "enfileirarTransacaoViagem5Response")
public class EnfileirarTransacaoViagem5Response {

    protected WsResultado enfileirarTransacaoViagem5Result;

    /**
     * Gets the value of the enfileirarTransacaoViagem5Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getEnfileirarTransacaoViagem5Result() {
        return enfileirarTransacaoViagem5Result;
    }

    /**
     * Sets the value of the enfileirarTransacaoViagem5Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setEnfileirarTransacaoViagem5Result(WsResultado value) {
        this.enfileirarTransacaoViagem5Result = value;
    }

}
