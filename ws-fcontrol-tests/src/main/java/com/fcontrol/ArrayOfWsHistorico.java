
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsHistorico complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsHistorico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsHistorico" type="{http://tempuri.org/}WsHistorico" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsHistorico", propOrder = {
    "wsHistorico"
})
public class ArrayOfWsHistorico {

    @XmlElement(name = "WsHistorico", nillable = true)
    protected List<WsHistorico> wsHistorico;

    /**
     * Gets the value of the wsHistorico property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsHistorico property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsHistorico().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsHistorico }
     * 
     * 
     */
    public List<WsHistorico> getWsHistorico() {
        if (wsHistorico == null) {
            wsHistorico = new ArrayList<WsHistorico>();
        }
        return this.wsHistorico;
    }

}
