
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarStatusAtualResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarStatusAtualResult"
})
@XmlRootElement(name = "capturarStatusAtualResponse")
public class CapturarStatusAtualResponse {

    protected int capturarStatusAtualResult;

    /**
     * Gets the value of the capturarStatusAtualResult property.
     * 
     */
    public int getCapturarStatusAtualResult() {
        return capturarStatusAtualResult;
    }

    /**
     * Sets the value of the capturarStatusAtualResult property.
     * 
     */
    public void setCapturarStatusAtualResult(int value) {
        this.capturarStatusAtualResult = value;
    }

}
