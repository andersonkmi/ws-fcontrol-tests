
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="denunciarSubLoja2Result" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "denunciarSubLoja2Result"
})
@XmlRootElement(name = "denunciarSubLoja2Response")
public class DenunciarSubLoja2Response {

    protected WsResultado denunciarSubLoja2Result;

    /**
     * Gets the value of the denunciarSubLoja2Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getDenunciarSubLoja2Result() {
        return denunciarSubLoja2Result;
    }

    /**
     * Sets the value of the denunciarSubLoja2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setDenunciarSubLoja2Result(WsResultado value) {
        this.denunciarSubLoja2Result = value;
    }

}
