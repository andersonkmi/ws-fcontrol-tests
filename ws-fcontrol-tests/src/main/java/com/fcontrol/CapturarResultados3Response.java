
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultados3Result" type="{http://tempuri.org/}ArrayOfWsAnalise3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultados3Result"
})
@XmlRootElement(name = "capturarResultados3Response")
public class CapturarResultados3Response {

    protected ArrayOfWsAnalise3 capturarResultados3Result;

    /**
     * Gets the value of the capturarResultados3Result property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsAnalise3 }
     *     
     */
    public ArrayOfWsAnalise3 getCapturarResultados3Result() {
        return capturarResultados3Result;
    }

    /**
     * Sets the value of the capturarResultados3Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsAnalise3 }
     *     
     */
    public void setCapturarResultados3Result(ArrayOfWsAnalise3 value) {
        this.capturarResultados3Result = value;
    }

}
