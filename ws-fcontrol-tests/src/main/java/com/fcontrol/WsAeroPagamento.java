
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsAeroPagamento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAeroPagamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumeroCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CpfTitularCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NomeTitularCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataValidadeCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnderecoFaturaCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CepFaturaCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataNascimentoTitular" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="NumeroDeParcelas" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MetodoPagamento" type="{http://tempuri.org/}MetodoPagamento"/>
 *         &lt;element name="ValorTotalCompra" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAeroPagamento", propOrder = {
    "numeroCartao",
    "cpfTitularCartao",
    "nomeTitularCartao",
    "dataValidadeCartao",
    "enderecoFaturaCartao",
    "cepFaturaCartao",
    "dataNascimentoTitular",
    "numeroDeParcelas",
    "metodoPagamento",
    "valorTotalCompra"
})
@XmlSeeAlso({
    WsAeroPagamento2 .class
})
public class WsAeroPagamento {

    @XmlElement(name = "NumeroCartao")
    protected String numeroCartao;
    @XmlElement(name = "CpfTitularCartao")
    protected String cpfTitularCartao;
    @XmlElement(name = "NomeTitularCartao")
    protected String nomeTitularCartao;
    @XmlElement(name = "DataValidadeCartao")
    protected String dataValidadeCartao;
    @XmlElement(name = "EnderecoFaturaCartao")
    protected String enderecoFaturaCartao;
    @XmlElement(name = "CepFaturaCartao")
    protected String cepFaturaCartao;
    @XmlElement(name = "DataNascimentoTitular", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataNascimentoTitular;
    @XmlElement(name = "NumeroDeParcelas")
    protected int numeroDeParcelas;
    @XmlElement(name = "MetodoPagamento", required = true)
    @XmlSchemaType(name = "string")
    protected MetodoPagamento metodoPagamento;
    @XmlElement(name = "ValorTotalCompra")
    protected int valorTotalCompra;

    /**
     * Gets the value of the numeroCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCartao() {
        return numeroCartao;
    }

    /**
     * Sets the value of the numeroCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCartao(String value) {
        this.numeroCartao = value;
    }

    /**
     * Gets the value of the cpfTitularCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfTitularCartao() {
        return cpfTitularCartao;
    }

    /**
     * Sets the value of the cpfTitularCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfTitularCartao(String value) {
        this.cpfTitularCartao = value;
    }

    /**
     * Gets the value of the nomeTitularCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeTitularCartao() {
        return nomeTitularCartao;
    }

    /**
     * Sets the value of the nomeTitularCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeTitularCartao(String value) {
        this.nomeTitularCartao = value;
    }

    /**
     * Gets the value of the dataValidadeCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataValidadeCartao() {
        return dataValidadeCartao;
    }

    /**
     * Sets the value of the dataValidadeCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataValidadeCartao(String value) {
        this.dataValidadeCartao = value;
    }

    /**
     * Gets the value of the enderecoFaturaCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnderecoFaturaCartao() {
        return enderecoFaturaCartao;
    }

    /**
     * Sets the value of the enderecoFaturaCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnderecoFaturaCartao(String value) {
        this.enderecoFaturaCartao = value;
    }

    /**
     * Gets the value of the cepFaturaCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCepFaturaCartao() {
        return cepFaturaCartao;
    }

    /**
     * Sets the value of the cepFaturaCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCepFaturaCartao(String value) {
        this.cepFaturaCartao = value;
    }

    /**
     * Gets the value of the dataNascimentoTitular property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataNascimentoTitular() {
        return dataNascimentoTitular;
    }

    /**
     * Sets the value of the dataNascimentoTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataNascimentoTitular(XMLGregorianCalendar value) {
        this.dataNascimentoTitular = value;
    }

    /**
     * Gets the value of the numeroDeParcelas property.
     * 
     */
    public int getNumeroDeParcelas() {
        return numeroDeParcelas;
    }

    /**
     * Sets the value of the numeroDeParcelas property.
     * 
     */
    public void setNumeroDeParcelas(int value) {
        this.numeroDeParcelas = value;
    }

    /**
     * Gets the value of the metodoPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link MetodoPagamento }
     *     
     */
    public MetodoPagamento getMetodoPagamento() {
        return metodoPagamento;
    }

    /**
     * Sets the value of the metodoPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link MetodoPagamento }
     *     
     */
    public void setMetodoPagamento(MetodoPagamento value) {
        this.metodoPagamento = value;
    }

    /**
     * Gets the value of the valorTotalCompra property.
     * 
     */
    public int getValorTotalCompra() {
        return valorTotalCompra;
    }

    /**
     * Sets the value of the valorTotalCompra property.
     * 
     */
    public void setValorTotalCompra(int value) {
        this.valorTotalCompra = value;
    }

}
