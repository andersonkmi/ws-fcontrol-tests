
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsPagamentoTelecom complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsPagamentoTelecom">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DadosCartao" type="{http://tempuri.org/}WsCartaoTelecom" minOccurs="0"/>
 *         &lt;element name="MetodoPagamento" type="{http://tempuri.org/}MetodoPagamento"/>
 *         &lt;element name="RecargaAutomatica" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsPagamentoTelecom", propOrder = {
    "dadosCartao",
    "metodoPagamento",
    "recargaAutomatica"
})
public class WsPagamentoTelecom {

    @XmlElement(name = "DadosCartao")
    protected WsCartaoTelecom dadosCartao;
    @XmlElement(name = "MetodoPagamento", required = true)
    @XmlSchemaType(name = "string")
    protected MetodoPagamento metodoPagamento;
    @XmlElement(name = "RecargaAutomatica")
    protected boolean recargaAutomatica;

    /**
     * Gets the value of the dadosCartao property.
     * 
     * @return
     *     possible object is
     *     {@link WsCartaoTelecom }
     *     
     */
    public WsCartaoTelecom getDadosCartao() {
        return dadosCartao;
    }

    /**
     * Sets the value of the dadosCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsCartaoTelecom }
     *     
     */
    public void setDadosCartao(WsCartaoTelecom value) {
        this.dadosCartao = value;
    }

    /**
     * Gets the value of the metodoPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link MetodoPagamento }
     *     
     */
    public MetodoPagamento getMetodoPagamento() {
        return metodoPagamento;
    }

    /**
     * Sets the value of the metodoPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link MetodoPagamento }
     *     
     */
    public void setMetodoPagamento(MetodoPagamento value) {
        this.metodoPagamento = value;
    }

    /**
     * Gets the value of the recargaAutomatica property.
     * 
     */
    public boolean isRecargaAutomatica() {
        return recargaAutomatica;
    }

    /**
     * Sets the value of the recargaAutomatica property.
     * 
     */
    public void setRecargaAutomatica(boolean value) {
        this.recargaAutomatica = value;
    }

}
