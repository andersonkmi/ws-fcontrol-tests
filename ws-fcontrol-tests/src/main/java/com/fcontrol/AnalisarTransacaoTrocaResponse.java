
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="analisarTransacaoTrocaResult" type="{http://tempuri.org/}WsRetornoTroca" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarTransacaoTrocaResult"
})
@XmlRootElement(name = "analisarTransacaoTrocaResponse")
public class AnalisarTransacaoTrocaResponse {

    protected WsRetornoTroca analisarTransacaoTrocaResult;

    /**
     * Gets the value of the analisarTransacaoTrocaResult property.
     * 
     * @return
     *     possible object is
     *     {@link WsRetornoTroca }
     *     
     */
    public WsRetornoTroca getAnalisarTransacaoTrocaResult() {
        return analisarTransacaoTrocaResult;
    }

    /**
     * Sets the value of the analisarTransacaoTrocaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsRetornoTroca }
     *     
     */
    public void setAnalisarTransacaoTrocaResult(WsRetornoTroca value) {
        this.analisarTransacaoTrocaResult = value;
    }

}
