
package com.fcontrol;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Status.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Status">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Nulo"/>
 *     &lt;enumeration value="Pendente"/>
 *     &lt;enumeration value="Enviado"/>
 *     &lt;enumeration value="Cancelado"/>
 *     &lt;enumeration value="AguardandoDocumentacao"/>
 *     &lt;enumeration value="CanceladoSuspeita"/>
 *     &lt;enumeration value="Aprovada"/>
 *     &lt;enumeration value="EmEspera"/>
 *     &lt;enumeration value="SolicitadaSupervisao"/>
 *     &lt;enumeration value="FraudeConfirmada"/>
 *     &lt;enumeration value="EmRecuperacaoPerda"/>
 *     &lt;enumeration value="Recuperado"/>
 *     &lt;enumeration value="DesaprovadoOperadora"/>
 *     &lt;enumeration value="Descancelado"/>
 *     &lt;enumeration value="CanceladoSemSuspeita"/>
 *     &lt;enumeration value="ReAnalise"/>
 *     &lt;enumeration value="AguardandoDocumentacaoFilaGeral"/>
 *     &lt;enumeration value="SolicitadoContato"/>
 *     &lt;enumeration value="ContatoEfetuado"/>
 *     &lt;enumeration value="EmRecuperacaoVenda"/>
 *     &lt;enumeration value="VendaRecuperada"/>
 *     &lt;enumeration value="EmRecuperacaoVendaAgendada"/>
 *     &lt;enumeration value="AprovadoOperadora"/>
 *     &lt;enumeration value="PreAprovado"/>
 *     &lt;enumeration value="PreCancelado"/>
 *     &lt;enumeration value="PreCanceladoSuspeita"/>
 *     &lt;enumeration value="PreFraudeConfirmada"/>
 *     &lt;enumeration value="AnalisePrioritaria"/>
 *     &lt;enumeration value="Redistribuido"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Status")
@XmlEnum
public enum Status {

    @XmlEnumValue("Nulo")
    NULO("Nulo"),
    @XmlEnumValue("Pendente")
    PENDENTE("Pendente"),
    @XmlEnumValue("Enviado")
    ENVIADO("Enviado"),
    @XmlEnumValue("Cancelado")
    CANCELADO("Cancelado"),
    @XmlEnumValue("AguardandoDocumentacao")
    AGUARDANDO_DOCUMENTACAO("AguardandoDocumentacao"),
    @XmlEnumValue("CanceladoSuspeita")
    CANCELADO_SUSPEITA("CanceladoSuspeita"),
    @XmlEnumValue("Aprovada")
    APROVADA("Aprovada"),
    @XmlEnumValue("EmEspera")
    EM_ESPERA("EmEspera"),
    @XmlEnumValue("SolicitadaSupervisao")
    SOLICITADA_SUPERVISAO("SolicitadaSupervisao"),
    @XmlEnumValue("FraudeConfirmada")
    FRAUDE_CONFIRMADA("FraudeConfirmada"),
    @XmlEnumValue("EmRecuperacaoPerda")
    EM_RECUPERACAO_PERDA("EmRecuperacaoPerda"),
    @XmlEnumValue("Recuperado")
    RECUPERADO("Recuperado"),
    @XmlEnumValue("DesaprovadoOperadora")
    DESAPROVADO_OPERADORA("DesaprovadoOperadora"),
    @XmlEnumValue("Descancelado")
    DESCANCELADO("Descancelado"),
    @XmlEnumValue("CanceladoSemSuspeita")
    CANCELADO_SEM_SUSPEITA("CanceladoSemSuspeita"),
    @XmlEnumValue("ReAnalise")
    RE_ANALISE("ReAnalise"),
    @XmlEnumValue("AguardandoDocumentacaoFilaGeral")
    AGUARDANDO_DOCUMENTACAO_FILA_GERAL("AguardandoDocumentacaoFilaGeral"),
    @XmlEnumValue("SolicitadoContato")
    SOLICITADO_CONTATO("SolicitadoContato"),
    @XmlEnumValue("ContatoEfetuado")
    CONTATO_EFETUADO("ContatoEfetuado"),
    @XmlEnumValue("EmRecuperacaoVenda")
    EM_RECUPERACAO_VENDA("EmRecuperacaoVenda"),
    @XmlEnumValue("VendaRecuperada")
    VENDA_RECUPERADA("VendaRecuperada"),
    @XmlEnumValue("EmRecuperacaoVendaAgendada")
    EM_RECUPERACAO_VENDA_AGENDADA("EmRecuperacaoVendaAgendada"),
    @XmlEnumValue("AprovadoOperadora")
    APROVADO_OPERADORA("AprovadoOperadora"),
    @XmlEnumValue("PreAprovado")
    PRE_APROVADO("PreAprovado"),
    @XmlEnumValue("PreCancelado")
    PRE_CANCELADO("PreCancelado"),
    @XmlEnumValue("PreCanceladoSuspeita")
    PRE_CANCELADO_SUSPEITA("PreCanceladoSuspeita"),
    @XmlEnumValue("PreFraudeConfirmada")
    PRE_FRAUDE_CONFIRMADA("PreFraudeConfirmada"),
    @XmlEnumValue("AnalisePrioritaria")
    ANALISE_PRIORITARIA("AnalisePrioritaria"),
    @XmlEnumValue("Redistribuido")
    REDISTRIBUIDO("Redistribuido");
    private final String value;

    Status(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Status fromValue(String v) {
        for (Status c: Status.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
