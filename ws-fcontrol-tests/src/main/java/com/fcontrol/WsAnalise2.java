
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsAnalise2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAnalise2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoCompra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CodigoMotivo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Comentario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Analista" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ramal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Telefone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OpiniaoFcontrol" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Score" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAnalise2", propOrder = {
    "codigoCompra",
    "status",
    "codigoMotivo",
    "comentario",
    "analista",
    "email",
    "ramal",
    "telefone",
    "opiniaoFcontrol",
    "score"
})
@XmlSeeAlso({
    WsAnalise3 .class
})
public class WsAnalise2 {

    @XmlElement(name = "CodigoCompra")
    protected String codigoCompra;
    @XmlElement(name = "Status")
    protected int status;
    @XmlElement(name = "CodigoMotivo")
    protected int codigoMotivo;
    @XmlElement(name = "Comentario")
    protected String comentario;
    @XmlElement(name = "Analista")
    protected String analista;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "Ramal")
    protected String ramal;
    @XmlElement(name = "Telefone")
    protected String telefone;
    @XmlElement(name = "OpiniaoFcontrol")
    protected int opiniaoFcontrol;
    @XmlElement(name = "Score")
    protected int score;

    /**
     * Gets the value of the codigoCompra property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCompra() {
        return codigoCompra;
    }

    /**
     * Sets the value of the codigoCompra property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCompra(String value) {
        this.codigoCompra = value;
    }

    /**
     * Gets the value of the status property.
     * 
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     */
    public void setStatus(int value) {
        this.status = value;
    }

    /**
     * Gets the value of the codigoMotivo property.
     * 
     */
    public int getCodigoMotivo() {
        return codigoMotivo;
    }

    /**
     * Sets the value of the codigoMotivo property.
     * 
     */
    public void setCodigoMotivo(int value) {
        this.codigoMotivo = value;
    }

    /**
     * Gets the value of the comentario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * Sets the value of the comentario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComentario(String value) {
        this.comentario = value;
    }

    /**
     * Gets the value of the analista property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalista() {
        return analista;
    }

    /**
     * Sets the value of the analista property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalista(String value) {
        this.analista = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the ramal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRamal() {
        return ramal;
    }

    /**
     * Sets the value of the ramal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRamal(String value) {
        this.ramal = value;
    }

    /**
     * Gets the value of the telefone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * Sets the value of the telefone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefone(String value) {
        this.telefone = value;
    }

    /**
     * Gets the value of the opiniaoFcontrol property.
     * 
     */
    public int getOpiniaoFcontrol() {
        return opiniaoFcontrol;
    }

    /**
     * Sets the value of the opiniaoFcontrol property.
     * 
     */
    public void setOpiniaoFcontrol(int value) {
        this.opiniaoFcontrol = value;
    }

    /**
     * Gets the value of the score property.
     * 
     */
    public int getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     */
    public void setScore(int value) {
        this.score = value;
    }

}
