
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="desconfirmarRetornoResult" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "desconfirmarRetornoResult"
})
@XmlRootElement(name = "desconfirmarRetornoResponse")
public class DesconfirmarRetornoResponse {

    protected WsResultado desconfirmarRetornoResult;

    /**
     * Gets the value of the desconfirmarRetornoResult property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getDesconfirmarRetornoResult() {
        return desconfirmarRetornoResult;
    }

    /**
     * Sets the value of the desconfirmarRetornoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setDesconfirmarRetornoResult(WsResultado value) {
        this.desconfirmarRetornoResult = value;
    }

}
