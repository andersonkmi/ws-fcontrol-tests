
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadosTodasSubLojas3Result" type="{http://tempuri.org/}ArrayOfWsAnaliseTodasSublojas2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadosTodasSubLojas3Result"
})
@XmlRootElement(name = "capturarResultadosTodasSubLojas3Response")
public class CapturarResultadosTodasSubLojas3Response {

    protected ArrayOfWsAnaliseTodasSublojas2 capturarResultadosTodasSubLojas3Result;

    /**
     * Gets the value of the capturarResultadosTodasSubLojas3Result property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsAnaliseTodasSublojas2 }
     *     
     */
    public ArrayOfWsAnaliseTodasSublojas2 getCapturarResultadosTodasSubLojas3Result() {
        return capturarResultadosTodasSubLojas3Result;
    }

    /**
     * Sets the value of the capturarResultadosTodasSubLojas3Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsAnaliseTodasSublojas2 }
     *     
     */
    public void setCapturarResultadosTodasSubLojas3Result(ArrayOfWsAnaliseTodasSublojas2 value) {
        this.capturarResultadosTodasSubLojas3Result = value;
    }

}
