
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsRiscoAnaliseTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsRiscoAnaliseTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Risco" type="{http://tempuri.org/}WsOpiniaoRisco"/>
 *         &lt;element name="Score" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PontosAtencao" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataAnalise" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="AnaliseAutomatica" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DadosAnalista" type="{http://tempuri.org/}WsAnalistaTroca" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsRiscoAnaliseTroca", propOrder = {
    "risco",
    "score",
    "pontosAtencao",
    "status",
    "dataAnalise",
    "analiseAutomatica",
    "dadosAnalista"
})
public class WsRiscoAnaliseTroca {

    @XmlElement(name = "Risco", required = true)
    @XmlSchemaType(name = "string")
    protected WsOpiniaoRisco risco;
    @XmlElement(name = "Score")
    protected int score;
    @XmlElement(name = "PontosAtencao")
    protected ArrayOfString pontosAtencao;
    @XmlElement(name = "Status")
    protected int status;
    @XmlElement(name = "DataAnalise", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAnalise;
    @XmlElement(name = "AnaliseAutomatica")
    protected boolean analiseAutomatica;
    @XmlElement(name = "DadosAnalista")
    protected WsAnalistaTroca dadosAnalista;

    /**
     * Gets the value of the risco property.
     * 
     * @return
     *     possible object is
     *     {@link WsOpiniaoRisco }
     *     
     */
    public WsOpiniaoRisco getRisco() {
        return risco;
    }

    /**
     * Sets the value of the risco property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsOpiniaoRisco }
     *     
     */
    public void setRisco(WsOpiniaoRisco value) {
        this.risco = value;
    }

    /**
     * Gets the value of the score property.
     * 
     */
    public int getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     */
    public void setScore(int value) {
        this.score = value;
    }

    /**
     * Gets the value of the pontosAtencao property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getPontosAtencao() {
        return pontosAtencao;
    }

    /**
     * Sets the value of the pontosAtencao property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setPontosAtencao(ArrayOfString value) {
        this.pontosAtencao = value;
    }

    /**
     * Gets the value of the status property.
     * 
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     */
    public void setStatus(int value) {
        this.status = value;
    }

    /**
     * Gets the value of the dataAnalise property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAnalise() {
        return dataAnalise;
    }

    /**
     * Sets the value of the dataAnalise property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAnalise(XMLGregorianCalendar value) {
        this.dataAnalise = value;
    }

    /**
     * Gets the value of the analiseAutomatica property.
     * 
     */
    public boolean isAnaliseAutomatica() {
        return analiseAutomatica;
    }

    /**
     * Sets the value of the analiseAutomatica property.
     * 
     */
    public void setAnaliseAutomatica(boolean value) {
        this.analiseAutomatica = value;
    }

    /**
     * Gets the value of the dadosAnalista property.
     * 
     * @return
     *     possible object is
     *     {@link WsAnalistaTroca }
     *     
     */
    public WsAnalistaTroca getDadosAnalista() {
        return dadosAnalista;
    }

    /**
     * Sets the value of the dadosAnalista property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAnalistaTroca }
     *     
     */
    public void setDadosAnalista(WsAnalistaTroca value) {
        this.dadosAnalista = value;
    }

}
