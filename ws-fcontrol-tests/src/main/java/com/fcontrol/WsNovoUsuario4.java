
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsNovoUsuario4 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsNovoUsuario4">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsNovoUsuario3">
 *       &lt;sequence>
 *         &lt;element name="DeviceFingerPrintAtivado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsNovoUsuario4", propOrder = {
    "deviceFingerPrintAtivado"
})
public class WsNovoUsuario4
    extends WsNovoUsuario3
{

    @XmlElement(name = "DeviceFingerPrintAtivado")
    protected boolean deviceFingerPrintAtivado;

    /**
     * Gets the value of the deviceFingerPrintAtivado property.
     * 
     */
    public boolean isDeviceFingerPrintAtivado() {
        return deviceFingerPrintAtivado;
    }

    /**
     * Sets the value of the deviceFingerPrintAtivado property.
     * 
     */
    public void setDeviceFingerPrintAtivado(boolean value) {
        this.deviceFingerPrintAtivado = value;
    }

}
