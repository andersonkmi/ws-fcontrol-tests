
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsProdutoTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsProdutoTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsProdutoTroca" type="{http://tempuri.org/}WsProdutoTroca" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsProdutoTroca", propOrder = {
    "wsProdutoTroca"
})
public class ArrayOfWsProdutoTroca {

    @XmlElement(name = "WsProdutoTroca", nillable = true)
    protected List<WsProdutoTroca> wsProdutoTroca;

    /**
     * Gets the value of the wsProdutoTroca property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsProdutoTroca property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsProdutoTroca().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsProdutoTroca }
     * 
     * 
     */
    public List<WsProdutoTroca> getWsProdutoTroca() {
        if (wsProdutoTroca == null) {
            wsProdutoTroca = new ArrayList<WsProdutoTroca>();
        }
        return this.wsProdutoTroca;
    }

}
