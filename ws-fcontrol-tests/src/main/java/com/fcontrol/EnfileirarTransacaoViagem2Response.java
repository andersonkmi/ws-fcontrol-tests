
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enfileirarTransacaoViagem2Result" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enfileirarTransacaoViagem2Result"
})
@XmlRootElement(name = "enfileirarTransacaoViagem2Response")
public class EnfileirarTransacaoViagem2Response {

    protected WsResultado enfileirarTransacaoViagem2Result;

    /**
     * Gets the value of the enfileirarTransacaoViagem2Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getEnfileirarTransacaoViagem2Result() {
        return enfileirarTransacaoViagem2Result;
    }

    /**
     * Sets the value of the enfileirarTransacaoViagem2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setEnfileirarTransacaoViagem2Result(WsResultado value) {
        this.enfileirarTransacaoViagem2Result = value;
    }

}
