
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enfileirarTransacaoAero2Result" type="{http://tempuri.org/}WsAeroResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enfileirarTransacaoAero2Result"
})
@XmlRootElement(name = "enfileirarTransacaoAero2Response")
public class EnfileirarTransacaoAero2Response {

    protected WsAeroResultado enfileirarTransacaoAero2Result;

    /**
     * Gets the value of the enfileirarTransacaoAero2Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsAeroResultado }
     *     
     */
    public WsAeroResultado getEnfileirarTransacaoAero2Result() {
        return enfileirarTransacaoAero2Result;
    }

    /**
     * Sets the value of the enfileirarTransacaoAero2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAeroResultado }
     *     
     */
    public void setEnfileirarTransacaoAero2Result(WsAeroResultado value) {
        this.enfileirarTransacaoAero2Result = value;
    }

}
