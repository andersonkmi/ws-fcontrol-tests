
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="analisarTransacaoTelecom2Result" type="{http://tempuri.org/}WsResultadoAnalise" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarTransacaoTelecom2Result"
})
@XmlRootElement(name = "analisarTransacaoTelecom2Response")
public class AnalisarTransacaoTelecom2Response {

    protected WsResultadoAnalise analisarTransacaoTelecom2Result;

    /**
     * Gets the value of the analisarTransacaoTelecom2Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultadoAnalise }
     *     
     */
    public WsResultadoAnalise getAnalisarTransacaoTelecom2Result() {
        return analisarTransacaoTelecom2Result;
    }

    /**
     * Sets the value of the analisarTransacaoTelecom2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultadoAnalise }
     *     
     */
    public void setAnalisarTransacaoTelecom2Result(WsResultadoAnalise value) {
        this.analisarTransacaoTelecom2Result = value;
    }

}
