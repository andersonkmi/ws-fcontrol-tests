
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsPessoaTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsPessoaTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Telefones" type="{http://tempuri.org/}ArrayOfWsTelefoneTroca" minOccurs="0"/>
 *         &lt;element name="Endereco" type="{http://tempuri.org/}WsEnderecoTroca" minOccurs="0"/>
 *         &lt;element name="Funcionario" type="{http://tempuri.org/}WsFuncionarioTroca" minOccurs="0"/>
 *         &lt;element name="ContaBancaria" type="{http://tempuri.org/}WsContaBancariaTroca" minOccurs="0"/>
 *         &lt;element name="TipoPublico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailPrincipal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoPessoa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CPF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrgaoEmissor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataNascimento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="EstadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExisteCadastroFuncionario" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NomeEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CPFIgualConsultor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EnderecoIgualConsultor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsPessoaTroca", propOrder = {
    "telefones",
    "endereco",
    "funcionario",
    "contaBancaria",
    "tipoPublico",
    "codigo",
    "nome",
    "sexo",
    "email",
    "emailPrincipal",
    "tipoPessoa",
    "cpf",
    "rg",
    "orgaoEmissor",
    "dataNascimento",
    "estadoCivil",
    "existeCadastroFuncionario",
    "nomeEmpresa",
    "cpfIgualConsultor",
    "enderecoIgualConsultor"
})
public class WsPessoaTroca {

    @XmlElement(name = "Telefones")
    protected ArrayOfWsTelefoneTroca telefones;
    @XmlElement(name = "Endereco")
    protected WsEnderecoTroca endereco;
    @XmlElement(name = "Funcionario")
    protected WsFuncionarioTroca funcionario;
    @XmlElement(name = "ContaBancaria")
    protected WsContaBancariaTroca contaBancaria;
    @XmlElement(name = "TipoPublico")
    protected String tipoPublico;
    @XmlElement(name = "Codigo")
    protected String codigo;
    @XmlElement(name = "Nome")
    protected String nome;
    @XmlElement(name = "Sexo")
    protected String sexo;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "EmailPrincipal")
    protected String emailPrincipal;
    @XmlElement(name = "TipoPessoa")
    protected String tipoPessoa;
    @XmlElement(name = "CPF")
    protected String cpf;
    @XmlElement(name = "RG")
    protected String rg;
    @XmlElement(name = "OrgaoEmissor")
    protected String orgaoEmissor;
    @XmlElement(name = "DataNascimento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataNascimento;
    @XmlElement(name = "EstadoCivil")
    protected String estadoCivil;
    @XmlElement(name = "ExisteCadastroFuncionario")
    protected boolean existeCadastroFuncionario;
    @XmlElement(name = "NomeEmpresa")
    protected String nomeEmpresa;
    @XmlElement(name = "CPFIgualConsultor")
    protected boolean cpfIgualConsultor;
    @XmlElement(name = "EnderecoIgualConsultor")
    protected boolean enderecoIgualConsultor;

    /**
     * Gets the value of the telefones property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsTelefoneTroca }
     *     
     */
    public ArrayOfWsTelefoneTroca getTelefones() {
        return telefones;
    }

    /**
     * Sets the value of the telefones property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsTelefoneTroca }
     *     
     */
    public void setTelefones(ArrayOfWsTelefoneTroca value) {
        this.telefones = value;
    }

    /**
     * Gets the value of the endereco property.
     * 
     * @return
     *     possible object is
     *     {@link WsEnderecoTroca }
     *     
     */
    public WsEnderecoTroca getEndereco() {
        return endereco;
    }

    /**
     * Sets the value of the endereco property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsEnderecoTroca }
     *     
     */
    public void setEndereco(WsEnderecoTroca value) {
        this.endereco = value;
    }

    /**
     * Gets the value of the funcionario property.
     * 
     * @return
     *     possible object is
     *     {@link WsFuncionarioTroca }
     *     
     */
    public WsFuncionarioTroca getFuncionario() {
        return funcionario;
    }

    /**
     * Sets the value of the funcionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsFuncionarioTroca }
     *     
     */
    public void setFuncionario(WsFuncionarioTroca value) {
        this.funcionario = value;
    }

    /**
     * Gets the value of the contaBancaria property.
     * 
     * @return
     *     possible object is
     *     {@link WsContaBancariaTroca }
     *     
     */
    public WsContaBancariaTroca getContaBancaria() {
        return contaBancaria;
    }

    /**
     * Sets the value of the contaBancaria property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsContaBancariaTroca }
     *     
     */
    public void setContaBancaria(WsContaBancariaTroca value) {
        this.contaBancaria = value;
    }

    /**
     * Gets the value of the tipoPublico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPublico() {
        return tipoPublico;
    }

    /**
     * Sets the value of the tipoPublico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPublico(String value) {
        this.tipoPublico = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the sexo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * Sets the value of the sexo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSexo(String value) {
        this.sexo = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the emailPrincipal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailPrincipal() {
        return emailPrincipal;
    }

    /**
     * Sets the value of the emailPrincipal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailPrincipal(String value) {
        this.emailPrincipal = value;
    }

    /**
     * Gets the value of the tipoPessoa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPessoa() {
        return tipoPessoa;
    }

    /**
     * Sets the value of the tipoPessoa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPessoa(String value) {
        this.tipoPessoa = value;
    }

    /**
     * Gets the value of the cpf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPF() {
        return cpf;
    }

    /**
     * Sets the value of the cpf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPF(String value) {
        this.cpf = value;
    }

    /**
     * Gets the value of the rg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRG() {
        return rg;
    }

    /**
     * Sets the value of the rg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRG(String value) {
        this.rg = value;
    }

    /**
     * Gets the value of the orgaoEmissor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgaoEmissor() {
        return orgaoEmissor;
    }

    /**
     * Sets the value of the orgaoEmissor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgaoEmissor(String value) {
        this.orgaoEmissor = value;
    }

    /**
     * Gets the value of the dataNascimento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataNascimento() {
        return dataNascimento;
    }

    /**
     * Sets the value of the dataNascimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataNascimento(XMLGregorianCalendar value) {
        this.dataNascimento = value;
    }

    /**
     * Gets the value of the estadoCivil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Sets the value of the estadoCivil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoCivil(String value) {
        this.estadoCivil = value;
    }

    /**
     * Gets the value of the existeCadastroFuncionario property.
     * 
     */
    public boolean isExisteCadastroFuncionario() {
        return existeCadastroFuncionario;
    }

    /**
     * Sets the value of the existeCadastroFuncionario property.
     * 
     */
    public void setExisteCadastroFuncionario(boolean value) {
        this.existeCadastroFuncionario = value;
    }

    /**
     * Gets the value of the nomeEmpresa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    /**
     * Sets the value of the nomeEmpresa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeEmpresa(String value) {
        this.nomeEmpresa = value;
    }

    /**
     * Gets the value of the cpfIgualConsultor property.
     * 
     */
    public boolean isCPFIgualConsultor() {
        return cpfIgualConsultor;
    }

    /**
     * Sets the value of the cpfIgualConsultor property.
     * 
     */
    public void setCPFIgualConsultor(boolean value) {
        this.cpfIgualConsultor = value;
    }

    /**
     * Gets the value of the enderecoIgualConsultor property.
     * 
     */
    public boolean isEnderecoIgualConsultor() {
        return enderecoIgualConsultor;
    }

    /**
     * Sets the value of the enderecoIgualConsultor property.
     * 
     */
    public void setEnderecoIgualConsultor(boolean value) {
        this.enderecoIgualConsultor = value;
    }

}
