
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsNovoUsuario2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsNovoUsuario2">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsNovoUsuario">
 *       &lt;sequence>
 *         &lt;element name="CodigoSegmento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsNovoUsuario2", propOrder = {
    "codigoSegmento"
})
@XmlSeeAlso({
    WsNovoUsuario3 .class
})
public class WsNovoUsuario2
    extends WsNovoUsuario
{

    @XmlElement(name = "CodigoSegmento")
    protected int codigoSegmento;

    /**
     * Gets the value of the codigoSegmento property.
     * 
     */
    public int getCodigoSegmento() {
        return codigoSegmento;
    }

    /**
     * Sets the value of the codigoSegmento property.
     * 
     */
    public void setCodigoSegmento(int value) {
        this.codigoSegmento = value;
    }

}
