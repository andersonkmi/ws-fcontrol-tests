
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enfileirarTransacao10Result" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enfileirarTransacao10Result"
})
@XmlRootElement(name = "enfileirarTransacao10Response")
public class EnfileirarTransacao10Response {

    protected WsResultado enfileirarTransacao10Result;

    /**
     * Gets the value of the enfileirarTransacao10Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getEnfileirarTransacao10Result() {
        return enfileirarTransacao10Result;
    }

    /**
     * Sets the value of the enfileirarTransacao10Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setEnfileirarTransacao10Result(WsResultado value) {
        this.enfileirarTransacao10Result = value;
    }

}
