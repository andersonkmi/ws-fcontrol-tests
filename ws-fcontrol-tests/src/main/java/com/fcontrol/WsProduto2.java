
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsProduto2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsProduto2">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsProduto">
 *       &lt;sequence>
 *         &lt;element name="ListaDeCasamento" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsProduto2", propOrder = {
    "listaDeCasamento"
})
@XmlSeeAlso({
    WsProduto3 .class
})
public class WsProduto2
    extends WsProduto
{

    @XmlElement(name = "ListaDeCasamento")
    protected boolean listaDeCasamento;

    /**
     * Gets the value of the listaDeCasamento property.
     * 
     */
    public boolean isListaDeCasamento() {
        return listaDeCasamento;
    }

    /**
     * Sets the value of the listaDeCasamento property.
     * 
     */
    public void setListaDeCasamento(boolean value) {
        this.listaDeCasamento = value;
    }

}
