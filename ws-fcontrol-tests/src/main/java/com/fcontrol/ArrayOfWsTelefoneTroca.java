
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsTelefoneTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsTelefoneTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsTelefoneTroca" type="{http://tempuri.org/}WsTelefoneTroca" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsTelefoneTroca", propOrder = {
    "wsTelefoneTroca"
})
public class ArrayOfWsTelefoneTroca {

    @XmlElement(name = "WsTelefoneTroca", nillable = true)
    protected List<WsTelefoneTroca> wsTelefoneTroca;

    /**
     * Gets the value of the wsTelefoneTroca property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsTelefoneTroca property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsTelefoneTroca().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsTelefoneTroca }
     * 
     * 
     */
    public List<WsTelefoneTroca> getWsTelefoneTroca() {
        if (wsTelefoneTroca == null) {
            wsTelefoneTroca = new ArrayList<WsTelefoneTroca>();
        }
        return this.wsTelefoneTroca;
    }

}
