
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarComentariosResult" type="{http://tempuri.org/}ArrayOfWsComentario" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarComentariosResult"
})
@XmlRootElement(name = "capturarComentariosResponse")
public class CapturarComentariosResponse {

    protected ArrayOfWsComentario capturarComentariosResult;

    /**
     * Gets the value of the capturarComentariosResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsComentario }
     *     
     */
    public ArrayOfWsComentario getCapturarComentariosResult() {
        return capturarComentariosResult;
    }

    /**
     * Sets the value of the capturarComentariosResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsComentario }
     *     
     */
    public void setCapturarComentariosResult(ArrayOfWsComentario value) {
        this.capturarComentariosResult = value;
    }

}
