
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsProduto4 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsProduto4">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsProduto3">
 *       &lt;sequence>
 *         &lt;element name="NomeVendedorMarketPlace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoTransacaoMarketPlace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsProduto4", propOrder = {
    "nomeVendedorMarketPlace",
    "codigoTransacaoMarketPlace"
})
public class WsProduto4
    extends WsProduto3
{

    @XmlElement(name = "NomeVendedorMarketPlace")
    protected String nomeVendedorMarketPlace;
    @XmlElement(name = "CodigoTransacaoMarketPlace")
    protected String codigoTransacaoMarketPlace;

    /**
     * Gets the value of the nomeVendedorMarketPlace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeVendedorMarketPlace() {
        return nomeVendedorMarketPlace;
    }

    /**
     * Sets the value of the nomeVendedorMarketPlace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeVendedorMarketPlace(String value) {
        this.nomeVendedorMarketPlace = value;
    }

    /**
     * Gets the value of the codigoTransacaoMarketPlace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTransacaoMarketPlace() {
        return codigoTransacaoMarketPlace;
    }

    /**
     * Sets the value of the codigoTransacaoMarketPlace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTransacaoMarketPlace(String value) {
        this.codigoTransacaoMarketPlace = value;
    }

}
