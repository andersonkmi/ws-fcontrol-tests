
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadoEspecificoResult" type="{http://tempuri.org/}WsAnalise" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadoEspecificoResult"
})
@XmlRootElement(name = "capturarResultadoEspecificoResponse")
public class CapturarResultadoEspecificoResponse {

    protected WsAnalise capturarResultadoEspecificoResult;

    /**
     * Gets the value of the capturarResultadoEspecificoResult property.
     * 
     * @return
     *     possible object is
     *     {@link WsAnalise }
     *     
     */
    public WsAnalise getCapturarResultadoEspecificoResult() {
        return capturarResultadoEspecificoResult;
    }

    /**
     * Sets the value of the capturarResultadoEspecificoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAnalise }
     *     
     */
    public void setCapturarResultadoEspecificoResult(WsAnalise value) {
        this.capturarResultadoEspecificoResult = value;
    }

}
