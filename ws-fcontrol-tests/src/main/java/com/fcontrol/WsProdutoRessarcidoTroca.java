
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsProdutoRessarcidoTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsProdutoRessarcidoTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoVenda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Linha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ValorUnitario" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Quantidade" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ValorTotal" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Alterado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsProdutoRessarcidoTroca", propOrder = {
    "codigoVenda",
    "linha",
    "descricao",
    "valorUnitario",
    "quantidade",
    "valorTotal",
    "alterado"
})
public class WsProdutoRessarcidoTroca {

    @XmlElement(name = "CodigoVenda")
    protected String codigoVenda;
    @XmlElement(name = "Linha")
    protected String linha;
    @XmlElement(name = "Descricao")
    protected String descricao;
    @XmlElement(name = "ValorUnitario")
    protected double valorUnitario;
    @XmlElement(name = "Quantidade")
    protected int quantidade;
    @XmlElement(name = "ValorTotal")
    protected double valorTotal;
    @XmlElement(name = "Alterado")
    protected boolean alterado;

    /**
     * Gets the value of the codigoVenda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoVenda() {
        return codigoVenda;
    }

    /**
     * Sets the value of the codigoVenda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoVenda(String value) {
        this.codigoVenda = value;
    }

    /**
     * Gets the value of the linha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinha() {
        return linha;
    }

    /**
     * Sets the value of the linha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinha(String value) {
        this.linha = value;
    }

    /**
     * Gets the value of the descricao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Sets the value of the descricao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Gets the value of the valorUnitario property.
     * 
     */
    public double getValorUnitario() {
        return valorUnitario;
    }

    /**
     * Sets the value of the valorUnitario property.
     * 
     */
    public void setValorUnitario(double value) {
        this.valorUnitario = value;
    }

    /**
     * Gets the value of the quantidade property.
     * 
     */
    public int getQuantidade() {
        return quantidade;
    }

    /**
     * Sets the value of the quantidade property.
     * 
     */
    public void setQuantidade(int value) {
        this.quantidade = value;
    }

    /**
     * Gets the value of the valorTotal property.
     * 
     */
    public double getValorTotal() {
        return valorTotal;
    }

    /**
     * Sets the value of the valorTotal property.
     * 
     */
    public void setValorTotal(double value) {
        this.valorTotal = value;
    }

    /**
     * Gets the value of the alterado property.
     * 
     */
    public boolean isAlterado() {
        return alterado;
    }

    /**
     * Sets the value of the alterado property.
     * 
     */
    public void setAlterado(boolean value) {
        this.alterado = value;
    }

}
