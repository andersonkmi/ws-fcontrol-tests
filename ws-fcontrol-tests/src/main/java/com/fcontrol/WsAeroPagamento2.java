
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsAeroPagamento2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAeroPagamento2">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsAeroPagamento">
 *       &lt;sequence>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NomeMae" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DddTelefoneMae" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroTelefoneMae" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnderecoMae" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DddTelefone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroTelefone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DddCelular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroCelular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAeroPagamento2", propOrder = {
    "email",
    "nomeMae",
    "dddTelefoneMae",
    "numeroTelefoneMae",
    "enderecoMae",
    "dddTelefone",
    "numeroTelefone",
    "dddCelular",
    "numeroCelular"
})
public class WsAeroPagamento2
    extends WsAeroPagamento
{

    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "NomeMae")
    protected String nomeMae;
    @XmlElement(name = "DddTelefoneMae")
    protected String dddTelefoneMae;
    @XmlElement(name = "NumeroTelefoneMae")
    protected String numeroTelefoneMae;
    @XmlElement(name = "EnderecoMae")
    protected String enderecoMae;
    @XmlElement(name = "DddTelefone")
    protected String dddTelefone;
    @XmlElement(name = "NumeroTelefone")
    protected String numeroTelefone;
    @XmlElement(name = "DddCelular")
    protected String dddCelular;
    @XmlElement(name = "NumeroCelular")
    protected String numeroCelular;

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the nomeMae property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeMae() {
        return nomeMae;
    }

    /**
     * Sets the value of the nomeMae property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeMae(String value) {
        this.nomeMae = value;
    }

    /**
     * Gets the value of the dddTelefoneMae property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddTelefoneMae() {
        return dddTelefoneMae;
    }

    /**
     * Sets the value of the dddTelefoneMae property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddTelefoneMae(String value) {
        this.dddTelefoneMae = value;
    }

    /**
     * Gets the value of the numeroTelefoneMae property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTelefoneMae() {
        return numeroTelefoneMae;
    }

    /**
     * Sets the value of the numeroTelefoneMae property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTelefoneMae(String value) {
        this.numeroTelefoneMae = value;
    }

    /**
     * Gets the value of the enderecoMae property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnderecoMae() {
        return enderecoMae;
    }

    /**
     * Sets the value of the enderecoMae property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnderecoMae(String value) {
        this.enderecoMae = value;
    }

    /**
     * Gets the value of the dddTelefone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddTelefone() {
        return dddTelefone;
    }

    /**
     * Sets the value of the dddTelefone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddTelefone(String value) {
        this.dddTelefone = value;
    }

    /**
     * Gets the value of the numeroTelefone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTelefone() {
        return numeroTelefone;
    }

    /**
     * Sets the value of the numeroTelefone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTelefone(String value) {
        this.numeroTelefone = value;
    }

    /**
     * Gets the value of the dddCelular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddCelular() {
        return dddCelular;
    }

    /**
     * Sets the value of the dddCelular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddCelular(String value) {
        this.dddCelular = value;
    }

    /**
     * Gets the value of the numeroCelular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCelular() {
        return numeroCelular;
    }

    /**
     * Sets the value of the numeroCelular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCelular(String value) {
        this.numeroCelular = value;
    }

}
