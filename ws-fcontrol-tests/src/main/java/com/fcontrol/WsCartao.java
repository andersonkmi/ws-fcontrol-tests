
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsCartao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCartao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NomeBancoEmissor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataValidadeCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NomeTitularCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CpfTitularCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Bin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="quatroUltimosDigitosCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BinBandeira" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BinBanco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BinPais" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DddTelefone2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroTelefone2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCartao", propOrder = {
    "nomeBancoEmissor",
    "numeroCartao",
    "dataValidadeCartao",
    "nomeTitularCartao",
    "cpfTitularCartao",
    "bin",
    "quatroUltimosDigitosCartao",
    "binBandeira",
    "binBanco",
    "binPais",
    "dddTelefone2",
    "numeroTelefone2"
})
public class WsCartao {

    @XmlElement(name = "NomeBancoEmissor")
    protected String nomeBancoEmissor;
    @XmlElement(name = "NumeroCartao")
    protected String numeroCartao;
    @XmlElement(name = "DataValidadeCartao")
    protected String dataValidadeCartao;
    @XmlElement(name = "NomeTitularCartao")
    protected String nomeTitularCartao;
    @XmlElement(name = "CpfTitularCartao")
    protected String cpfTitularCartao;
    @XmlElement(name = "Bin")
    protected String bin;
    protected String quatroUltimosDigitosCartao;
    @XmlElement(name = "BinBandeira")
    protected String binBandeira;
    @XmlElement(name = "BinBanco")
    protected String binBanco;
    @XmlElement(name = "BinPais")
    protected String binPais;
    @XmlElement(name = "DddTelefone2")
    protected String dddTelefone2;
    @XmlElement(name = "NumeroTelefone2")
    protected String numeroTelefone2;

    /**
     * Gets the value of the nomeBancoEmissor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeBancoEmissor() {
        return nomeBancoEmissor;
    }

    /**
     * Sets the value of the nomeBancoEmissor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeBancoEmissor(String value) {
        this.nomeBancoEmissor = value;
    }

    /**
     * Gets the value of the numeroCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCartao() {
        return numeroCartao;
    }

    /**
     * Sets the value of the numeroCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCartao(String value) {
        this.numeroCartao = value;
    }

    /**
     * Gets the value of the dataValidadeCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataValidadeCartao() {
        return dataValidadeCartao;
    }

    /**
     * Sets the value of the dataValidadeCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataValidadeCartao(String value) {
        this.dataValidadeCartao = value;
    }

    /**
     * Gets the value of the nomeTitularCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeTitularCartao() {
        return nomeTitularCartao;
    }

    /**
     * Sets the value of the nomeTitularCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeTitularCartao(String value) {
        this.nomeTitularCartao = value;
    }

    /**
     * Gets the value of the cpfTitularCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfTitularCartao() {
        return cpfTitularCartao;
    }

    /**
     * Sets the value of the cpfTitularCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfTitularCartao(String value) {
        this.cpfTitularCartao = value;
    }

    /**
     * Gets the value of the bin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBin() {
        return bin;
    }

    /**
     * Sets the value of the bin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBin(String value) {
        this.bin = value;
    }

    /**
     * Gets the value of the quatroUltimosDigitosCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuatroUltimosDigitosCartao() {
        return quatroUltimosDigitosCartao;
    }

    /**
     * Sets the value of the quatroUltimosDigitosCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuatroUltimosDigitosCartao(String value) {
        this.quatroUltimosDigitosCartao = value;
    }

    /**
     * Gets the value of the binBandeira property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBinBandeira() {
        return binBandeira;
    }

    /**
     * Sets the value of the binBandeira property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBinBandeira(String value) {
        this.binBandeira = value;
    }

    /**
     * Gets the value of the binBanco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBinBanco() {
        return binBanco;
    }

    /**
     * Sets the value of the binBanco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBinBanco(String value) {
        this.binBanco = value;
    }

    /**
     * Gets the value of the binPais property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBinPais() {
        return binPais;
    }

    /**
     * Sets the value of the binPais property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBinPais(String value) {
        this.binPais = value;
    }

    /**
     * Gets the value of the dddTelefone2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddTelefone2() {
        return dddTelefone2;
    }

    /**
     * Sets the value of the dddTelefone2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddTelefone2(String value) {
        this.dddTelefone2 = value;
    }

    /**
     * Gets the value of the numeroTelefone2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTelefone2() {
        return numeroTelefone2;
    }

    /**
     * Sets the value of the numeroTelefone2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTelefone2(String value) {
        this.numeroTelefone2 = value;
    }

}
