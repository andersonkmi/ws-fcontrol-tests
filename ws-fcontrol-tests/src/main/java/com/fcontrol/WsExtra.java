
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsExtra complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsExtra">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Extra1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Extra2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Extra3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Extra4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsExtra", propOrder = {
    "extra1",
    "extra2",
    "extra3",
    "extra4"
})
public class WsExtra {

    @XmlElement(name = "Extra1")
    protected String extra1;
    @XmlElement(name = "Extra2")
    protected String extra2;
    @XmlElement(name = "Extra3")
    protected String extra3;
    @XmlElement(name = "Extra4")
    protected String extra4;

    /**
     * Gets the value of the extra1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtra1() {
        return extra1;
    }

    /**
     * Sets the value of the extra1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtra1(String value) {
        this.extra1 = value;
    }

    /**
     * Gets the value of the extra2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtra2() {
        return extra2;
    }

    /**
     * Sets the value of the extra2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtra2(String value) {
        this.extra2 = value;
    }

    /**
     * Gets the value of the extra3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtra3() {
        return extra3;
    }

    /**
     * Sets the value of the extra3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtra3(String value) {
        this.extra3 = value;
    }

    /**
     * Gets the value of the extra4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtra4() {
        return extra4;
    }

    /**
     * Sets the value of the extra4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtra4(String value) {
        this.extra4 = value;
    }

}
