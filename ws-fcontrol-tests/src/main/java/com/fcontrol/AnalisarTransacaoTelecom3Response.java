
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="analisarTransacaoTelecom3Result" type="{http://tempuri.org/}WsResultadoAnalise" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarTransacaoTelecom3Result"
})
@XmlRootElement(name = "analisarTransacaoTelecom3Response")
public class AnalisarTransacaoTelecom3Response {

    protected WsResultadoAnalise analisarTransacaoTelecom3Result;

    /**
     * Gets the value of the analisarTransacaoTelecom3Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultadoAnalise }
     *     
     */
    public WsResultadoAnalise getAnalisarTransacaoTelecom3Result() {
        return analisarTransacaoTelecom3Result;
    }

    /**
     * Sets the value of the analisarTransacaoTelecom3Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultadoAnalise }
     *     
     */
    public void setAnalisarTransacaoTelecom3Result(WsResultadoAnalise value) {
        this.analisarTransacaoTelecom3Result = value;
    }

}
