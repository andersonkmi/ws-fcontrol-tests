
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarComentariosSubLojaResult" type="{http://tempuri.org/}ArrayOfWsComentario" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarComentariosSubLojaResult"
})
@XmlRootElement(name = "capturarComentariosSubLojaResponse")
public class CapturarComentariosSubLojaResponse {

    protected ArrayOfWsComentario capturarComentariosSubLojaResult;

    /**
     * Gets the value of the capturarComentariosSubLojaResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsComentario }
     *     
     */
    public ArrayOfWsComentario getCapturarComentariosSubLojaResult() {
        return capturarComentariosSubLojaResult;
    }

    /**
     * Sets the value of the capturarComentariosSubLojaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsComentario }
     *     
     */
    public void setCapturarComentariosSubLojaResult(ArrayOfWsComentario value) {
        this.capturarComentariosSubLojaResult = value;
    }

}
