
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsFuncionarioTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsFuncionarioTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CPF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NaturaEPS" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Endereco" type="{http://tempuri.org/}WsEnderecoTroca" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsFuncionarioTroca", propOrder = {
    "cpf",
    "naturaEPS",
    "endereco"
})
public class WsFuncionarioTroca {

    @XmlElement(name = "CPF")
    protected String cpf;
    @XmlElement(name = "NaturaEPS")
    protected boolean naturaEPS;
    @XmlElement(name = "Endereco")
    protected WsEnderecoTroca endereco;

    /**
     * Gets the value of the cpf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPF() {
        return cpf;
    }

    /**
     * Sets the value of the cpf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPF(String value) {
        this.cpf = value;
    }

    /**
     * Gets the value of the naturaEPS property.
     * 
     */
    public boolean isNaturaEPS() {
        return naturaEPS;
    }

    /**
     * Sets the value of the naturaEPS property.
     * 
     */
    public void setNaturaEPS(boolean value) {
        this.naturaEPS = value;
    }

    /**
     * Gets the value of the endereco property.
     * 
     * @return
     *     possible object is
     *     {@link WsEnderecoTroca }
     *     
     */
    public WsEnderecoTroca getEndereco() {
        return endereco;
    }

    /**
     * Sets the value of the endereco property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsEnderecoTroca }
     *     
     */
    public void setEndereco(WsEnderecoTroca value) {
        this.endereco = value;
    }

}
