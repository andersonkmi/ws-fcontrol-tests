
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsPagamento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsPagamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MetodoPagamento" type="{http://tempuri.org/}MetodoPagamento"/>
 *         &lt;element name="Cartao" type="{http://tempuri.org/}WsCartao" minOccurs="0"/>
 *         &lt;element name="Valor" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NumeroParcelas" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsPagamento", propOrder = {
    "metodoPagamento",
    "cartao",
    "valor",
    "numeroParcelas"
})
@XmlSeeAlso({
    WsPagamento2 .class
})
public class WsPagamento {

    @XmlElement(name = "MetodoPagamento", required = true)
    @XmlSchemaType(name = "string")
    protected MetodoPagamento metodoPagamento;
    @XmlElement(name = "Cartao")
    protected WsCartao cartao;
    @XmlElement(name = "Valor")
    protected int valor;
    @XmlElement(name = "NumeroParcelas")
    protected int numeroParcelas;

    /**
     * Gets the value of the metodoPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link MetodoPagamento }
     *     
     */
    public MetodoPagamento getMetodoPagamento() {
        return metodoPagamento;
    }

    /**
     * Sets the value of the metodoPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link MetodoPagamento }
     *     
     */
    public void setMetodoPagamento(MetodoPagamento value) {
        this.metodoPagamento = value;
    }

    /**
     * Gets the value of the cartao property.
     * 
     * @return
     *     possible object is
     *     {@link WsCartao }
     *     
     */
    public WsCartao getCartao() {
        return cartao;
    }

    /**
     * Sets the value of the cartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsCartao }
     *     
     */
    public void setCartao(WsCartao value) {
        this.cartao = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     */
    public int getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     */
    public void setValor(int value) {
        this.valor = value;
    }

    /**
     * Gets the value of the numeroParcelas property.
     * 
     */
    public int getNumeroParcelas() {
        return numeroParcelas;
    }

    /**
     * Sets the value of the numeroParcelas property.
     * 
     */
    public void setNumeroParcelas(int value) {
        this.numeroParcelas = value;
    }

}
