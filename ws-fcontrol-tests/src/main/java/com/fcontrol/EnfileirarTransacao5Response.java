
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enfileirarTransacao5Result" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enfileirarTransacao5Result"
})
@XmlRootElement(name = "enfileirarTransacao5Response")
public class EnfileirarTransacao5Response {

    protected WsResultado enfileirarTransacao5Result;

    /**
     * Gets the value of the enfileirarTransacao5Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getEnfileirarTransacao5Result() {
        return enfileirarTransacao5Result;
    }

    /**
     * Sets the value of the enfileirarTransacao5Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setEnfileirarTransacao5Result(WsResultado value) {
        this.enfileirarTransacao5Result = value;
    }

}
