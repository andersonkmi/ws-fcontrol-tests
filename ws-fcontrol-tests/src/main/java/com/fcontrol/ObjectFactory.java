
package com.fcontrol;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fcontrol package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fcontrol
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoManual2Response }
     * 
     */
    public EnfileirarTransacaoManual2Response createEnfileirarTransacaoManual2Response() {
        return new EnfileirarTransacaoManual2Response();
    }

    /**
     * Create an instance of {@link CriarUsuario }
     * 
     */
    public CriarUsuario createCriarUsuario() {
        return new CriarUsuario();
    }

    /**
     * Create an instance of {@link WsNovoUsuario }
     * 
     */
    public WsNovoUsuario createWsNovoUsuario() {
        return new WsNovoUsuario();
    }

    /**
     * Create an instance of {@link AlterarMetodoPagamento }
     * 
     */
    public AlterarMetodoPagamento createAlterarMetodoPagamento() {
        return new AlterarMetodoPagamento();
    }

    /**
     * Create an instance of {@link ArrayOfWsPagamento }
     * 
     */
    public ArrayOfWsPagamento createArrayOfWsPagamento() {
        return new ArrayOfWsPagamento();
    }

    /**
     * Create an instance of {@link AnalisarTransacao4Response }
     * 
     */
    public AnalisarTransacao4Response createAnalisarTransacao4Response() {
        return new AnalisarTransacao4Response();
    }

    /**
     * Create an instance of {@link WsResultadoAnalise2 }
     * 
     */
    public WsResultadoAnalise2 createWsResultadoAnalise2() {
        return new WsResultadoAnalise2();
    }

    /**
     * Create an instance of {@link AtualizarRecebimentoTrocaResponse }
     * 
     */
    public AtualizarRecebimentoTrocaResponse createAtualizarRecebimentoTrocaResponse() {
        return new AtualizarRecebimentoTrocaResponse();
    }

    /**
     * Create an instance of {@link WsResultadoTroca }
     * 
     */
    public WsResultadoTroca createWsResultadoTroca() {
        return new WsResultadoTroca();
    }

    /**
     * Create an instance of {@link AnalisarTransacao }
     * 
     */
    public AnalisarTransacao createAnalisarTransacao() {
        return new AnalisarTransacao();
    }

    /**
     * Create an instance of {@link WsTransacaoPagamentoMultiplo }
     * 
     */
    public WsTransacaoPagamentoMultiplo createWsTransacaoPagamentoMultiplo() {
        return new WsTransacaoPagamentoMultiplo();
    }

    /**
     * Create an instance of {@link AlterarStatusSubLoja2Response }
     * 
     */
    public AlterarStatusSubLoja2Response createAlterarStatusSubLoja2Response() {
        return new AlterarStatusSubLoja2Response();
    }

    /**
     * Create an instance of {@link WsResultado }
     * 
     */
    public WsResultado createWsResultado() {
        return new WsResultado();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecifico }
     * 
     */
    public CapturarResultadoEspecifico createCapturarResultadoEspecifico() {
        return new CapturarResultadoEspecifico();
    }

    /**
     * Create an instance of {@link ReanalisarTransacaoSubLojaResponse }
     * 
     */
    public ReanalisarTransacaoSubLojaResponse createReanalisarTransacaoSubLojaResponse() {
        return new ReanalisarTransacaoSubLojaResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao6Response }
     * 
     */
    public EnfileirarTransacao6Response createEnfileirarTransacao6Response() {
        return new EnfileirarTransacao6Response();
    }

    /**
     * Create an instance of {@link Denunciar }
     * 
     */
    public Denunciar createDenunciar() {
        return new Denunciar();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagemResponse }
     * 
     */
    public EnfileirarTransacaoViagemResponse createEnfileirarTransacaoViagemResponse() {
        return new EnfileirarTransacaoViagemResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao }
     * 
     */
    public EnfileirarTransacao createEnfileirarTransacao() {
        return new EnfileirarTransacao();
    }

    /**
     * Create an instance of {@link WsTransacao }
     * 
     */
    public WsTransacao createWsTransacao() {
        return new WsTransacao();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeralResponse }
     * 
     */
    public CapturarResultadosGeralResponse createCapturarResultadosGeralResponse() {
        return new CapturarResultadosGeralResponse();
    }

    /**
     * Create an instance of {@link ArrayOfWsAnalise }
     * 
     */
    public ArrayOfWsAnalise createArrayOfWsAnalise() {
        return new ArrayOfWsAnalise();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoAeroResponse }
     * 
     */
    public EnfileirarTransacaoAeroResponse createEnfileirarTransacaoAeroResponse() {
        return new EnfileirarTransacaoAeroResponse();
    }

    /**
     * Create an instance of {@link WsAeroResultado }
     * 
     */
    public WsAeroResultado createWsAeroResultado() {
        return new WsAeroResultado();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao11Response }
     * 
     */
    public EnfileirarTransacao11Response createEnfileirarTransacao11Response() {
        return new EnfileirarTransacao11Response();
    }

    /**
     * Create an instance of {@link CriarUsuario2 }
     * 
     */
    public CriarUsuario2 createCriarUsuario2() {
        return new CriarUsuario2();
    }

    /**
     * Create an instance of {@link WsNovoUsuario2 }
     * 
     */
    public WsNovoUsuario2 createWsNovoUsuario2() {
        return new WsNovoUsuario2();
    }

    /**
     * Create an instance of {@link AlterarStatusSubLoja2 }
     * 
     */
    public AlterarStatusSubLoja2 createAlterarStatusSubLoja2() {
        return new AlterarStatusSubLoja2();
    }

    /**
     * Create an instance of {@link CriarUsuario3Response }
     * 
     */
    public CriarUsuario3Response createCriarUsuario3Response() {
        return new CriarUsuario3Response();
    }

    /**
     * Create an instance of {@link AlterarStatus }
     * 
     */
    public AlterarStatus createAlterarStatus() {
        return new AlterarStatus();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoManual3 }
     * 
     */
    public EnfileirarTransacaoManual3 createEnfileirarTransacaoManual3() {
        return new EnfileirarTransacaoManual3();
    }

    /**
     * Create an instance of {@link ConfirmarRetornoResponse }
     * 
     */
    public ConfirmarRetornoResponse createConfirmarRetornoResponse() {
        return new ConfirmarRetornoResponse();
    }

    /**
     * Create an instance of {@link CapturarResultadosTodasSubLojasResponse }
     * 
     */
    public CapturarResultadosTodasSubLojasResponse createCapturarResultadosTodasSubLojasResponse() {
        return new CapturarResultadosTodasSubLojasResponse();
    }

    /**
     * Create an instance of {@link ArrayOfWsAnaliseTodasSublojas }
     * 
     */
    public ArrayOfWsAnaliseTodasSublojas createArrayOfWsAnaliseTodasSublojas() {
        return new ArrayOfWsAnaliseTodasSublojas();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao2Response }
     * 
     */
    public EnfileirarTransacao2Response createEnfileirarTransacao2Response() {
        return new EnfileirarTransacao2Response();
    }

    /**
     * Create an instance of {@link CriarUsuario4 }
     * 
     */
    public CriarUsuario4 createCriarUsuario4() {
        return new CriarUsuario4();
    }

    /**
     * Create an instance of {@link WsNovoUsuario4 }
     * 
     */
    public WsNovoUsuario4 createWsNovoUsuario4() {
        return new WsNovoUsuario4();
    }

    /**
     * Create an instance of {@link CriarUsuario3 }
     * 
     */
    public CriarUsuario3 createCriarUsuario3() {
        return new CriarUsuario3();
    }

    /**
     * Create an instance of {@link WsNovoUsuario3 }
     * 
     */
    public WsNovoUsuario3 createWsNovoUsuario3() {
        return new WsNovoUsuario3();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoManual2 }
     * 
     */
    public EnfileirarTransacaoManual2 createEnfileirarTransacaoManual2() {
        return new EnfileirarTransacaoManual2();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem3Response }
     * 
     */
    public EnfileirarTransacaoViagem3Response createEnfileirarTransacaoViagem3Response() {
        return new EnfileirarTransacaoViagem3Response();
    }

    /**
     * Create an instance of {@link ConfirmarRetorno }
     * 
     */
    public ConfirmarRetorno createConfirmarRetorno() {
        return new ConfirmarRetorno();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoTelecom4 }
     * 
     */
    public AnalisarTransacaoTelecom4 createAnalisarTransacaoTelecom4() {
        return new AnalisarTransacaoTelecom4();
    }

    /**
     * Create an instance of {@link WsTransacaoTelecom3 }
     * 
     */
    public WsTransacaoTelecom3 createWsTransacaoTelecom3() {
        return new WsTransacaoTelecom3();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoTelecom3 }
     * 
     */
    public AnalisarTransacaoTelecom3 createAnalisarTransacaoTelecom3() {
        return new AnalisarTransacaoTelecom3();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecificoSubLoja3 }
     * 
     */
    public CapturarResultadoEspecificoSubLoja3 createCapturarResultadoEspecificoSubLoja3() {
        return new CapturarResultadoEspecificoSubLoja3();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoTelecom2 }
     * 
     */
    public AnalisarTransacaoTelecom2 createAnalisarTransacaoTelecom2() {
        return new AnalisarTransacaoTelecom2();
    }

    /**
     * Create an instance of {@link WsTransacaoTelecom2 }
     * 
     */
    public WsTransacaoTelecom2 createWsTransacaoTelecom2() {
        return new WsTransacaoTelecom2();
    }

    /**
     * Create an instance of {@link CapturarResultadosTodasSubLojas4Response }
     * 
     */
    public CapturarResultadosTodasSubLojas4Response createCapturarResultadosTodasSubLojas4Response() {
        return new CapturarResultadosTodasSubLojas4Response();
    }

    /**
     * Create an instance of {@link ArrayOfWsAnaliseTodasSublojas3 }
     * 
     */
    public ArrayOfWsAnaliseTodasSublojas3 createArrayOfWsAnaliseTodasSublojas3() {
        return new ArrayOfWsAnaliseTodasSublojas3();
    }

    /**
     * Create an instance of {@link CapturarStatusAtualResponse }
     * 
     */
    public CapturarStatusAtualResponse createCapturarStatusAtualResponse() {
        return new CapturarStatusAtualResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoResponse }
     * 
     */
    public EnfileirarTransacaoResponse createEnfileirarTransacaoResponse() {
        return new EnfileirarTransacaoResponse();
    }

    /**
     * Create an instance of {@link AlterarStatusResponse }
     * 
     */
    public AlterarStatusResponse createAlterarStatusResponse() {
        return new AlterarStatusResponse();
    }

    /**
     * Create an instance of {@link Denunciar2 }
     * 
     */
    public Denunciar2 createDenunciar2() {
        return new Denunciar2();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem }
     * 
     */
    public EnfileirarTransacaoViagem createEnfileirarTransacaoViagem() {
        return new EnfileirarTransacaoViagem();
    }

    /**
     * Create an instance of {@link WsTransacaoViagem }
     * 
     */
    public WsTransacaoViagem createWsTransacaoViagem() {
        return new WsTransacaoViagem();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao3 }
     * 
     */
    public EnfileirarTransacao3 createEnfileirarTransacao3() {
        return new EnfileirarTransacao3();
    }

    /**
     * Create an instance of {@link WsTransacao3 }
     * 
     */
    public WsTransacao3 createWsTransacao3() {
        return new WsTransacao3();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem6 }
     * 
     */
    public EnfileirarTransacaoViagem6 createEnfileirarTransacaoViagem6() {
        return new EnfileirarTransacaoViagem6();
    }

    /**
     * Create an instance of {@link WsTransacaoViagem6 }
     * 
     */
    public WsTransacaoViagem6 createWsTransacaoViagem6() {
        return new WsTransacaoViagem6();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao4 }
     * 
     */
    public EnfileirarTransacao4 createEnfileirarTransacao4() {
        return new EnfileirarTransacao4();
    }

    /**
     * Create an instance of {@link WsTransacao4 }
     * 
     */
    public WsTransacao4 createWsTransacao4() {
        return new WsTransacao4();
    }

    /**
     * Create an instance of {@link AlterarStatus2Response }
     * 
     */
    public AlterarStatus2Response createAlterarStatus2Response() {
        return new AlterarStatus2Response();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoTelecom }
     * 
     */
    public AnalisarTransacaoTelecom createAnalisarTransacaoTelecom() {
        return new AnalisarTransacaoTelecom();
    }

    /**
     * Create an instance of {@link WsTransacaoTelecom }
     * 
     */
    public WsTransacaoTelecom createWsTransacaoTelecom() {
        return new WsTransacaoTelecom();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao7Response }
     * 
     */
    public EnfileirarTransacao7Response createEnfileirarTransacao7Response() {
        return new EnfileirarTransacao7Response();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao2 }
     * 
     */
    public EnfileirarTransacao2 createEnfileirarTransacao2() {
        return new EnfileirarTransacao2();
    }

    /**
     * Create an instance of {@link WsTransacaoPagamentoMultiplo2 }
     * 
     */
    public WsTransacaoPagamentoMultiplo2 createWsTransacaoPagamentoMultiplo2() {
        return new WsTransacaoPagamentoMultiplo2();
    }

    /**
     * Create an instance of {@link CapturarComentarios }
     * 
     */
    public CapturarComentarios createCapturarComentarios() {
        return new CapturarComentarios();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem3 }
     * 
     */
    public EnfileirarTransacaoViagem3 createEnfileirarTransacaoViagem3() {
        return new EnfileirarTransacaoViagem3();
    }

    /**
     * Create an instance of {@link WsTransacaoViagem3 }
     * 
     */
    public WsTransacaoViagem3 createWsTransacaoViagem3() {
        return new WsTransacaoViagem3();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao7 }
     * 
     */
    public EnfileirarTransacao7 createEnfileirarTransacao7() {
        return new EnfileirarTransacao7();
    }

    /**
     * Create an instance of {@link WsTransacao7 }
     * 
     */
    public WsTransacao7 createWsTransacao7() {
        return new WsTransacao7();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecificoSubLoja2 }
     * 
     */
    public CapturarResultadoEspecificoSubLoja2 createCapturarResultadoEspecificoSubLoja2() {
        return new CapturarResultadoEspecificoSubLoja2();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoTelecom4Response }
     * 
     */
    public AnalisarTransacaoTelecom4Response createAnalisarTransacaoTelecom4Response() {
        return new AnalisarTransacaoTelecom4Response();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem2 }
     * 
     */
    public EnfileirarTransacaoViagem2 createEnfileirarTransacaoViagem2() {
        return new EnfileirarTransacaoViagem2();
    }

    /**
     * Create an instance of {@link WsTransacaoViagem2 }
     * 
     */
    public WsTransacaoViagem2 createWsTransacaoViagem2() {
        return new WsTransacaoViagem2();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao8 }
     * 
     */
    public EnfileirarTransacao8 createEnfileirarTransacao8() {
        return new EnfileirarTransacao8();
    }

    /**
     * Create an instance of {@link WsTransacao8 }
     * 
     */
    public WsTransacao8 createWsTransacao8() {
        return new WsTransacao8();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem5 }
     * 
     */
    public EnfileirarTransacaoViagem5 createEnfileirarTransacaoViagem5() {
        return new EnfileirarTransacaoViagem5();
    }

    /**
     * Create an instance of {@link WsTransacaoViagem5 }
     * 
     */
    public WsTransacaoViagem5 createWsTransacaoViagem5() {
        return new WsTransacaoViagem5();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao5 }
     * 
     */
    public EnfileirarTransacao5 createEnfileirarTransacao5() {
        return new EnfileirarTransacao5();
    }

    /**
     * Create an instance of {@link WsTransacao5 }
     * 
     */
    public WsTransacao5 createWsTransacao5() {
        return new WsTransacao5();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem4 }
     * 
     */
    public EnfileirarTransacaoViagem4 createEnfileirarTransacaoViagem4() {
        return new EnfileirarTransacaoViagem4();
    }

    /**
     * Create an instance of {@link WsTransacaoViagem4 }
     * 
     */
    public WsTransacaoViagem4 createWsTransacaoViagem4() {
        return new WsTransacaoViagem4();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao6 }
     * 
     */
    public EnfileirarTransacao6 createEnfileirarTransacao6() {
        return new EnfileirarTransacao6();
    }

    /**
     * Create an instance of {@link WsTransacao6 }
     * 
     */
    public WsTransacao6 createWsTransacao6() {
        return new WsTransacao6();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeralSubLoja2Response }
     * 
     */
    public CapturarResultadosGeralSubLoja2Response createCapturarResultadosGeralSubLoja2Response() {
        return new CapturarResultadosGeralSubLoja2Response();
    }

    /**
     * Create an instance of {@link ArrayOfWsAnalise2 }
     * 
     */
    public ArrayOfWsAnalise2 createArrayOfWsAnalise2() {
        return new ArrayOfWsAnalise2();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao9 }
     * 
     */
    public EnfileirarTransacao9 createEnfileirarTransacao9() {
        return new EnfileirarTransacao9();
    }

    /**
     * Create an instance of {@link WsTransacao9 }
     * 
     */
    public WsTransacao9 createWsTransacao9() {
        return new WsTransacao9();
    }

    /**
     * Create an instance of {@link AlterarMetodoPagamentoResponse }
     * 
     */
    public AlterarMetodoPagamentoResponse createAlterarMetodoPagamentoResponse() {
        return new AlterarMetodoPagamentoResponse();
    }

    /**
     * Create an instance of {@link DesconfirmarRetornoResponse }
     * 
     */
    public DesconfirmarRetornoResponse createDesconfirmarRetornoResponse() {
        return new DesconfirmarRetornoResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao3Response }
     * 
     */
    public EnfileirarTransacao3Response createEnfileirarTransacao3Response() {
        return new EnfileirarTransacao3Response();
    }

    /**
     * Create an instance of {@link CriarUsuario2Response }
     * 
     */
    public CriarUsuario2Response createCriarUsuario2Response() {
        return new CriarUsuario2Response();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecificoSubLoja2Response }
     * 
     */
    public CapturarResultadoEspecificoSubLoja2Response createCapturarResultadoEspecificoSubLoja2Response() {
        return new CapturarResultadoEspecificoSubLoja2Response();
    }

    /**
     * Create an instance of {@link WsAnalise2 }
     * 
     */
    public WsAnalise2 createWsAnalise2() {
        return new WsAnalise2();
    }

    /**
     * Create an instance of {@link ExcluirSubLojaResponse }
     * 
     */
    public ExcluirSubLojaResponse createExcluirSubLojaResponse() {
        return new ExcluirSubLojaResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem6Response }
     * 
     */
    public EnfileirarTransacaoViagem6Response createEnfileirarTransacaoViagem6Response() {
        return new EnfileirarTransacaoViagem6Response();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecificoSubLojaResponse }
     * 
     */
    public CapturarResultadoEspecificoSubLojaResponse createCapturarResultadoEspecificoSubLojaResponse() {
        return new CapturarResultadoEspecificoSubLojaResponse();
    }

    /**
     * Create an instance of {@link WsAnalise }
     * 
     */
    public WsAnalise createWsAnalise() {
        return new WsAnalise();
    }

    /**
     * Create an instance of {@link AnalisarTransacao3Response }
     * 
     */
    public AnalisarTransacao3Response createAnalisarTransacao3Response() {
        return new AnalisarTransacao3Response();
    }

    /**
     * Create an instance of {@link WsResultadoAnalise }
     * 
     */
    public WsResultadoAnalise createWsResultadoAnalise() {
        return new WsResultadoAnalise();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecificoResponse }
     * 
     */
    public CapturarResultadoEspecificoResponse createCapturarResultadoEspecificoResponse() {
        return new CapturarResultadoEspecificoResponse();
    }

    /**
     * Create an instance of {@link CapturarResultadosTodasSubLojas2 }
     * 
     */
    public CapturarResultadosTodasSubLojas2 createCapturarResultadosTodasSubLojas2() {
        return new CapturarResultadosTodasSubLojas2();
    }

    /**
     * Create an instance of {@link CapturarResultadosTodasSubLojas3 }
     * 
     */
    public CapturarResultadosTodasSubLojas3 createCapturarResultadosTodasSubLojas3() {
        return new CapturarResultadosTodasSubLojas3();
    }

    /**
     * Create an instance of {@link ArrayOfMetodoPagamento }
     * 
     */
    public ArrayOfMetodoPagamento createArrayOfMetodoPagamento() {
        return new ArrayOfMetodoPagamento();
    }

    /**
     * Create an instance of {@link CapturarResultadosSubLoja2Response }
     * 
     */
    public CapturarResultadosSubLoja2Response createCapturarResultadosSubLoja2Response() {
        return new CapturarResultadosSubLoja2Response();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeral2Response }
     * 
     */
    public CapturarResultadosGeral2Response createCapturarResultadosGeral2Response() {
        return new CapturarResultadosGeral2Response();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoAero2 }
     * 
     */
    public EnfileirarTransacaoAero2 createEnfileirarTransacaoAero2() {
        return new EnfileirarTransacaoAero2();
    }

    /**
     * Create an instance of {@link WsAeroTransacao2 }
     * 
     */
    public WsAeroTransacao2 createWsAeroTransacao2() {
        return new WsAeroTransacao2();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecifico3Response }
     * 
     */
    public CapturarResultadoEspecifico3Response createCapturarResultadoEspecifico3Response() {
        return new CapturarResultadoEspecifico3Response();
    }

    /**
     * Create an instance of {@link WsAnalise3 }
     * 
     */
    public WsAnalise3 createWsAnalise3() {
        return new WsAnalise3();
    }

    /**
     * Create an instance of {@link CapturarResultadosTodasSubLojas4 }
     * 
     */
    public CapturarResultadosTodasSubLojas4 createCapturarResultadosTodasSubLojas4() {
        return new CapturarResultadosTodasSubLojas4();
    }

    /**
     * Create an instance of {@link CapturarComentariosResponse }
     * 
     */
    public CapturarComentariosResponse createCapturarComentariosResponse() {
        return new CapturarComentariosResponse();
    }

    /**
     * Create an instance of {@link ArrayOfWsComentario }
     * 
     */
    public ArrayOfWsComentario createArrayOfWsComentario() {
        return new ArrayOfWsComentario();
    }

    /**
     * Create an instance of {@link CapturarResultadosTodasSubLojas }
     * 
     */
    public CapturarResultadosTodasSubLojas createCapturarResultadosTodasSubLojas() {
        return new CapturarResultadosTodasSubLojas();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoAero }
     * 
     */
    public EnfileirarTransacaoAero createEnfileirarTransacaoAero() {
        return new EnfileirarTransacaoAero();
    }

    /**
     * Create an instance of {@link WsAeroTransacao }
     * 
     */
    public WsAeroTransacao createWsAeroTransacao() {
        return new WsAeroTransacao();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem2Response }
     * 
     */
    public EnfileirarTransacaoViagem2Response createEnfileirarTransacaoViagem2Response() {
        return new EnfileirarTransacaoViagem2Response();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeralSubLoja2 }
     * 
     */
    public CapturarResultadosGeralSubLoja2 createCapturarResultadosGeralSubLoja2() {
        return new CapturarResultadosGeralSubLoja2();
    }

    /**
     * Create an instance of {@link CapturarResultados2Response }
     * 
     */
    public CapturarResultados2Response createCapturarResultados2Response() {
        return new CapturarResultados2Response();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeral }
     * 
     */
    public CapturarResultadosGeral createCapturarResultadosGeral() {
        return new CapturarResultadosGeral();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao11 }
     * 
     */
    public EnfileirarTransacao11 createEnfileirarTransacao11() {
        return new EnfileirarTransacao11();
    }

    /**
     * Create an instance of {@link WsTransacao11 }
     * 
     */
    public WsTransacao11 createWsTransacao11() {
        return new WsTransacao11();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao10 }
     * 
     */
    public EnfileirarTransacao10 createEnfileirarTransacao10() {
        return new EnfileirarTransacao10();
    }

    /**
     * Create an instance of {@link WsTransacao10 }
     * 
     */
    public WsTransacao10 createWsTransacao10() {
        return new WsTransacao10();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeralSubLoja3 }
     * 
     */
    public CapturarResultadosGeralSubLoja3 createCapturarResultadosGeralSubLoja3() {
        return new CapturarResultadosGeralSubLoja3();
    }

    /**
     * Create an instance of {@link ConfirmarRetornoSubLojaResponse }
     * 
     */
    public ConfirmarRetornoSubLojaResponse createConfirmarRetornoSubLojaResponse() {
        return new ConfirmarRetornoSubLojaResponse();
    }

    /**
     * Create an instance of {@link CriarUsuario4Response }
     * 
     */
    public CriarUsuario4Response createCriarUsuario4Response() {
        return new CriarUsuario4Response();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeralSubLoja }
     * 
     */
    public CapturarResultadosGeralSubLoja createCapturarResultadosGeralSubLoja() {
        return new CapturarResultadosGeralSubLoja();
    }

    /**
     * Create an instance of {@link ConfirmarRetornoSubLoja }
     * 
     */
    public ConfirmarRetornoSubLoja createConfirmarRetornoSubLoja() {
        return new ConfirmarRetornoSubLoja();
    }

    /**
     * Create an instance of {@link ReanalisarTransacaoResponse }
     * 
     */
    public ReanalisarTransacaoResponse createReanalisarTransacaoResponse() {
        return new ReanalisarTransacaoResponse();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoTelecom3Response }
     * 
     */
    public AnalisarTransacaoTelecom3Response createAnalisarTransacaoTelecom3Response() {
        return new AnalisarTransacaoTelecom3Response();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoTrocaResponse }
     * 
     */
    public AnalisarTransacaoTrocaResponse createAnalisarTransacaoTrocaResponse() {
        return new AnalisarTransacaoTrocaResponse();
    }

    /**
     * Create an instance of {@link WsRetornoTroca }
     * 
     */
    public WsRetornoTroca createWsRetornoTroca() {
        return new WsRetornoTroca();
    }

    /**
     * Create an instance of {@link CapturarResultadosTodasSubLojas3Response }
     * 
     */
    public CapturarResultadosTodasSubLojas3Response createCapturarResultadosTodasSubLojas3Response() {
        return new CapturarResultadosTodasSubLojas3Response();
    }

    /**
     * Create an instance of {@link ArrayOfWsAnaliseTodasSublojas2 }
     * 
     */
    public ArrayOfWsAnaliseTodasSublojas2 createArrayOfWsAnaliseTodasSublojas2() {
        return new ArrayOfWsAnaliseTodasSublojas2();
    }

    /**
     * Create an instance of {@link Denunciar2Response }
     * 
     */
    public Denunciar2Response createDenunciar2Response() {
        return new Denunciar2Response();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoPagamentoMultiploResponse }
     * 
     */
    public EnfileirarTransacaoPagamentoMultiploResponse createEnfileirarTransacaoPagamentoMultiploResponse() {
        return new EnfileirarTransacaoPagamentoMultiploResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoManual }
     * 
     */
    public EnfileirarTransacaoManual createEnfileirarTransacaoManual() {
        return new EnfileirarTransacaoManual();
    }

    /**
     * Create an instance of {@link AnalisarTransacao2 }
     * 
     */
    public AnalisarTransacao2 createAnalisarTransacao2() {
        return new AnalisarTransacao2();
    }

    /**
     * Create an instance of {@link AnalisarTransacao4 }
     * 
     */
    public AnalisarTransacao4 createAnalisarTransacao4() {
        return new AnalisarTransacao4();
    }

    /**
     * Create an instance of {@link AnalisarTransacao3 }
     * 
     */
    public AnalisarTransacao3 createAnalisarTransacao3() {
        return new AnalisarTransacao3();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao4Response }
     * 
     */
    public EnfileirarTransacao4Response createEnfileirarTransacao4Response() {
        return new EnfileirarTransacao4Response();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao9Response }
     * 
     */
    public EnfileirarTransacao9Response createEnfileirarTransacao9Response() {
        return new EnfileirarTransacao9Response();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeralSubLoja3Response }
     * 
     */
    public CapturarResultadosGeralSubLoja3Response createCapturarResultadosGeralSubLoja3Response() {
        return new CapturarResultadosGeralSubLoja3Response();
    }

    /**
     * Create an instance of {@link ArrayOfWsAnalise3 }
     * 
     */
    public ArrayOfWsAnalise3 createArrayOfWsAnalise3() {
        return new ArrayOfWsAnalise3();
    }

    /**
     * Create an instance of {@link AtualizarStatusPedidoSubLoja }
     * 
     */
    public AtualizarStatusPedidoSubLoja createAtualizarStatusPedidoSubLoja() {
        return new AtualizarStatusPedidoSubLoja();
    }

    /**
     * Create an instance of {@link AlterarStatus2 }
     * 
     */
    public AlterarStatus2 createAlterarStatus2() {
        return new AlterarStatus2();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecificoSubLoja3Response }
     * 
     */
    public CapturarResultadoEspecificoSubLoja3Response createCapturarResultadoEspecificoSubLoja3Response() {
        return new CapturarResultadoEspecificoSubLoja3Response();
    }

    /**
     * Create an instance of {@link ExcluirSubLoja }
     * 
     */
    public ExcluirSubLoja createExcluirSubLoja() {
        return new ExcluirSubLoja();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem5Response }
     * 
     */
    public EnfileirarTransacaoViagem5Response createEnfileirarTransacaoViagem5Response() {
        return new EnfileirarTransacaoViagem5Response();
    }

    /**
     * Create an instance of {@link DesconfirmarRetornoSubLoja }
     * 
     */
    public DesconfirmarRetornoSubLoja createDesconfirmarRetornoSubLoja() {
        return new DesconfirmarRetornoSubLoja();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecificoSubLoja }
     * 
     */
    public CapturarResultadoEspecificoSubLoja createCapturarResultadoEspecificoSubLoja() {
        return new CapturarResultadoEspecificoSubLoja();
    }

    /**
     * Create an instance of {@link CapturarResultadosSubLoja3Response }
     * 
     */
    public CapturarResultadosSubLoja3Response createCapturarResultadosSubLoja3Response() {
        return new CapturarResultadosSubLoja3Response();
    }

    /**
     * Create an instance of {@link DesconfirmarRetorno }
     * 
     */
    public DesconfirmarRetorno createDesconfirmarRetorno() {
        return new DesconfirmarRetorno();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeralSubLojaResponse }
     * 
     */
    public CapturarResultadosGeralSubLojaResponse createCapturarResultadosGeralSubLojaResponse() {
        return new CapturarResultadosGeralSubLojaResponse();
    }

    /**
     * Create an instance of {@link ReanalisarTransacao }
     * 
     */
    public ReanalisarTransacao createReanalisarTransacao() {
        return new ReanalisarTransacao();
    }

    /**
     * Create an instance of {@link DenunciarSubLoja }
     * 
     */
    public DenunciarSubLoja createDenunciarSubLoja() {
        return new DenunciarSubLoja();
    }

    /**
     * Create an instance of {@link AtualizarStatusTroca }
     * 
     */
    public AtualizarStatusTroca createAtualizarStatusTroca() {
        return new AtualizarStatusTroca();
    }

    /**
     * Create an instance of {@link AnalisarTransacao2Response }
     * 
     */
    public AnalisarTransacao2Response createAnalisarTransacao2Response() {
        return new AnalisarTransacao2Response();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoPagamentoMultiplo }
     * 
     */
    public EnfileirarTransacaoPagamentoMultiplo createEnfileirarTransacaoPagamentoMultiplo() {
        return new EnfileirarTransacaoPagamentoMultiplo();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeral3Response }
     * 
     */
    public CapturarResultadosGeral3Response createCapturarResultadosGeral3Response() {
        return new CapturarResultadosGeral3Response();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecifico2Response }
     * 
     */
    public CapturarResultadoEspecifico2Response createCapturarResultadoEspecifico2Response() {
        return new CapturarResultadoEspecifico2Response();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeral2 }
     * 
     */
    public CapturarResultadosGeral2 createCapturarResultadosGeral2() {
        return new CapturarResultadosGeral2();
    }

    /**
     * Create an instance of {@link DenunciarSubLoja2 }
     * 
     */
    public DenunciarSubLoja2 createDenunciarSubLoja2() {
        return new DenunciarSubLoja2();
    }

    /**
     * Create an instance of {@link CapturarResultadosSubLojaResponse }
     * 
     */
    public CapturarResultadosSubLojaResponse createCapturarResultadosSubLojaResponse() {
        return new CapturarResultadosSubLojaResponse();
    }

    /**
     * Create an instance of {@link CapturarResultadosGeral3 }
     * 
     */
    public CapturarResultadosGeral3 createCapturarResultadosGeral3() {
        return new CapturarResultadosGeral3();
    }

    /**
     * Create an instance of {@link CapturarResultados }
     * 
     */
    public CapturarResultados createCapturarResultados() {
        return new CapturarResultados();
    }

    /**
     * Create an instance of {@link CapturarComentariosSubLojaResponse }
     * 
     */
    public CapturarComentariosSubLojaResponse createCapturarComentariosSubLojaResponse() {
        return new CapturarComentariosSubLojaResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoManualResponse }
     * 
     */
    public EnfileirarTransacaoManualResponse createEnfileirarTransacaoManualResponse() {
        return new EnfileirarTransacaoManualResponse();
    }

    /**
     * Create an instance of {@link AlterarStatusSubLojaResponse }
     * 
     */
    public AlterarStatusSubLojaResponse createAlterarStatusSubLojaResponse() {
        return new AlterarStatusSubLojaResponse();
    }

    /**
     * Create an instance of {@link CapturarResultadosSubLoja2 }
     * 
     */
    public CapturarResultadosSubLoja2 createCapturarResultadosSubLoja2() {
        return new CapturarResultadosSubLoja2();
    }

    /**
     * Create an instance of {@link CapturarComentariosSubLoja }
     * 
     */
    public CapturarComentariosSubLoja createCapturarComentariosSubLoja() {
        return new CapturarComentariosSubLoja();
    }

    /**
     * Create an instance of {@link CapturarResultadosSubLoja3 }
     * 
     */
    public CapturarResultadosSubLoja3 createCapturarResultadosSubLoja3() {
        return new CapturarResultadosSubLoja3();
    }

    /**
     * Create an instance of {@link CapturarResultadosTodasSubLojas2Response }
     * 
     */
    public CapturarResultadosTodasSubLojas2Response createCapturarResultadosTodasSubLojas2Response() {
        return new CapturarResultadosTodasSubLojas2Response();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoResponse }
     * 
     */
    public AnalisarTransacaoResponse createAnalisarTransacaoResponse() {
        return new AnalisarTransacaoResponse();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoTelecom2Response }
     * 
     */
    public AnalisarTransacaoTelecom2Response createAnalisarTransacaoTelecom2Response() {
        return new AnalisarTransacaoTelecom2Response();
    }

    /**
     * Create an instance of {@link AlterarStatusSubLoja }
     * 
     */
    public AlterarStatusSubLoja createAlterarStatusSubLoja() {
        return new AlterarStatusSubLoja();
    }

    /**
     * Create an instance of {@link AtualizarStatusTrocaResponse }
     * 
     */
    public AtualizarStatusTrocaResponse createAtualizarStatusTrocaResponse() {
        return new AtualizarStatusTrocaResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao5Response }
     * 
     */
    public EnfileirarTransacao5Response createEnfileirarTransacao5Response() {
        return new EnfileirarTransacao5Response();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoTelecomResponse }
     * 
     */
    public AnalisarTransacaoTelecomResponse createAnalisarTransacaoTelecomResponse() {
        return new AnalisarTransacaoTelecomResponse();
    }

    /**
     * Create an instance of {@link CapturarResultadosResponse }
     * 
     */
    public CapturarResultadosResponse createCapturarResultadosResponse() {
        return new CapturarResultadosResponse();
    }

    /**
     * Create an instance of {@link CapturarResultados2 }
     * 
     */
    public CapturarResultados2 createCapturarResultados2() {
        return new CapturarResultados2();
    }

    /**
     * Create an instance of {@link AlterarMetodoPagamentoSubLojaResponse }
     * 
     */
    public AlterarMetodoPagamentoSubLojaResponse createAlterarMetodoPagamentoSubLojaResponse() {
        return new AlterarMetodoPagamentoSubLojaResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoAero2Response }
     * 
     */
    public EnfileirarTransacaoAero2Response createEnfileirarTransacaoAero2Response() {
        return new EnfileirarTransacaoAero2Response();
    }

    /**
     * Create an instance of {@link CapturarResultados3 }
     * 
     */
    public CapturarResultados3 createCapturarResultados3() {
        return new CapturarResultados3();
    }

    /**
     * Create an instance of {@link DenunciarSubLojaResponse }
     * 
     */
    public DenunciarSubLojaResponse createDenunciarSubLojaResponse() {
        return new DenunciarSubLojaResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao8Response }
     * 
     */
    public EnfileirarTransacao8Response createEnfileirarTransacao8Response() {
        return new EnfileirarTransacao8Response();
    }

    /**
     * Create an instance of {@link DenunciarResponse }
     * 
     */
    public DenunciarResponse createDenunciarResponse() {
        return new DenunciarResponse();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecifico3 }
     * 
     */
    public CapturarResultadoEspecifico3 createCapturarResultadoEspecifico3() {
        return new CapturarResultadoEspecifico3();
    }

    /**
     * Create an instance of {@link AnalisarTransacaoTroca }
     * 
     */
    public AnalisarTransacaoTroca createAnalisarTransacaoTroca() {
        return new AnalisarTransacaoTroca();
    }

    /**
     * Create an instance of {@link WsTransacaoTroca }
     * 
     */
    public WsTransacaoTroca createWsTransacaoTroca() {
        return new WsTransacaoTroca();
    }

    /**
     * Create an instance of {@link CapturarResultadoEspecifico2 }
     * 
     */
    public CapturarResultadoEspecifico2 createCapturarResultadoEspecifico2() {
        return new CapturarResultadoEspecifico2();
    }

    /**
     * Create an instance of {@link CapturarStatusAtual }
     * 
     */
    public CapturarStatusAtual createCapturarStatusAtual() {
        return new CapturarStatusAtual();
    }

    /**
     * Create an instance of {@link AtualizarRecebimentoTroca }
     * 
     */
    public AtualizarRecebimentoTroca createAtualizarRecebimentoTroca() {
        return new AtualizarRecebimentoTroca();
    }

    /**
     * Create an instance of {@link WsRecebimentoTroca }
     * 
     */
    public WsRecebimentoTroca createWsRecebimentoTroca() {
        return new WsRecebimentoTroca();
    }

    /**
     * Create an instance of {@link AlterarMetodoPagamentoSubLoja }
     * 
     */
    public AlterarMetodoPagamentoSubLoja createAlterarMetodoPagamentoSubLoja() {
        return new AlterarMetodoPagamentoSubLoja();
    }

    /**
     * Create an instance of {@link AtualizarStatusPedidoSubLojaResponse }
     * 
     */
    public AtualizarStatusPedidoSubLojaResponse createAtualizarStatusPedidoSubLojaResponse() {
        return new AtualizarStatusPedidoSubLojaResponse();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoViagem4Response }
     * 
     */
    public EnfileirarTransacaoViagem4Response createEnfileirarTransacaoViagem4Response() {
        return new EnfileirarTransacaoViagem4Response();
    }

    /**
     * Create an instance of {@link EnfileirarTransacao10Response }
     * 
     */
    public EnfileirarTransacao10Response createEnfileirarTransacao10Response() {
        return new EnfileirarTransacao10Response();
    }

    /**
     * Create an instance of {@link EnfileirarTransacaoManual3Response }
     * 
     */
    public EnfileirarTransacaoManual3Response createEnfileirarTransacaoManual3Response() {
        return new EnfileirarTransacaoManual3Response();
    }

    /**
     * Create an instance of {@link CapturarResultados3Response }
     * 
     */
    public CapturarResultados3Response createCapturarResultados3Response() {
        return new CapturarResultados3Response();
    }

    /**
     * Create an instance of {@link DenunciarSubLoja2Response }
     * 
     */
    public DenunciarSubLoja2Response createDenunciarSubLoja2Response() {
        return new DenunciarSubLoja2Response();
    }

    /**
     * Create an instance of {@link ReanalisarTransacaoSubLoja }
     * 
     */
    public ReanalisarTransacaoSubLoja createReanalisarTransacaoSubLoja() {
        return new ReanalisarTransacaoSubLoja();
    }

    /**
     * Create an instance of {@link CriarUsuarioResponse }
     * 
     */
    public CriarUsuarioResponse createCriarUsuarioResponse() {
        return new CriarUsuarioResponse();
    }

    /**
     * Create an instance of {@link DesconfirmarRetornoSubLojaResponse }
     * 
     */
    public DesconfirmarRetornoSubLojaResponse createDesconfirmarRetornoSubLojaResponse() {
        return new DesconfirmarRetornoSubLojaResponse();
    }

    /**
     * Create an instance of {@link CapturarResultadosSubLoja }
     * 
     */
    public CapturarResultadosSubLoja createCapturarResultadosSubLoja() {
        return new CapturarResultadosSubLoja();
    }

    /**
     * Create an instance of {@link ArrayOfWsAeroBilhete2 }
     * 
     */
    public ArrayOfWsAeroBilhete2 createArrayOfWsAeroBilhete2() {
        return new ArrayOfWsAeroBilhete2();
    }

    /**
     * Create an instance of {@link WsComentario }
     * 
     */
    public WsComentario createWsComentario() {
        return new WsComentario();
    }

    /**
     * Create an instance of {@link ArrayOfWsProduto2 }
     * 
     */
    public ArrayOfWsProduto2 createArrayOfWsProduto2() {
        return new ArrayOfWsProduto2();
    }

    /**
     * Create an instance of {@link ArrayOfWsProduto4 }
     * 
     */
    public ArrayOfWsProduto4 createArrayOfWsProduto4() {
        return new ArrayOfWsProduto4();
    }

    /**
     * Create an instance of {@link WsCompradorTelecom }
     * 
     */
    public WsCompradorTelecom createWsCompradorTelecom() {
        return new WsCompradorTelecom();
    }

    /**
     * Create an instance of {@link ArrayOfWsProduto3 }
     * 
     */
    public ArrayOfWsProduto3 createArrayOfWsProduto3() {
        return new ArrayOfWsProduto3();
    }

    /**
     * Create an instance of {@link ArrayOfWsProdutoTroca }
     * 
     */
    public ArrayOfWsProdutoTroca createArrayOfWsProdutoTroca() {
        return new ArrayOfWsProdutoTroca();
    }

    /**
     * Create an instance of {@link WsAnalistaTroca }
     * 
     */
    public WsAnalistaTroca createWsAnalistaTroca() {
        return new WsAnalistaTroca();
    }

    /**
     * Create an instance of {@link WsEnderecoTelecom }
     * 
     */
    public WsEnderecoTelecom createWsEnderecoTelecom() {
        return new WsEnderecoTelecom();
    }

    /**
     * Create an instance of {@link WsPessoaTroca }
     * 
     */
    public WsPessoaTroca createWsPessoaTroca() {
        return new WsPessoaTroca();
    }

    /**
     * Create an instance of {@link WsRessarcimentoTroca }
     * 
     */
    public WsRessarcimentoTroca createWsRessarcimentoTroca() {
        return new WsRessarcimentoTroca();
    }

    /**
     * Create an instance of {@link WsUsuario }
     * 
     */
    public WsUsuario createWsUsuario() {
        return new WsUsuario();
    }

    /**
     * Create an instance of {@link WsAnaliseTodasSublojas2 }
     * 
     */
    public WsAnaliseTodasSublojas2 createWsAnaliseTodasSublojas2() {
        return new WsAnaliseTodasSublojas2();
    }

    /**
     * Create an instance of {@link WsAnaliseTodasSublojas3 }
     * 
     */
    public WsAnaliseTodasSublojas3 createWsAnaliseTodasSublojas3() {
        return new WsAnaliseTodasSublojas3();
    }

    /**
     * Create an instance of {@link WsEnderecoTroca }
     * 
     */
    public WsEnderecoTroca createWsEnderecoTroca() {
        return new WsEnderecoTroca();
    }

    /**
     * Create an instance of {@link WsEntrega3 }
     * 
     */
    public WsEntrega3 createWsEntrega3() {
        return new WsEntrega3();
    }

    /**
     * Create an instance of {@link WsUsuarioTroca }
     * 
     */
    public WsUsuarioTroca createWsUsuarioTroca() {
        return new WsUsuarioTroca();
    }

    /**
     * Create an instance of {@link WsAeroUsuario }
     * 
     */
    public WsAeroUsuario createWsAeroUsuario() {
        return new WsAeroUsuario();
    }

    /**
     * Create an instance of {@link WsPagamentoTelecom }
     * 
     */
    public WsPagamentoTelecom createWsPagamentoTelecom() {
        return new WsPagamentoTelecom();
    }

    /**
     * Create an instance of {@link WsContaBancariaTroca }
     * 
     */
    public WsContaBancariaTroca createWsContaBancariaTroca() {
        return new WsContaBancariaTroca();
    }

    /**
     * Create an instance of {@link WsPagamento }
     * 
     */
    public WsPagamento createWsPagamento() {
        return new WsPagamento();
    }

    /**
     * Create an instance of {@link ArrayOfWsProduto }
     * 
     */
    public ArrayOfWsProduto createArrayOfWsProduto() {
        return new ArrayOfWsProduto();
    }

    /**
     * Create an instance of {@link WsUsuarioTelecom }
     * 
     */
    public WsUsuarioTelecom createWsUsuarioTelecom() {
        return new WsUsuarioTelecom();
    }

    /**
     * Create an instance of {@link WsCartaoTelecom }
     * 
     */
    public WsCartaoTelecom createWsCartaoTelecom() {
        return new WsCartaoTelecom();
    }

    /**
     * Create an instance of {@link WsAeroBilhete }
     * 
     */
    public WsAeroBilhete createWsAeroBilhete() {
        return new WsAeroBilhete();
    }

    /**
     * Create an instance of {@link ArrayOfWsResultadoValidacaoTroca }
     * 
     */
    public ArrayOfWsResultadoValidacaoTroca createArrayOfWsResultadoValidacaoTroca() {
        return new ArrayOfWsResultadoValidacaoTroca();
    }

    /**
     * Create an instance of {@link WsHistorico }
     * 
     */
    public WsHistorico createWsHistorico() {
        return new WsHistorico();
    }

    /**
     * Create an instance of {@link WsEndereco }
     * 
     */
    public WsEndereco createWsEndereco() {
        return new WsEndereco();
    }

    /**
     * Create an instance of {@link WsAeroPagamento }
     * 
     */
    public WsAeroPagamento createWsAeroPagamento() {
        return new WsAeroPagamento();
    }

    /**
     * Create an instance of {@link ArrayOfWsBilheteViagem }
     * 
     */
    public ArrayOfWsBilheteViagem createArrayOfWsBilheteViagem() {
        return new ArrayOfWsBilheteViagem();
    }

    /**
     * Create an instance of {@link ArrayOfWsProdutoRessarcidoTroca }
     * 
     */
    public ArrayOfWsProdutoRessarcidoTroca createArrayOfWsProdutoRessarcidoTroca() {
        return new ArrayOfWsProdutoRessarcidoTroca();
    }

    /**
     * Create an instance of {@link WsRiscoAnalise }
     * 
     */
    public WsRiscoAnalise createWsRiscoAnalise() {
        return new WsRiscoAnalise();
    }

    /**
     * Create an instance of {@link WsAeroComprador }
     * 
     */
    public WsAeroComprador createWsAeroComprador() {
        return new WsAeroComprador();
    }

    /**
     * Create an instance of {@link WsPessoaViagem }
     * 
     */
    public WsPessoaViagem createWsPessoaViagem() {
        return new WsPessoaViagem();
    }

    /**
     * Create an instance of {@link WsHistorico2 }
     * 
     */
    public WsHistorico2 createWsHistorico2() {
        return new WsHistorico2();
    }

    /**
     * Create an instance of {@link WsProduto }
     * 
     */
    public WsProduto createWsProduto() {
        return new WsProduto();
    }

    /**
     * Create an instance of {@link WsCartaoViagem }
     * 
     */
    public WsCartaoViagem createWsCartaoViagem() {
        return new WsCartaoViagem();
    }

    /**
     * Create an instance of {@link WsPagamentoViagem2 }
     * 
     */
    public WsPagamentoViagem2 createWsPagamentoViagem2() {
        return new WsPagamentoViagem2();
    }

    /**
     * Create an instance of {@link WsPagamento2 }
     * 
     */
    public WsPagamento2 createWsPagamento2() {
        return new WsPagamento2();
    }

    /**
     * Create an instance of {@link WsFila }
     * 
     */
    public WsFila createWsFila() {
        return new WsFila();
    }

    /**
     * Create an instance of {@link WsUsuarioViagem }
     * 
     */
    public WsUsuarioViagem createWsUsuarioViagem() {
        return new WsUsuarioViagem();
    }

    /**
     * Create an instance of {@link ArrayOfWsTelefoneTroca }
     * 
     */
    public ArrayOfWsTelefoneTroca createArrayOfWsTelefoneTroca() {
        return new ArrayOfWsTelefoneTroca();
    }

    /**
     * Create an instance of {@link WsPagamentoViagem }
     * 
     */
    public WsPagamentoViagem createWsPagamentoViagem() {
        return new WsPagamentoViagem();
    }

    /**
     * Create an instance of {@link WsAnaliseTodasSublojas }
     * 
     */
    public WsAnaliseTodasSublojas createWsAnaliseTodasSublojas() {
        return new WsAnaliseTodasSublojas();
    }

    /**
     * Create an instance of {@link WsAeroEndereco }
     * 
     */
    public WsAeroEndereco createWsAeroEndereco() {
        return new WsAeroEndereco();
    }

    /**
     * Create an instance of {@link WsRiscoAnalise2 }
     * 
     */
    public WsRiscoAnalise2 createWsRiscoAnalise2() {
        return new WsRiscoAnalise2();
    }

    /**
     * Create an instance of {@link WsCompradorTelecom2 }
     * 
     */
    public WsCompradorTelecom2 createWsCompradorTelecom2() {
        return new WsCompradorTelecom2();
    }

    /**
     * Create an instance of {@link ArrayOfWsPagamentoViagem2 }
     * 
     */
    public ArrayOfWsPagamentoViagem2 createArrayOfWsPagamentoViagem2() {
        return new ArrayOfWsPagamentoViagem2();
    }

    /**
     * Create an instance of {@link WsFuncionarioTroca }
     * 
     */
    public WsFuncionarioTroca createWsFuncionarioTroca() {
        return new WsFuncionarioTroca();
    }

    /**
     * Create an instance of {@link ArrayOfWsHistorico }
     * 
     */
    public ArrayOfWsHistorico createArrayOfWsHistorico() {
        return new ArrayOfWsHistorico();
    }

    /**
     * Create an instance of {@link WsComprador }
     * 
     */
    public WsComprador createWsComprador() {
        return new WsComprador();
    }

    /**
     * Create an instance of {@link WsCartao }
     * 
     */
    public WsCartao createWsCartao() {
        return new WsCartao();
    }

    /**
     * Create an instance of {@link WsCompradorViagem4 }
     * 
     */
    public WsCompradorViagem4 createWsCompradorViagem4() {
        return new WsCompradorViagem4();
    }

    /**
     * Create an instance of {@link WsCompradorViagem3 }
     * 
     */
    public WsCompradorViagem3 createWsCompradorViagem3() {
        return new WsCompradorViagem3();
    }

    /**
     * Create an instance of {@link WsBilheteViagem }
     * 
     */
    public WsBilheteViagem createWsBilheteViagem() {
        return new WsBilheteViagem();
    }

    /**
     * Create an instance of {@link WsProduto2 }
     * 
     */
    public WsProduto2 createWsProduto2() {
        return new WsProduto2();
    }

    /**
     * Create an instance of {@link ArrayOfWsPagamento2 }
     * 
     */
    public ArrayOfWsPagamento2 createArrayOfWsPagamento2() {
        return new ArrayOfWsPagamento2();
    }

    /**
     * Create an instance of {@link WsAeroPagamento2 }
     * 
     */
    public WsAeroPagamento2 createWsAeroPagamento2() {
        return new WsAeroPagamento2();
    }

    /**
     * Create an instance of {@link WsComprador2 }
     * 
     */
    public WsComprador2 createWsComprador2() {
        return new WsComprador2();
    }

    /**
     * Create an instance of {@link WsProduto4 }
     * 
     */
    public WsProduto4 createWsProduto4() {
        return new WsProduto4();
    }

    /**
     * Create an instance of {@link WsProduto3 }
     * 
     */
    public WsProduto3 createWsProduto3() {
        return new WsProduto3();
    }

    /**
     * Create an instance of {@link WsTelefoneTroca }
     * 
     */
    public WsTelefoneTroca createWsTelefoneTroca() {
        return new WsTelefoneTroca();
    }

    /**
     * Create an instance of {@link WsProdutoTroca }
     * 
     */
    public WsProdutoTroca createWsProdutoTroca() {
        return new WsProdutoTroca();
    }

    /**
     * Create an instance of {@link WsAeroBilhete2 }
     * 
     */
    public WsAeroBilhete2 createWsAeroBilhete2() {
        return new WsAeroBilhete2();
    }

    /**
     * Create an instance of {@link ArrayOfWsPagamentoViagem }
     * 
     */
    public ArrayOfWsPagamentoViagem createArrayOfWsPagamentoViagem() {
        return new ArrayOfWsPagamentoViagem();
    }

    /**
     * Create an instance of {@link WsProdutoRessarcidoTroca }
     * 
     */
    public WsProdutoRessarcidoTroca createWsProdutoRessarcidoTroca() {
        return new WsProdutoRessarcidoTroca();
    }

    /**
     * Create an instance of {@link WsEntrega }
     * 
     */
    public WsEntrega createWsEntrega() {
        return new WsEntrega();
    }

    /**
     * Create an instance of {@link ArrayOfWsHistorico2 }
     * 
     */
    public ArrayOfWsHistorico2 createArrayOfWsHistorico2() {
        return new ArrayOfWsHistorico2();
    }

    /**
     * Create an instance of {@link WsRiscoAnaliseTroca }
     * 
     */
    public WsRiscoAnaliseTroca createWsRiscoAnaliseTroca() {
        return new WsRiscoAnaliseTroca();
    }

    /**
     * Create an instance of {@link WsResultadoValidacaoTroca }
     * 
     */
    public WsResultadoValidacaoTroca createWsResultadoValidacaoTroca() {
        return new WsResultadoValidacaoTroca();
    }

    /**
     * Create an instance of {@link WsCompradorViagem }
     * 
     */
    public WsCompradorViagem createWsCompradorViagem() {
        return new WsCompradorViagem();
    }

    /**
     * Create an instance of {@link WsEnderecoViagem }
     * 
     */
    public WsEnderecoViagem createWsEnderecoViagem() {
        return new WsEnderecoViagem();
    }

    /**
     * Create an instance of {@link ArrayOfWsAeroBilhete }
     * 
     */
    public ArrayOfWsAeroBilhete createArrayOfWsAeroBilhete() {
        return new ArrayOfWsAeroBilhete();
    }

    /**
     * Create an instance of {@link WsExtra }
     * 
     */
    public WsExtra createWsExtra() {
        return new WsExtra();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link WsPagador }
     * 
     */
    public WsPagador createWsPagador() {
        return new WsPagador();
    }

    /**
     * Create an instance of {@link ArrayOfWsPagador }
     * 
     */
    public ArrayOfWsPagador createArrayOfWsPagador() {
        return new ArrayOfWsPagador();
    }

}
