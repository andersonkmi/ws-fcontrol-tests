
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsResultadoAnalise2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsResultadoAnalise2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultadoValidacao" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *         &lt;element name="ResultadoRisco" type="{http://tempuri.org/}WsRiscoAnalise2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsResultadoAnalise2", propOrder = {
    "resultadoValidacao",
    "resultadoRisco"
})
public class WsResultadoAnalise2 {

    @XmlElement(name = "ResultadoValidacao")
    protected WsResultado resultadoValidacao;
    @XmlElement(name = "ResultadoRisco")
    protected WsRiscoAnalise2 resultadoRisco;

    /**
     * Gets the value of the resultadoValidacao property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getResultadoValidacao() {
        return resultadoValidacao;
    }

    /**
     * Sets the value of the resultadoValidacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setResultadoValidacao(WsResultado value) {
        this.resultadoValidacao = value;
    }

    /**
     * Gets the value of the resultadoRisco property.
     * 
     * @return
     *     possible object is
     *     {@link WsRiscoAnalise2 }
     *     
     */
    public WsRiscoAnalise2 getResultadoRisco() {
        return resultadoRisco;
    }

    /**
     * Sets the value of the resultadoRisco property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsRiscoAnalise2 }
     *     
     */
    public void setResultadoRisco(WsRiscoAnalise2 value) {
        this.resultadoRisco = value;
    }

}
