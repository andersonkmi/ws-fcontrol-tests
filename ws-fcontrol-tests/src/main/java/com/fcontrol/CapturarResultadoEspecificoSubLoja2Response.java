
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadoEspecificoSubLoja2Result" type="{http://tempuri.org/}WsAnalise2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadoEspecificoSubLoja2Result"
})
@XmlRootElement(name = "capturarResultadoEspecificoSubLoja2Response")
public class CapturarResultadoEspecificoSubLoja2Response {

    protected WsAnalise2 capturarResultadoEspecificoSubLoja2Result;

    /**
     * Gets the value of the capturarResultadoEspecificoSubLoja2Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsAnalise2 }
     *     
     */
    public WsAnalise2 getCapturarResultadoEspecificoSubLoja2Result() {
        return capturarResultadoEspecificoSubLoja2Result;
    }

    /**
     * Sets the value of the capturarResultadoEspecificoSubLoja2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAnalise2 }
     *     
     */
    public void setCapturarResultadoEspecificoSubLoja2Result(WsAnalise2 value) {
        this.capturarResultadoEspecificoSubLoja2Result = value;
    }

}
