
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultados2Result" type="{http://tempuri.org/}ArrayOfWsAnalise2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultados2Result"
})
@XmlRootElement(name = "capturarResultados2Response")
public class CapturarResultados2Response {

    protected ArrayOfWsAnalise2 capturarResultados2Result;

    /**
     * Gets the value of the capturarResultados2Result property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsAnalise2 }
     *     
     */
    public ArrayOfWsAnalise2 getCapturarResultados2Result() {
        return capturarResultados2Result;
    }

    /**
     * Sets the value of the capturarResultados2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsAnalise2 }
     *     
     */
    public void setCapturarResultados2Result(ArrayOfWsAnalise2 value) {
        this.capturarResultados2Result = value;
    }

}
