
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="analisarTransacaoTelecom4Result" type="{http://tempuri.org/}WsResultadoAnalise2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarTransacaoTelecom4Result"
})
@XmlRootElement(name = "analisarTransacaoTelecom4Response")
public class AnalisarTransacaoTelecom4Response {

    protected WsResultadoAnalise2 analisarTransacaoTelecom4Result;

    /**
     * Gets the value of the analisarTransacaoTelecom4Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultadoAnalise2 }
     *     
     */
    public WsResultadoAnalise2 getAnalisarTransacaoTelecom4Result() {
        return analisarTransacaoTelecom4Result;
    }

    /**
     * Sets the value of the analisarTransacaoTelecom4Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultadoAnalise2 }
     *     
     */
    public void setAnalisarTransacaoTelecom4Result(WsResultadoAnalise2 value) {
        this.analisarTransacaoTelecom4Result = value;
    }

}
