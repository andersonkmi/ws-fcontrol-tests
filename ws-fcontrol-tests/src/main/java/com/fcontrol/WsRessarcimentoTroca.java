
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsRessarcimentoTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsRessarcimentoTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProdutosRessarcidos" type="{http://tempuri.org/}ArrayOfWsProdutoRessarcidoTroca" minOccurs="0"/>
 *         &lt;element name="DadosReembolso" type="{http://tempuri.org/}WsContaBancariaTroca" minOccurs="0"/>
 *         &lt;element name="FormaReembolso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ValorReembolso" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="EnderecoTroca" type="{http://tempuri.org/}WsEnderecoTroca" minOccurs="0"/>
 *         &lt;element name="EnderecoTrocaAlterado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ValorReembolsoAlterado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsRessarcimentoTroca", propOrder = {
    "tipo",
    "produtosRessarcidos",
    "dadosReembolso",
    "formaReembolso",
    "valorReembolso",
    "enderecoTroca",
    "enderecoTrocaAlterado",
    "valorReembolsoAlterado"
})
public class WsRessarcimentoTroca {

    @XmlElement(name = "Tipo")
    protected String tipo;
    @XmlElement(name = "ProdutosRessarcidos")
    protected ArrayOfWsProdutoRessarcidoTroca produtosRessarcidos;
    @XmlElement(name = "DadosReembolso")
    protected WsContaBancariaTroca dadosReembolso;
    @XmlElement(name = "FormaReembolso")
    protected String formaReembolso;
    @XmlElement(name = "ValorReembolso")
    protected double valorReembolso;
    @XmlElement(name = "EnderecoTroca")
    protected WsEnderecoTroca enderecoTroca;
    @XmlElement(name = "EnderecoTrocaAlterado")
    protected boolean enderecoTrocaAlterado;
    @XmlElement(name = "ValorReembolsoAlterado")
    protected boolean valorReembolsoAlterado;

    /**
     * Gets the value of the tipo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Sets the value of the tipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipo(String value) {
        this.tipo = value;
    }

    /**
     * Gets the value of the produtosRessarcidos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsProdutoRessarcidoTroca }
     *     
     */
    public ArrayOfWsProdutoRessarcidoTroca getProdutosRessarcidos() {
        return produtosRessarcidos;
    }

    /**
     * Sets the value of the produtosRessarcidos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsProdutoRessarcidoTroca }
     *     
     */
    public void setProdutosRessarcidos(ArrayOfWsProdutoRessarcidoTroca value) {
        this.produtosRessarcidos = value;
    }

    /**
     * Gets the value of the dadosReembolso property.
     * 
     * @return
     *     possible object is
     *     {@link WsContaBancariaTroca }
     *     
     */
    public WsContaBancariaTroca getDadosReembolso() {
        return dadosReembolso;
    }

    /**
     * Sets the value of the dadosReembolso property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsContaBancariaTroca }
     *     
     */
    public void setDadosReembolso(WsContaBancariaTroca value) {
        this.dadosReembolso = value;
    }

    /**
     * Gets the value of the formaReembolso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaReembolso() {
        return formaReembolso;
    }

    /**
     * Sets the value of the formaReembolso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaReembolso(String value) {
        this.formaReembolso = value;
    }

    /**
     * Gets the value of the valorReembolso property.
     * 
     */
    public double getValorReembolso() {
        return valorReembolso;
    }

    /**
     * Sets the value of the valorReembolso property.
     * 
     */
    public void setValorReembolso(double value) {
        this.valorReembolso = value;
    }

    /**
     * Gets the value of the enderecoTroca property.
     * 
     * @return
     *     possible object is
     *     {@link WsEnderecoTroca }
     *     
     */
    public WsEnderecoTroca getEnderecoTroca() {
        return enderecoTroca;
    }

    /**
     * Sets the value of the enderecoTroca property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsEnderecoTroca }
     *     
     */
    public void setEnderecoTroca(WsEnderecoTroca value) {
        this.enderecoTroca = value;
    }

    /**
     * Gets the value of the enderecoTrocaAlterado property.
     * 
     */
    public boolean isEnderecoTrocaAlterado() {
        return enderecoTrocaAlterado;
    }

    /**
     * Sets the value of the enderecoTrocaAlterado property.
     * 
     */
    public void setEnderecoTrocaAlterado(boolean value) {
        this.enderecoTrocaAlterado = value;
    }

    /**
     * Gets the value of the valorReembolsoAlterado property.
     * 
     */
    public boolean isValorReembolsoAlterado() {
        return valorReembolsoAlterado;
    }

    /**
     * Sets the value of the valorReembolsoAlterado property.
     * 
     */
    public void setValorReembolsoAlterado(boolean value) {
        this.valorReembolsoAlterado = value;
    }

}
