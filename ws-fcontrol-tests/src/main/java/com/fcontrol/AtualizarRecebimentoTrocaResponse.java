
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="atualizarRecebimentoTrocaResult" type="{http://tempuri.org/}WsResultadoTroca" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "atualizarRecebimentoTrocaResult"
})
@XmlRootElement(name = "atualizarRecebimentoTrocaResponse")
public class AtualizarRecebimentoTrocaResponse {

    protected WsResultadoTroca atualizarRecebimentoTrocaResult;

    /**
     * Gets the value of the atualizarRecebimentoTrocaResult property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultadoTroca }
     *     
     */
    public WsResultadoTroca getAtualizarRecebimentoTrocaResult() {
        return atualizarRecebimentoTrocaResult;
    }

    /**
     * Sets the value of the atualizarRecebimentoTrocaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultadoTroca }
     *     
     */
    public void setAtualizarRecebimentoTrocaResult(WsResultadoTroca value) {
        this.atualizarRecebimentoTrocaResult = value;
    }

}
