
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsTransacaoTelecom3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsTransacaoTelecom3">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoPedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Valor" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DadosUsuario" type="{http://tempuri.org/}WsUsuarioTelecom" minOccurs="0"/>
 *         &lt;element name="DadosComprador" type="{http://tempuri.org/}WsCompradorTelecom2" minOccurs="0"/>
 *         &lt;element name="DadosPagamento" type="{http://tempuri.org/}WsPagamentoTelecom" minOccurs="0"/>
 *         &lt;element name="PedidoDeTeste" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CodigoIntegrador" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsTransacaoTelecom3", propOrder = {
    "codigoPedido",
    "data",
    "valor",
    "observacao",
    "dadosUsuario",
    "dadosComprador",
    "dadosPagamento",
    "pedidoDeTeste",
    "codigoIntegrador"
})
public class WsTransacaoTelecom3 {

    @XmlElement(name = "CodigoPedido")
    protected String codigoPedido;
    @XmlElement(name = "Data", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar data;
    @XmlElement(name = "Valor")
    protected int valor;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(name = "DadosUsuario")
    protected WsUsuarioTelecom dadosUsuario;
    @XmlElement(name = "DadosComprador")
    protected WsCompradorTelecom2 dadosComprador;
    @XmlElement(name = "DadosPagamento")
    protected WsPagamentoTelecom dadosPagamento;
    @XmlElement(name = "PedidoDeTeste")
    protected boolean pedidoDeTeste;
    @XmlElement(name = "CodigoIntegrador")
    protected int codigoIntegrador;

    /**
     * Gets the value of the codigoPedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPedido() {
        return codigoPedido;
    }

    /**
     * Sets the value of the codigoPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPedido(String value) {
        this.codigoPedido = value;
    }

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setData(XMLGregorianCalendar value) {
        this.data = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     */
    public int getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     */
    public void setValor(int value) {
        this.valor = value;
    }

    /**
     * Gets the value of the observacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Sets the value of the observacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Gets the value of the dadosUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link WsUsuarioTelecom }
     *     
     */
    public WsUsuarioTelecom getDadosUsuario() {
        return dadosUsuario;
    }

    /**
     * Sets the value of the dadosUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsUsuarioTelecom }
     *     
     */
    public void setDadosUsuario(WsUsuarioTelecom value) {
        this.dadosUsuario = value;
    }

    /**
     * Gets the value of the dadosComprador property.
     * 
     * @return
     *     possible object is
     *     {@link WsCompradorTelecom2 }
     *     
     */
    public WsCompradorTelecom2 getDadosComprador() {
        return dadosComprador;
    }

    /**
     * Sets the value of the dadosComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsCompradorTelecom2 }
     *     
     */
    public void setDadosComprador(WsCompradorTelecom2 value) {
        this.dadosComprador = value;
    }

    /**
     * Gets the value of the dadosPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link WsPagamentoTelecom }
     *     
     */
    public WsPagamentoTelecom getDadosPagamento() {
        return dadosPagamento;
    }

    /**
     * Sets the value of the dadosPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPagamentoTelecom }
     *     
     */
    public void setDadosPagamento(WsPagamentoTelecom value) {
        this.dadosPagamento = value;
    }

    /**
     * Gets the value of the pedidoDeTeste property.
     * 
     */
    public boolean isPedidoDeTeste() {
        return pedidoDeTeste;
    }

    /**
     * Sets the value of the pedidoDeTeste property.
     * 
     */
    public void setPedidoDeTeste(boolean value) {
        this.pedidoDeTeste = value;
    }

    /**
     * Gets the value of the codigoIntegrador property.
     * 
     */
    public int getCodigoIntegrador() {
        return codigoIntegrador;
    }

    /**
     * Sets the value of the codigoIntegrador property.
     * 
     */
    public void setCodigoIntegrador(int value) {
        this.codigoIntegrador = value;
    }

}
