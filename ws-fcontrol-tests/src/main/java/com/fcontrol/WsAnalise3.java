
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsAnalise3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAnalise3">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsAnalise2">
 *       &lt;sequence>
 *         &lt;element name="Fila" type="{http://tempuri.org/}WsFila" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAnalise3", propOrder = {
    "fila"
})
public class WsAnalise3
    extends WsAnalise2
{

    @XmlElement(name = "Fila")
    protected WsFila fila;

    /**
     * Gets the value of the fila property.
     * 
     * @return
     *     possible object is
     *     {@link WsFila }
     *     
     */
    public WsFila getFila() {
        return fila;
    }

    /**
     * Sets the value of the fila property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsFila }
     *     
     */
    public void setFila(WsFila value) {
        this.fila = value;
    }

}
