
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsAeroTransacao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAeroTransacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoPedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataCompra" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CanalVenda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DadosUsuario" type="{http://tempuri.org/}WsAeroUsuario" minOccurs="0"/>
 *         &lt;element name="DadosComprador" type="{http://tempuri.org/}WsAeroComprador" minOccurs="0"/>
 *         &lt;element name="DadosPagamento" type="{http://tempuri.org/}WsAeroPagamento" minOccurs="0"/>
 *         &lt;element name="Bilhetes" type="{http://tempuri.org/}ArrayOfWsAeroBilhete" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAeroTransacao", propOrder = {
    "codigoPedido",
    "dataCompra",
    "canalVenda",
    "observacao",
    "dadosUsuario",
    "dadosComprador",
    "dadosPagamento",
    "bilhetes"
})
public class WsAeroTransacao {

    @XmlElement(name = "CodigoPedido")
    protected String codigoPedido;
    @XmlElement(name = "DataCompra", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCompra;
    @XmlElement(name = "CanalVenda")
    protected String canalVenda;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(name = "DadosUsuario")
    protected WsAeroUsuario dadosUsuario;
    @XmlElement(name = "DadosComprador")
    protected WsAeroComprador dadosComprador;
    @XmlElement(name = "DadosPagamento")
    protected WsAeroPagamento dadosPagamento;
    @XmlElement(name = "Bilhetes")
    protected ArrayOfWsAeroBilhete bilhetes;

    /**
     * Gets the value of the codigoPedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPedido() {
        return codigoPedido;
    }

    /**
     * Sets the value of the codigoPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPedido(String value) {
        this.codigoPedido = value;
    }

    /**
     * Gets the value of the dataCompra property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCompra() {
        return dataCompra;
    }

    /**
     * Sets the value of the dataCompra property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCompra(XMLGregorianCalendar value) {
        this.dataCompra = value;
    }

    /**
     * Gets the value of the canalVenda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanalVenda() {
        return canalVenda;
    }

    /**
     * Sets the value of the canalVenda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanalVenda(String value) {
        this.canalVenda = value;
    }

    /**
     * Gets the value of the observacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Sets the value of the observacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Gets the value of the dadosUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link WsAeroUsuario }
     *     
     */
    public WsAeroUsuario getDadosUsuario() {
        return dadosUsuario;
    }

    /**
     * Sets the value of the dadosUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAeroUsuario }
     *     
     */
    public void setDadosUsuario(WsAeroUsuario value) {
        this.dadosUsuario = value;
    }

    /**
     * Gets the value of the dadosComprador property.
     * 
     * @return
     *     possible object is
     *     {@link WsAeroComprador }
     *     
     */
    public WsAeroComprador getDadosComprador() {
        return dadosComprador;
    }

    /**
     * Sets the value of the dadosComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAeroComprador }
     *     
     */
    public void setDadosComprador(WsAeroComprador value) {
        this.dadosComprador = value;
    }

    /**
     * Gets the value of the dadosPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link WsAeroPagamento }
     *     
     */
    public WsAeroPagamento getDadosPagamento() {
        return dadosPagamento;
    }

    /**
     * Sets the value of the dadosPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAeroPagamento }
     *     
     */
    public void setDadosPagamento(WsAeroPagamento value) {
        this.dadosPagamento = value;
    }

    /**
     * Gets the value of the bilhetes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsAeroBilhete }
     *     
     */
    public ArrayOfWsAeroBilhete getBilhetes() {
        return bilhetes;
    }

    /**
     * Sets the value of the bilhetes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsAeroBilhete }
     *     
     */
    public void setBilhetes(ArrayOfWsAeroBilhete value) {
        this.bilhetes = value;
    }

}
