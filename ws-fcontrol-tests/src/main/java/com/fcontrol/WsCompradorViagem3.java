
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsCompradorViagem3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCompradorViagem3">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsCompradorViagem">
 *       &lt;sequence>
 *         &lt;element name="ClienteFidelidade" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCompradorViagem3", propOrder = {
    "clienteFidelidade"
})
@XmlSeeAlso({
    WsCompradorViagem4 .class
})
public class WsCompradorViagem3
    extends WsCompradorViagem
{

    @XmlElement(name = "ClienteFidelidade")
    protected boolean clienteFidelidade;

    /**
     * Gets the value of the clienteFidelidade property.
     * 
     */
    public boolean isClienteFidelidade() {
        return clienteFidelidade;
    }

    /**
     * Sets the value of the clienteFidelidade property.
     * 
     */
    public void setClienteFidelidade(boolean value) {
        this.clienteFidelidade = value;
    }

}
