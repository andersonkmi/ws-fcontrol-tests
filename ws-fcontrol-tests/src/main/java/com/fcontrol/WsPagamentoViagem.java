
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsPagamentoViagem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsPagamentoViagem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DadosCartao" type="{http://tempuri.org/}WsCartaoViagem" minOccurs="0"/>
 *         &lt;element name="DadosMae" type="{http://tempuri.org/}WsPessoaViagem" minOccurs="0"/>
 *         &lt;element name="NumeroDeParcelas" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MetodoPagamento" type="{http://tempuri.org/}MetodoPagamento"/>
 *         &lt;element name="Valor" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsPagamentoViagem", propOrder = {
    "dadosCartao",
    "dadosMae",
    "numeroDeParcelas",
    "metodoPagamento",
    "valor"
})
@XmlSeeAlso({
    WsPagamentoViagem2 .class
})
public class WsPagamentoViagem {

    @XmlElement(name = "DadosCartao")
    protected WsCartaoViagem dadosCartao;
    @XmlElement(name = "DadosMae")
    protected WsPessoaViagem dadosMae;
    @XmlElement(name = "NumeroDeParcelas")
    protected int numeroDeParcelas;
    @XmlElement(name = "MetodoPagamento", required = true)
    @XmlSchemaType(name = "string")
    protected MetodoPagamento metodoPagamento;
    @XmlElement(name = "Valor")
    protected int valor;

    /**
     * Gets the value of the dadosCartao property.
     * 
     * @return
     *     possible object is
     *     {@link WsCartaoViagem }
     *     
     */
    public WsCartaoViagem getDadosCartao() {
        return dadosCartao;
    }

    /**
     * Sets the value of the dadosCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsCartaoViagem }
     *     
     */
    public void setDadosCartao(WsCartaoViagem value) {
        this.dadosCartao = value;
    }

    /**
     * Gets the value of the dadosMae property.
     * 
     * @return
     *     possible object is
     *     {@link WsPessoaViagem }
     *     
     */
    public WsPessoaViagem getDadosMae() {
        return dadosMae;
    }

    /**
     * Sets the value of the dadosMae property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPessoaViagem }
     *     
     */
    public void setDadosMae(WsPessoaViagem value) {
        this.dadosMae = value;
    }

    /**
     * Gets the value of the numeroDeParcelas property.
     * 
     */
    public int getNumeroDeParcelas() {
        return numeroDeParcelas;
    }

    /**
     * Sets the value of the numeroDeParcelas property.
     * 
     */
    public void setNumeroDeParcelas(int value) {
        this.numeroDeParcelas = value;
    }

    /**
     * Gets the value of the metodoPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link MetodoPagamento }
     *     
     */
    public MetodoPagamento getMetodoPagamento() {
        return metodoPagamento;
    }

    /**
     * Sets the value of the metodoPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link MetodoPagamento }
     *     
     */
    public void setMetodoPagamento(MetodoPagamento value) {
        this.metodoPagamento = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     */
    public int getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     */
    public void setValor(int value) {
        this.valor = value;
    }

}
