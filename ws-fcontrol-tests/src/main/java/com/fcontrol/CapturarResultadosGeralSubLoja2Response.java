
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadosGeralSubLoja2Result" type="{http://tempuri.org/}ArrayOfWsAnalise2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadosGeralSubLoja2Result"
})
@XmlRootElement(name = "capturarResultadosGeralSubLoja2Response")
public class CapturarResultadosGeralSubLoja2Response {

    protected ArrayOfWsAnalise2 capturarResultadosGeralSubLoja2Result;

    /**
     * Gets the value of the capturarResultadosGeralSubLoja2Result property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsAnalise2 }
     *     
     */
    public ArrayOfWsAnalise2 getCapturarResultadosGeralSubLoja2Result() {
        return capturarResultadosGeralSubLoja2Result;
    }

    /**
     * Sets the value of the capturarResultadosGeralSubLoja2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsAnalise2 }
     *     
     */
    public void setCapturarResultadosGeralSubLoja2Result(ArrayOfWsAnalise2 value) {
        this.capturarResultadosGeralSubLoja2Result = value;
    }

}
