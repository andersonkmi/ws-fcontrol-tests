
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsResultadoAnalise complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsResultadoAnalise">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultadoValidacao" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *         &lt;element name="ResultadoRisco" type="{http://tempuri.org/}WsRiscoAnalise" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsResultadoAnalise", propOrder = {
    "resultadoValidacao",
    "resultadoRisco"
})
public class WsResultadoAnalise {

    @XmlElement(name = "ResultadoValidacao")
    protected WsResultado resultadoValidacao;
    @XmlElement(name = "ResultadoRisco")
    protected WsRiscoAnalise resultadoRisco;

    /**
     * Gets the value of the resultadoValidacao property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getResultadoValidacao() {
        return resultadoValidacao;
    }

    /**
     * Sets the value of the resultadoValidacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setResultadoValidacao(WsResultado value) {
        this.resultadoValidacao = value;
    }

    /**
     * Gets the value of the resultadoRisco property.
     * 
     * @return
     *     possible object is
     *     {@link WsRiscoAnalise }
     *     
     */
    public WsRiscoAnalise getResultadoRisco() {
        return resultadoRisco;
    }

    /**
     * Sets the value of the resultadoRisco property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsRiscoAnalise }
     *     
     */
    public void setResultadoRisco(WsRiscoAnalise value) {
        this.resultadoRisco = value;
    }

}
