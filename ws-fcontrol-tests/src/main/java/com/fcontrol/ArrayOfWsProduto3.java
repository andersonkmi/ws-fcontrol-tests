
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsProduto3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsProduto3">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsProduto3" type="{http://tempuri.org/}WsProduto3" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsProduto3", propOrder = {
    "wsProduto3"
})
public class ArrayOfWsProduto3 {

    @XmlElement(name = "WsProduto3", nillable = true)
    protected List<WsProduto3> wsProduto3;

    /**
     * Gets the value of the wsProduto3 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsProduto3 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsProduto3().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsProduto3 }
     * 
     * 
     */
    public List<WsProduto3> getWsProduto3() {
        if (wsProduto3 == null) {
            wsProduto3 = new ArrayList<WsProduto3>();
        }
        return this.wsProduto3;
    }

}
