
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsTransacaoViagem4 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsTransacaoViagem4">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoPedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataCompra" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CanalVenda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DadosUsuario" type="{http://tempuri.org/}WsUsuarioViagem" minOccurs="0"/>
 *         &lt;element name="DadosComprador" type="{http://tempuri.org/}WsCompradorViagem4" minOccurs="0"/>
 *         &lt;element name="Pagamentos" type="{http://tempuri.org/}ArrayOfWsPagamentoViagem" minOccurs="0"/>
 *         &lt;element name="Bilhetes" type="{http://tempuri.org/}ArrayOfWsBilheteViagem" minOccurs="0"/>
 *         &lt;element name="PNR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VooInternacional" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsTransacaoViagem4", propOrder = {
    "codigoPedido",
    "dataCompra",
    "canalVenda",
    "observacao",
    "dadosUsuario",
    "dadosComprador",
    "pagamentos",
    "bilhetes",
    "pnr",
    "vooInternacional"
})
public class WsTransacaoViagem4 {

    @XmlElement(name = "CodigoPedido")
    protected String codigoPedido;
    @XmlElement(name = "DataCompra", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCompra;
    @XmlElement(name = "CanalVenda")
    protected String canalVenda;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(name = "DadosUsuario")
    protected WsUsuarioViagem dadosUsuario;
    @XmlElement(name = "DadosComprador")
    protected WsCompradorViagem4 dadosComprador;
    @XmlElement(name = "Pagamentos")
    protected ArrayOfWsPagamentoViagem pagamentos;
    @XmlElement(name = "Bilhetes")
    protected ArrayOfWsBilheteViagem bilhetes;
    @XmlElement(name = "PNR")
    protected String pnr;
    @XmlElement(name = "VooInternacional")
    protected boolean vooInternacional;

    /**
     * Gets the value of the codigoPedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPedido() {
        return codigoPedido;
    }

    /**
     * Sets the value of the codigoPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPedido(String value) {
        this.codigoPedido = value;
    }

    /**
     * Gets the value of the dataCompra property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCompra() {
        return dataCompra;
    }

    /**
     * Sets the value of the dataCompra property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCompra(XMLGregorianCalendar value) {
        this.dataCompra = value;
    }

    /**
     * Gets the value of the canalVenda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanalVenda() {
        return canalVenda;
    }

    /**
     * Sets the value of the canalVenda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanalVenda(String value) {
        this.canalVenda = value;
    }

    /**
     * Gets the value of the observacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Sets the value of the observacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Gets the value of the dadosUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link WsUsuarioViagem }
     *     
     */
    public WsUsuarioViagem getDadosUsuario() {
        return dadosUsuario;
    }

    /**
     * Sets the value of the dadosUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsUsuarioViagem }
     *     
     */
    public void setDadosUsuario(WsUsuarioViagem value) {
        this.dadosUsuario = value;
    }

    /**
     * Gets the value of the dadosComprador property.
     * 
     * @return
     *     possible object is
     *     {@link WsCompradorViagem4 }
     *     
     */
    public WsCompradorViagem4 getDadosComprador() {
        return dadosComprador;
    }

    /**
     * Sets the value of the dadosComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsCompradorViagem4 }
     *     
     */
    public void setDadosComprador(WsCompradorViagem4 value) {
        this.dadosComprador = value;
    }

    /**
     * Gets the value of the pagamentos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsPagamentoViagem }
     *     
     */
    public ArrayOfWsPagamentoViagem getPagamentos() {
        return pagamentos;
    }

    /**
     * Sets the value of the pagamentos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsPagamentoViagem }
     *     
     */
    public void setPagamentos(ArrayOfWsPagamentoViagem value) {
        this.pagamentos = value;
    }

    /**
     * Gets the value of the bilhetes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsBilheteViagem }
     *     
     */
    public ArrayOfWsBilheteViagem getBilhetes() {
        return bilhetes;
    }

    /**
     * Sets the value of the bilhetes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsBilheteViagem }
     *     
     */
    public void setBilhetes(ArrayOfWsBilheteViagem value) {
        this.bilhetes = value;
    }

    /**
     * Gets the value of the pnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPNR() {
        return pnr;
    }

    /**
     * Sets the value of the pnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPNR(String value) {
        this.pnr = value;
    }

    /**
     * Gets the value of the vooInternacional property.
     * 
     */
    public boolean isVooInternacional() {
        return vooInternacional;
    }

    /**
     * Sets the value of the vooInternacional property.
     * 
     */
    public void setVooInternacional(boolean value) {
        this.vooInternacional = value;
    }

}
