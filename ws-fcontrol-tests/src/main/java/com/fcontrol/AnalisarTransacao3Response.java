
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="analisarTransacao3Result" type="{http://tempuri.org/}WsResultadoAnalise" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarTransacao3Result"
})
@XmlRootElement(name = "analisarTransacao3Response")
public class AnalisarTransacao3Response {

    protected WsResultadoAnalise analisarTransacao3Result;

    /**
     * Gets the value of the analisarTransacao3Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultadoAnalise }
     *     
     */
    public WsResultadoAnalise getAnalisarTransacao3Result() {
        return analisarTransacao3Result;
    }

    /**
     * Sets the value of the analisarTransacao3Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultadoAnalise }
     *     
     */
    public void setAnalisarTransacao3Result(WsResultadoAnalise value) {
        this.analisarTransacao3Result = value;
    }

}
