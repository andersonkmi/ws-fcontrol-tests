
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enfileirarTransacao2Result" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enfileirarTransacao2Result"
})
@XmlRootElement(name = "enfileirarTransacao2Response")
public class EnfileirarTransacao2Response {

    protected WsResultado enfileirarTransacao2Result;

    /**
     * Gets the value of the enfileirarTransacao2Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getEnfileirarTransacao2Result() {
        return enfileirarTransacao2Result;
    }

    /**
     * Sets the value of the enfileirarTransacao2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setEnfileirarTransacao2Result(WsResultado value) {
        this.enfileirarTransacao2Result = value;
    }

}
