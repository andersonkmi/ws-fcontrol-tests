
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsAnaliseTodasSublojas3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsAnaliseTodasSublojas3">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsAnaliseTodasSublojas3" type="{http://tempuri.org/}WsAnaliseTodasSublojas3" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsAnaliseTodasSublojas3", propOrder = {
    "wsAnaliseTodasSublojas3"
})
public class ArrayOfWsAnaliseTodasSublojas3 {

    @XmlElement(name = "WsAnaliseTodasSublojas3", nillable = true)
    protected List<WsAnaliseTodasSublojas3> wsAnaliseTodasSublojas3;

    /**
     * Gets the value of the wsAnaliseTodasSublojas3 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsAnaliseTodasSublojas3 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsAnaliseTodasSublojas3().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsAnaliseTodasSublojas3 }
     * 
     * 
     */
    public List<WsAnaliseTodasSublojas3> getWsAnaliseTodasSublojas3() {
        if (wsAnaliseTodasSublojas3 == null) {
            wsAnaliseTodasSublojas3 = new ArrayList<WsAnaliseTodasSublojas3>();
        }
        return this.wsAnaliseTodasSublojas3;
    }

}
