
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadosSubLojaResult" type="{http://tempuri.org/}ArrayOfWsAnalise" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadosSubLojaResult"
})
@XmlRootElement(name = "capturarResultadosSubLojaResponse")
public class CapturarResultadosSubLojaResponse {

    protected ArrayOfWsAnalise capturarResultadosSubLojaResult;

    /**
     * Gets the value of the capturarResultadosSubLojaResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsAnalise }
     *     
     */
    public ArrayOfWsAnalise getCapturarResultadosSubLojaResult() {
        return capturarResultadosSubLojaResult;
    }

    /**
     * Sets the value of the capturarResultadosSubLojaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsAnalise }
     *     
     */
    public void setCapturarResultadosSubLojaResult(ArrayOfWsAnalise value) {
        this.capturarResultadosSubLojaResult = value;
    }

}
