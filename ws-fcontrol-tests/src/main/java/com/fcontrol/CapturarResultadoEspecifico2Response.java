
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadoEspecifico2Result" type="{http://tempuri.org/}WsAnalise2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadoEspecifico2Result"
})
@XmlRootElement(name = "capturarResultadoEspecifico2Response")
public class CapturarResultadoEspecifico2Response {

    protected WsAnalise2 capturarResultadoEspecifico2Result;

    /**
     * Gets the value of the capturarResultadoEspecifico2Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsAnalise2 }
     *     
     */
    public WsAnalise2 getCapturarResultadoEspecifico2Result() {
        return capturarResultadoEspecifico2Result;
    }

    /**
     * Sets the value of the capturarResultadoEspecifico2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAnalise2 }
     *     
     */
    public void setCapturarResultadoEspecifico2Result(WsAnalise2 value) {
        this.capturarResultadoEspecifico2Result = value;
    }

}
