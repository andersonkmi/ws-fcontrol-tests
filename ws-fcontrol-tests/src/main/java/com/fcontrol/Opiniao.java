
package com.fcontrol;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Opiniao.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Opiniao">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Nulo"/>
 *     &lt;enumeration value="Normal"/>
 *     &lt;enumeration value="Suspeita"/>
 *     &lt;enumeration value="Fraude"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Opiniao")
@XmlEnum
public enum Opiniao {

    @XmlEnumValue("Nulo")
    NULO("Nulo"),
    @XmlEnumValue("Normal")
    NORMAL("Normal"),
    @XmlEnumValue("Suspeita")
    SUSPEITA("Suspeita"),
    @XmlEnumValue("Fraude")
    FRAUDE("Fraude");
    private final String value;

    Opiniao(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Opiniao fromValue(String v) {
        for (Opiniao c: Opiniao.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
