
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsRetornoTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsRetornoTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Sucesso" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ResultadoValidacao" type="{http://tempuri.org/}ArrayOfWsResultadoValidacaoTroca" minOccurs="0"/>
 *         &lt;element name="ResultadoAnalise" type="{http://tempuri.org/}WsRiscoAnaliseTroca" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsRetornoTroca", propOrder = {
    "sucesso",
    "resultadoValidacao",
    "resultadoAnalise"
})
public class WsRetornoTroca {

    @XmlElement(name = "Sucesso")
    protected boolean sucesso;
    @XmlElement(name = "ResultadoValidacao")
    protected ArrayOfWsResultadoValidacaoTroca resultadoValidacao;
    @XmlElement(name = "ResultadoAnalise")
    protected WsRiscoAnaliseTroca resultadoAnalise;

    /**
     * Gets the value of the sucesso property.
     * 
     */
    public boolean isSucesso() {
        return sucesso;
    }

    /**
     * Sets the value of the sucesso property.
     * 
     */
    public void setSucesso(boolean value) {
        this.sucesso = value;
    }

    /**
     * Gets the value of the resultadoValidacao property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsResultadoValidacaoTroca }
     *     
     */
    public ArrayOfWsResultadoValidacaoTroca getResultadoValidacao() {
        return resultadoValidacao;
    }

    /**
     * Sets the value of the resultadoValidacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsResultadoValidacaoTroca }
     *     
     */
    public void setResultadoValidacao(ArrayOfWsResultadoValidacaoTroca value) {
        this.resultadoValidacao = value;
    }

    /**
     * Gets the value of the resultadoAnalise property.
     * 
     * @return
     *     possible object is
     *     {@link WsRiscoAnaliseTroca }
     *     
     */
    public WsRiscoAnaliseTroca getResultadoAnalise() {
        return resultadoAnalise;
    }

    /**
     * Sets the value of the resultadoAnalise property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsRiscoAnaliseTroca }
     *     
     */
    public void setResultadoAnalise(WsRiscoAnaliseTroca value) {
        this.resultadoAnalise = value;
    }

}
