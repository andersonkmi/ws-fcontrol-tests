
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ano" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="limite" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="comMetodosDePagamento" type="{http://tempuri.org/}ArrayOfMetodoPagamento" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "login",
    "senha",
    "mes",
    "ano",
    "limite",
    "comMetodosDePagamento"
})
@XmlRootElement(name = "capturarResultadosTodasSubLojas4")
public class CapturarResultadosTodasSubLojas4 {

    protected String login;
    protected String senha;
    protected int mes;
    protected int ano;
    protected int limite;
    protected ArrayOfMetodoPagamento comMetodosDePagamento;

    /**
     * Gets the value of the login property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the value of the login property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogin(String value) {
        this.login = value;
    }

    /**
     * Gets the value of the senha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenha() {
        return senha;
    }

    /**
     * Sets the value of the senha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenha(String value) {
        this.senha = value;
    }

    /**
     * Gets the value of the mes property.
     * 
     */
    public int getMes() {
        return mes;
    }

    /**
     * Sets the value of the mes property.
     * 
     */
    public void setMes(int value) {
        this.mes = value;
    }

    /**
     * Gets the value of the ano property.
     * 
     */
    public int getAno() {
        return ano;
    }

    /**
     * Sets the value of the ano property.
     * 
     */
    public void setAno(int value) {
        this.ano = value;
    }

    /**
     * Gets the value of the limite property.
     * 
     */
    public int getLimite() {
        return limite;
    }

    /**
     * Sets the value of the limite property.
     * 
     */
    public void setLimite(int value) {
        this.limite = value;
    }

    /**
     * Gets the value of the comMetodosDePagamento property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMetodoPagamento }
     *     
     */
    public ArrayOfMetodoPagamento getComMetodosDePagamento() {
        return comMetodosDePagamento;
    }

    /**
     * Sets the value of the comMetodosDePagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMetodoPagamento }
     *     
     */
    public void setComMetodosDePagamento(ArrayOfMetodoPagamento value) {
        this.comMetodosDePagamento = value;
    }

}
