
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoPedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sexoComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ruaComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="complementoComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bairroComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cidadeComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ufComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paisComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cepComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpfComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dddComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefoneComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dddCelularComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="celularComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emailComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senhaComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ruaEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="complementoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bairroEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cidadeEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ufEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paisEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cepEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dddEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefoneEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dddCelularEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="celularEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="quantidadeItensDistintos" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="quantidadeTotalItens" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="valorTotalCompra" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="dataCompra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="metodoPagamento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroParcelas" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nsu" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="valorTotalFrete" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="paraPresente" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="prazoEntregaDias" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="formaEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeBancoEmissor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeTitularCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataValidadeCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="canalVenda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigosProdutos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descricoesProdutos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="quantidadesProdutos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valoresProdutos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="categoriasProdutos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pedidoTeste" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="mensagemRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefoneextraddd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefoneextra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="quatroultimosdigitos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpftitularcartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identificadorLojaFilho" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataNascimentoComprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefoneEntrega2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dddEntrega2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "login",
    "senha",
    "codigoPedido",
    "nomeComprador",
    "sexoComprador",
    "ruaComprador",
    "numeroComprador",
    "complementoComprador",
    "bairroComprador",
    "cidadeComprador",
    "ufComprador",
    "paisComprador",
    "cepComprador",
    "cpfComprador",
    "dddComprador",
    "telefoneComprador",
    "dddCelularComprador",
    "celularComprador",
    "emailComprador",
    "senhaComprador",
    "nomeEntrega",
    "ruaEntrega",
    "numeroEntrega",
    "complementoEntrega",
    "bairroEntrega",
    "cidadeEntrega",
    "ufEntrega",
    "paisEntrega",
    "cepEntrega",
    "dddEntrega",
    "telefoneEntrega",
    "dddCelularEntrega",
    "celularEntrega",
    "ip",
    "quantidadeItensDistintos",
    "quantidadeTotalItens",
    "valorTotalCompra",
    "dataCompra",
    "metodoPagamento",
    "numeroParcelas",
    "nsu",
    "valorTotalFrete",
    "paraPresente",
    "prazoEntregaDias",
    "formaEntrega",
    "nomeBancoEmissor",
    "numeroCartao",
    "nomeTitularCartao",
    "dataValidadeCartao",
    "observacao",
    "canalVenda",
    "codigosProdutos",
    "descricoesProdutos",
    "quantidadesProdutos",
    "valoresProdutos",
    "categoriasProdutos",
    "pedidoTeste",
    "mensagemRetorno",
    "telefoneextraddd",
    "telefoneextra",
    "bin",
    "quatroultimosdigitos",
    "cpftitularcartao",
    "identificadorLojaFilho",
    "dataNascimentoComprador",
    "telefoneEntrega2",
    "dddEntrega2",
    "dataEntrega"
})
@XmlRootElement(name = "enfileirarTransacaoManual2")
public class EnfileirarTransacaoManual2 {

    protected String login;
    protected String senha;
    protected String codigoPedido;
    protected String nomeComprador;
    protected String sexoComprador;
    protected String ruaComprador;
    protected String numeroComprador;
    protected String complementoComprador;
    protected String bairroComprador;
    protected String cidadeComprador;
    protected String ufComprador;
    protected String paisComprador;
    protected String cepComprador;
    protected String cpfComprador;
    protected String dddComprador;
    protected String telefoneComprador;
    protected String dddCelularComprador;
    protected String celularComprador;
    protected String emailComprador;
    protected String senhaComprador;
    protected String nomeEntrega;
    protected String ruaEntrega;
    protected String numeroEntrega;
    protected String complementoEntrega;
    protected String bairroEntrega;
    protected String cidadeEntrega;
    protected String ufEntrega;
    protected String paisEntrega;
    protected String cepEntrega;
    protected String dddEntrega;
    protected String telefoneEntrega;
    protected String dddCelularEntrega;
    protected String celularEntrega;
    protected String ip;
    protected int quantidadeItensDistintos;
    protected int quantidadeTotalItens;
    protected int valorTotalCompra;
    protected String dataCompra;
    protected int metodoPagamento;
    protected int numeroParcelas;
    protected long nsu;
    protected int valorTotalFrete;
    protected boolean paraPresente;
    protected int prazoEntregaDias;
    protected String formaEntrega;
    protected String nomeBancoEmissor;
    protected String numeroCartao;
    protected String nomeTitularCartao;
    protected String dataValidadeCartao;
    protected String observacao;
    protected String canalVenda;
    protected String codigosProdutos;
    protected String descricoesProdutos;
    protected String quantidadesProdutos;
    protected String valoresProdutos;
    protected String categoriasProdutos;
    protected boolean pedidoTeste;
    protected String mensagemRetorno;
    protected String telefoneextraddd;
    protected String telefoneextra;
    protected String bin;
    protected String quatroultimosdigitos;
    protected String cpftitularcartao;
    protected String identificadorLojaFilho;
    protected String dataNascimentoComprador;
    protected String telefoneEntrega2;
    protected String dddEntrega2;
    protected String dataEntrega;

    /**
     * Gets the value of the login property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the value of the login property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogin(String value) {
        this.login = value;
    }

    /**
     * Gets the value of the senha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenha() {
        return senha;
    }

    /**
     * Sets the value of the senha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenha(String value) {
        this.senha = value;
    }

    /**
     * Gets the value of the codigoPedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPedido() {
        return codigoPedido;
    }

    /**
     * Sets the value of the codigoPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPedido(String value) {
        this.codigoPedido = value;
    }

    /**
     * Gets the value of the nomeComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeComprador() {
        return nomeComprador;
    }

    /**
     * Sets the value of the nomeComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeComprador(String value) {
        this.nomeComprador = value;
    }

    /**
     * Gets the value of the sexoComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSexoComprador() {
        return sexoComprador;
    }

    /**
     * Sets the value of the sexoComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSexoComprador(String value) {
        this.sexoComprador = value;
    }

    /**
     * Gets the value of the ruaComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuaComprador() {
        return ruaComprador;
    }

    /**
     * Sets the value of the ruaComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuaComprador(String value) {
        this.ruaComprador = value;
    }

    /**
     * Gets the value of the numeroComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroComprador() {
        return numeroComprador;
    }

    /**
     * Sets the value of the numeroComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroComprador(String value) {
        this.numeroComprador = value;
    }

    /**
     * Gets the value of the complementoComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplementoComprador() {
        return complementoComprador;
    }

    /**
     * Sets the value of the complementoComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplementoComprador(String value) {
        this.complementoComprador = value;
    }

    /**
     * Gets the value of the bairroComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBairroComprador() {
        return bairroComprador;
    }

    /**
     * Sets the value of the bairroComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBairroComprador(String value) {
        this.bairroComprador = value;
    }

    /**
     * Gets the value of the cidadeComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidadeComprador() {
        return cidadeComprador;
    }

    /**
     * Sets the value of the cidadeComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidadeComprador(String value) {
        this.cidadeComprador = value;
    }

    /**
     * Gets the value of the ufComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfComprador() {
        return ufComprador;
    }

    /**
     * Sets the value of the ufComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfComprador(String value) {
        this.ufComprador = value;
    }

    /**
     * Gets the value of the paisComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaisComprador() {
        return paisComprador;
    }

    /**
     * Sets the value of the paisComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaisComprador(String value) {
        this.paisComprador = value;
    }

    /**
     * Gets the value of the cepComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCepComprador() {
        return cepComprador;
    }

    /**
     * Sets the value of the cepComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCepComprador(String value) {
        this.cepComprador = value;
    }

    /**
     * Gets the value of the cpfComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfComprador() {
        return cpfComprador;
    }

    /**
     * Sets the value of the cpfComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfComprador(String value) {
        this.cpfComprador = value;
    }

    /**
     * Gets the value of the dddComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddComprador() {
        return dddComprador;
    }

    /**
     * Sets the value of the dddComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddComprador(String value) {
        this.dddComprador = value;
    }

    /**
     * Gets the value of the telefoneComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefoneComprador() {
        return telefoneComprador;
    }

    /**
     * Sets the value of the telefoneComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefoneComprador(String value) {
        this.telefoneComprador = value;
    }

    /**
     * Gets the value of the dddCelularComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddCelularComprador() {
        return dddCelularComprador;
    }

    /**
     * Sets the value of the dddCelularComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddCelularComprador(String value) {
        this.dddCelularComprador = value;
    }

    /**
     * Gets the value of the celularComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelularComprador() {
        return celularComprador;
    }

    /**
     * Sets the value of the celularComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelularComprador(String value) {
        this.celularComprador = value;
    }

    /**
     * Gets the value of the emailComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailComprador() {
        return emailComprador;
    }

    /**
     * Sets the value of the emailComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailComprador(String value) {
        this.emailComprador = value;
    }

    /**
     * Gets the value of the senhaComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenhaComprador() {
        return senhaComprador;
    }

    /**
     * Sets the value of the senhaComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenhaComprador(String value) {
        this.senhaComprador = value;
    }

    /**
     * Gets the value of the nomeEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeEntrega() {
        return nomeEntrega;
    }

    /**
     * Sets the value of the nomeEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeEntrega(String value) {
        this.nomeEntrega = value;
    }

    /**
     * Gets the value of the ruaEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuaEntrega() {
        return ruaEntrega;
    }

    /**
     * Sets the value of the ruaEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuaEntrega(String value) {
        this.ruaEntrega = value;
    }

    /**
     * Gets the value of the numeroEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroEntrega() {
        return numeroEntrega;
    }

    /**
     * Sets the value of the numeroEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroEntrega(String value) {
        this.numeroEntrega = value;
    }

    /**
     * Gets the value of the complementoEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplementoEntrega() {
        return complementoEntrega;
    }

    /**
     * Sets the value of the complementoEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplementoEntrega(String value) {
        this.complementoEntrega = value;
    }

    /**
     * Gets the value of the bairroEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBairroEntrega() {
        return bairroEntrega;
    }

    /**
     * Sets the value of the bairroEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBairroEntrega(String value) {
        this.bairroEntrega = value;
    }

    /**
     * Gets the value of the cidadeEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidadeEntrega() {
        return cidadeEntrega;
    }

    /**
     * Sets the value of the cidadeEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidadeEntrega(String value) {
        this.cidadeEntrega = value;
    }

    /**
     * Gets the value of the ufEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfEntrega() {
        return ufEntrega;
    }

    /**
     * Sets the value of the ufEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfEntrega(String value) {
        this.ufEntrega = value;
    }

    /**
     * Gets the value of the paisEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaisEntrega() {
        return paisEntrega;
    }

    /**
     * Sets the value of the paisEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaisEntrega(String value) {
        this.paisEntrega = value;
    }

    /**
     * Gets the value of the cepEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCepEntrega() {
        return cepEntrega;
    }

    /**
     * Sets the value of the cepEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCepEntrega(String value) {
        this.cepEntrega = value;
    }

    /**
     * Gets the value of the dddEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddEntrega() {
        return dddEntrega;
    }

    /**
     * Sets the value of the dddEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddEntrega(String value) {
        this.dddEntrega = value;
    }

    /**
     * Gets the value of the telefoneEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefoneEntrega() {
        return telefoneEntrega;
    }

    /**
     * Sets the value of the telefoneEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefoneEntrega(String value) {
        this.telefoneEntrega = value;
    }

    /**
     * Gets the value of the dddCelularEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddCelularEntrega() {
        return dddCelularEntrega;
    }

    /**
     * Sets the value of the dddCelularEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddCelularEntrega(String value) {
        this.dddCelularEntrega = value;
    }

    /**
     * Gets the value of the celularEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelularEntrega() {
        return celularEntrega;
    }

    /**
     * Sets the value of the celularEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelularEntrega(String value) {
        this.celularEntrega = value;
    }

    /**
     * Gets the value of the ip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIp() {
        return ip;
    }

    /**
     * Sets the value of the ip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIp(String value) {
        this.ip = value;
    }

    /**
     * Gets the value of the quantidadeItensDistintos property.
     * 
     */
    public int getQuantidadeItensDistintos() {
        return quantidadeItensDistintos;
    }

    /**
     * Sets the value of the quantidadeItensDistintos property.
     * 
     */
    public void setQuantidadeItensDistintos(int value) {
        this.quantidadeItensDistintos = value;
    }

    /**
     * Gets the value of the quantidadeTotalItens property.
     * 
     */
    public int getQuantidadeTotalItens() {
        return quantidadeTotalItens;
    }

    /**
     * Sets the value of the quantidadeTotalItens property.
     * 
     */
    public void setQuantidadeTotalItens(int value) {
        this.quantidadeTotalItens = value;
    }

    /**
     * Gets the value of the valorTotalCompra property.
     * 
     */
    public int getValorTotalCompra() {
        return valorTotalCompra;
    }

    /**
     * Sets the value of the valorTotalCompra property.
     * 
     */
    public void setValorTotalCompra(int value) {
        this.valorTotalCompra = value;
    }

    /**
     * Gets the value of the dataCompra property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataCompra() {
        return dataCompra;
    }

    /**
     * Sets the value of the dataCompra property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataCompra(String value) {
        this.dataCompra = value;
    }

    /**
     * Gets the value of the metodoPagamento property.
     * 
     */
    public int getMetodoPagamento() {
        return metodoPagamento;
    }

    /**
     * Sets the value of the metodoPagamento property.
     * 
     */
    public void setMetodoPagamento(int value) {
        this.metodoPagamento = value;
    }

    /**
     * Gets the value of the numeroParcelas property.
     * 
     */
    public int getNumeroParcelas() {
        return numeroParcelas;
    }

    /**
     * Sets the value of the numeroParcelas property.
     * 
     */
    public void setNumeroParcelas(int value) {
        this.numeroParcelas = value;
    }

    /**
     * Gets the value of the nsu property.
     * 
     */
    public long getNsu() {
        return nsu;
    }

    /**
     * Sets the value of the nsu property.
     * 
     */
    public void setNsu(long value) {
        this.nsu = value;
    }

    /**
     * Gets the value of the valorTotalFrete property.
     * 
     */
    public int getValorTotalFrete() {
        return valorTotalFrete;
    }

    /**
     * Sets the value of the valorTotalFrete property.
     * 
     */
    public void setValorTotalFrete(int value) {
        this.valorTotalFrete = value;
    }

    /**
     * Gets the value of the paraPresente property.
     * 
     */
    public boolean isParaPresente() {
        return paraPresente;
    }

    /**
     * Sets the value of the paraPresente property.
     * 
     */
    public void setParaPresente(boolean value) {
        this.paraPresente = value;
    }

    /**
     * Gets the value of the prazoEntregaDias property.
     * 
     */
    public int getPrazoEntregaDias() {
        return prazoEntregaDias;
    }

    /**
     * Sets the value of the prazoEntregaDias property.
     * 
     */
    public void setPrazoEntregaDias(int value) {
        this.prazoEntregaDias = value;
    }

    /**
     * Gets the value of the formaEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaEntrega() {
        return formaEntrega;
    }

    /**
     * Sets the value of the formaEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaEntrega(String value) {
        this.formaEntrega = value;
    }

    /**
     * Gets the value of the nomeBancoEmissor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeBancoEmissor() {
        return nomeBancoEmissor;
    }

    /**
     * Sets the value of the nomeBancoEmissor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeBancoEmissor(String value) {
        this.nomeBancoEmissor = value;
    }

    /**
     * Gets the value of the numeroCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCartao() {
        return numeroCartao;
    }

    /**
     * Sets the value of the numeroCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCartao(String value) {
        this.numeroCartao = value;
    }

    /**
     * Gets the value of the nomeTitularCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeTitularCartao() {
        return nomeTitularCartao;
    }

    /**
     * Sets the value of the nomeTitularCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeTitularCartao(String value) {
        this.nomeTitularCartao = value;
    }

    /**
     * Gets the value of the dataValidadeCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataValidadeCartao() {
        return dataValidadeCartao;
    }

    /**
     * Sets the value of the dataValidadeCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataValidadeCartao(String value) {
        this.dataValidadeCartao = value;
    }

    /**
     * Gets the value of the observacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Sets the value of the observacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Gets the value of the canalVenda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanalVenda() {
        return canalVenda;
    }

    /**
     * Sets the value of the canalVenda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanalVenda(String value) {
        this.canalVenda = value;
    }

    /**
     * Gets the value of the codigosProdutos property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigosProdutos() {
        return codigosProdutos;
    }

    /**
     * Sets the value of the codigosProdutos property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigosProdutos(String value) {
        this.codigosProdutos = value;
    }

    /**
     * Gets the value of the descricoesProdutos property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricoesProdutos() {
        return descricoesProdutos;
    }

    /**
     * Sets the value of the descricoesProdutos property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricoesProdutos(String value) {
        this.descricoesProdutos = value;
    }

    /**
     * Gets the value of the quantidadesProdutos property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantidadesProdutos() {
        return quantidadesProdutos;
    }

    /**
     * Sets the value of the quantidadesProdutos property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantidadesProdutos(String value) {
        this.quantidadesProdutos = value;
    }

    /**
     * Gets the value of the valoresProdutos property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValoresProdutos() {
        return valoresProdutos;
    }

    /**
     * Sets the value of the valoresProdutos property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValoresProdutos(String value) {
        this.valoresProdutos = value;
    }

    /**
     * Gets the value of the categoriasProdutos property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoriasProdutos() {
        return categoriasProdutos;
    }

    /**
     * Sets the value of the categoriasProdutos property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoriasProdutos(String value) {
        this.categoriasProdutos = value;
    }

    /**
     * Gets the value of the pedidoTeste property.
     * 
     */
    public boolean isPedidoTeste() {
        return pedidoTeste;
    }

    /**
     * Sets the value of the pedidoTeste property.
     * 
     */
    public void setPedidoTeste(boolean value) {
        this.pedidoTeste = value;
    }

    /**
     * Gets the value of the mensagemRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagemRetorno() {
        return mensagemRetorno;
    }

    /**
     * Sets the value of the mensagemRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagemRetorno(String value) {
        this.mensagemRetorno = value;
    }

    /**
     * Gets the value of the telefoneextraddd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefoneextraddd() {
        return telefoneextraddd;
    }

    /**
     * Sets the value of the telefoneextraddd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefoneextraddd(String value) {
        this.telefoneextraddd = value;
    }

    /**
     * Gets the value of the telefoneextra property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefoneextra() {
        return telefoneextra;
    }

    /**
     * Sets the value of the telefoneextra property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefoneextra(String value) {
        this.telefoneextra = value;
    }

    /**
     * Gets the value of the bin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBin() {
        return bin;
    }

    /**
     * Sets the value of the bin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBin(String value) {
        this.bin = value;
    }

    /**
     * Gets the value of the quatroultimosdigitos property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuatroultimosdigitos() {
        return quatroultimosdigitos;
    }

    /**
     * Sets the value of the quatroultimosdigitos property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuatroultimosdigitos(String value) {
        this.quatroultimosdigitos = value;
    }

    /**
     * Gets the value of the cpftitularcartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpftitularcartao() {
        return cpftitularcartao;
    }

    /**
     * Sets the value of the cpftitularcartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpftitularcartao(String value) {
        this.cpftitularcartao = value;
    }

    /**
     * Gets the value of the identificadorLojaFilho property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificadorLojaFilho() {
        return identificadorLojaFilho;
    }

    /**
     * Sets the value of the identificadorLojaFilho property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificadorLojaFilho(String value) {
        this.identificadorLojaFilho = value;
    }

    /**
     * Gets the value of the dataNascimentoComprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataNascimentoComprador() {
        return dataNascimentoComprador;
    }

    /**
     * Sets the value of the dataNascimentoComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataNascimentoComprador(String value) {
        this.dataNascimentoComprador = value;
    }

    /**
     * Gets the value of the telefoneEntrega2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefoneEntrega2() {
        return telefoneEntrega2;
    }

    /**
     * Sets the value of the telefoneEntrega2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefoneEntrega2(String value) {
        this.telefoneEntrega2 = value;
    }

    /**
     * Gets the value of the dddEntrega2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddEntrega2() {
        return dddEntrega2;
    }

    /**
     * Sets the value of the dddEntrega2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddEntrega2(String value) {
        this.dddEntrega2 = value;
    }

    /**
     * Gets the value of the dataEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataEntrega() {
        return dataEntrega;
    }

    /**
     * Sets the value of the dataEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataEntrega(String value) {
        this.dataEntrega = value;
    }

}
