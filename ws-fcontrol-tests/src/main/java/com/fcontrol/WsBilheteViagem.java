
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsBilheteViagem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsBilheteViagem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ddd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Telefone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SomenteIda" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Origem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Destino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataPartida" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataRetorno" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Poltrona" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Empresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tipo" type="{http://tempuri.org/}Classe"/>
 *         &lt;element name="Valor" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsBilheteViagem", propOrder = {
    "nome",
    "ddd",
    "telefone",
    "somenteIda",
    "origem",
    "destino",
    "dataPartida",
    "dataRetorno",
    "email",
    "poltrona",
    "empresa",
    "tipo",
    "valor"
})
public class WsBilheteViagem {

    @XmlElement(name = "Nome")
    protected String nome;
    @XmlElement(name = "Ddd")
    protected String ddd;
    @XmlElement(name = "Telefone")
    protected String telefone;
    @XmlElement(name = "SomenteIda")
    protected boolean somenteIda;
    @XmlElement(name = "Origem")
    protected String origem;
    @XmlElement(name = "Destino")
    protected String destino;
    @XmlElement(name = "DataPartida", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataPartida;
    @XmlElement(name = "DataRetorno", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataRetorno;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "Poltrona")
    protected String poltrona;
    @XmlElement(name = "Empresa")
    protected String empresa;
    @XmlElement(name = "Tipo", required = true)
    @XmlSchemaType(name = "string")
    protected Classe tipo;
    @XmlElement(name = "Valor")
    protected int valor;

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the ddd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDdd() {
        return ddd;
    }

    /**
     * Sets the value of the ddd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDdd(String value) {
        this.ddd = value;
    }

    /**
     * Gets the value of the telefone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * Sets the value of the telefone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefone(String value) {
        this.telefone = value;
    }

    /**
     * Gets the value of the somenteIda property.
     * 
     */
    public boolean isSomenteIda() {
        return somenteIda;
    }

    /**
     * Sets the value of the somenteIda property.
     * 
     */
    public void setSomenteIda(boolean value) {
        this.somenteIda = value;
    }

    /**
     * Gets the value of the origem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigem() {
        return origem;
    }

    /**
     * Sets the value of the origem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigem(String value) {
        this.origem = value;
    }

    /**
     * Gets the value of the destino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestino() {
        return destino;
    }

    /**
     * Sets the value of the destino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestino(String value) {
        this.destino = value;
    }

    /**
     * Gets the value of the dataPartida property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataPartida() {
        return dataPartida;
    }

    /**
     * Sets the value of the dataPartida property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataPartida(XMLGregorianCalendar value) {
        this.dataPartida = value;
    }

    /**
     * Gets the value of the dataRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRetorno() {
        return dataRetorno;
    }

    /**
     * Sets the value of the dataRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRetorno(XMLGregorianCalendar value) {
        this.dataRetorno = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the poltrona property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoltrona() {
        return poltrona;
    }

    /**
     * Sets the value of the poltrona property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoltrona(String value) {
        this.poltrona = value;
    }

    /**
     * Gets the value of the empresa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Sets the value of the empresa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpresa(String value) {
        this.empresa = value;
    }

    /**
     * Gets the value of the tipo property.
     * 
     * @return
     *     possible object is
     *     {@link Classe }
     *     
     */
    public Classe getTipo() {
        return tipo;
    }

    /**
     * Sets the value of the tipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Classe }
     *     
     */
    public void setTipo(Classe value) {
        this.tipo = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     */
    public int getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     */
    public void setValor(int value) {
        this.valor = value;
    }

}
