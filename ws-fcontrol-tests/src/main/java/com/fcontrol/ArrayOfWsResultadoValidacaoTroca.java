
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsResultadoValidacaoTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsResultadoValidacaoTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsResultadoValidacaoTroca" type="{http://tempuri.org/}WsResultadoValidacaoTroca" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsResultadoValidacaoTroca", propOrder = {
    "wsResultadoValidacaoTroca"
})
public class ArrayOfWsResultadoValidacaoTroca {

    @XmlElement(name = "WsResultadoValidacaoTroca", nillable = true)
    protected List<WsResultadoValidacaoTroca> wsResultadoValidacaoTroca;

    /**
     * Gets the value of the wsResultadoValidacaoTroca property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsResultadoValidacaoTroca property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsResultadoValidacaoTroca().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsResultadoValidacaoTroca }
     * 
     * 
     */
    public List<WsResultadoValidacaoTroca> getWsResultadoValidacaoTroca() {
        if (wsResultadoValidacaoTroca == null) {
            wsResultadoValidacaoTroca = new ArrayList<WsResultadoValidacaoTroca>();
        }
        return this.wsResultadoValidacaoTroca;
    }

}
