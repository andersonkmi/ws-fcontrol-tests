
package com.fcontrol;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Classe.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Classe">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Nulo"/>
 *     &lt;enumeration value="Convencional"/>
 *     &lt;enumeration value="ConvencionalComAr"/>
 *     &lt;enumeration value="Executivo"/>
 *     &lt;enumeration value="ExecutivoSemLeito"/>
 *     &lt;enumeration value="SemiLeito"/>
 *     &lt;enumeration value="Leito"/>
 *     &lt;enumeration value="UrbanoComAr"/>
 *     &lt;enumeration value="UrbanoSemAr"/>
 *     &lt;enumeration value="DoubleLeito"/>
 *     &lt;enumeration value="DoublePlus"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Classe")
@XmlEnum
public enum Classe {

    @XmlEnumValue("Nulo")
    NULO("Nulo"),
    @XmlEnumValue("Convencional")
    CONVENCIONAL("Convencional"),
    @XmlEnumValue("ConvencionalComAr")
    CONVENCIONAL_COM_AR("ConvencionalComAr"),
    @XmlEnumValue("Executivo")
    EXECUTIVO("Executivo"),
    @XmlEnumValue("ExecutivoSemLeito")
    EXECUTIVO_SEM_LEITO("ExecutivoSemLeito"),
    @XmlEnumValue("SemiLeito")
    SEMI_LEITO("SemiLeito"),
    @XmlEnumValue("Leito")
    LEITO("Leito"),
    @XmlEnumValue("UrbanoComAr")
    URBANO_COM_AR("UrbanoComAr"),
    @XmlEnumValue("UrbanoSemAr")
    URBANO_SEM_AR("UrbanoSemAr"),
    @XmlEnumValue("DoubleLeito")
    DOUBLE_LEITO("DoubleLeito"),
    @XmlEnumValue("DoublePlus")
    DOUBLE_PLUS("DoublePlus");
    private final String value;

    Classe(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Classe fromValue(String v) {
        for (Classe c: Classe.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
