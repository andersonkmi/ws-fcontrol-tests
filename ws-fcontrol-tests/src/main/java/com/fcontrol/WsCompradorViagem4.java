
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsCompradorViagem4 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCompradorViagem4">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsCompradorViagem3">
 *       &lt;sequence>
 *         &lt;element name="NumeroFidelidade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCompradorViagem4", propOrder = {
    "numeroFidelidade"
})
public class WsCompradorViagem4
    extends WsCompradorViagem3
{

    @XmlElement(name = "NumeroFidelidade")
    protected String numeroFidelidade;

    /**
     * Gets the value of the numeroFidelidade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroFidelidade() {
        return numeroFidelidade;
    }

    /**
     * Sets the value of the numeroFidelidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroFidelidade(String value) {
        this.numeroFidelidade = value;
    }

}
