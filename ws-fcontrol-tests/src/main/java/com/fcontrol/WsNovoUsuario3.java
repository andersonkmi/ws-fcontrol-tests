
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsNovoUsuario3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsNovoUsuario3">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsNovoUsuario2">
 *       &lt;sequence>
 *         &lt;element name="CodigoUnidadeAnalise" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsNovoUsuario3", propOrder = {
    "codigoUnidadeAnalise"
})
@XmlSeeAlso({
    WsNovoUsuario4 .class
})
public class WsNovoUsuario3
    extends WsNovoUsuario2
{

    @XmlElement(name = "CodigoUnidadeAnalise")
    protected String codigoUnidadeAnalise;

    /**
     * Gets the value of the codigoUnidadeAnalise property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoUnidadeAnalise() {
        return codigoUnidadeAnalise;
    }

    /**
     * Sets the value of the codigoUnidadeAnalise property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoUnidadeAnalise(String value) {
        this.codigoUnidadeAnalise = value;
    }

}
