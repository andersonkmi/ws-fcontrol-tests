
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pedido" type="{http://tempuri.org/}WsTransacaoTelecom3" minOccurs="0"/>
 *         &lt;element name="comHistorico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pedido",
    "comHistorico"
})
@XmlRootElement(name = "analisarTransacaoTelecom4")
public class AnalisarTransacaoTelecom4 {

    protected WsTransacaoTelecom3 pedido;
    protected boolean comHistorico;

    /**
     * Gets the value of the pedido property.
     * 
     * @return
     *     possible object is
     *     {@link WsTransacaoTelecom3 }
     *     
     */
    public WsTransacaoTelecom3 getPedido() {
        return pedido;
    }

    /**
     * Sets the value of the pedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsTransacaoTelecom3 }
     *     
     */
    public void setPedido(WsTransacaoTelecom3 value) {
        this.pedido = value;
    }

    /**
     * Gets the value of the comHistorico property.
     * 
     */
    public boolean isComHistorico() {
        return comHistorico;
    }

    /**
     * Sets the value of the comHistorico property.
     * 
     */
    public void setComHistorico(boolean value) {
        this.comHistorico = value;
    }

}
