
package com.fcontrol;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsOpiniaoRisco.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WsOpiniaoRisco">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Baixo"/>
 *     &lt;enumeration value="Medio"/>
 *     &lt;enumeration value="Alto"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "WsOpiniaoRisco")
@XmlEnum
public enum WsOpiniaoRisco {

    @XmlEnumValue("Baixo")
    BAIXO("Baixo"),
    @XmlEnumValue("Medio")
    MEDIO("Medio"),
    @XmlEnumValue("Alto")
    ALTO("Alto");
    private final String value;

    WsOpiniaoRisco(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WsOpiniaoRisco fromValue(String v) {
        for (WsOpiniaoRisco c: WsOpiniaoRisco.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
