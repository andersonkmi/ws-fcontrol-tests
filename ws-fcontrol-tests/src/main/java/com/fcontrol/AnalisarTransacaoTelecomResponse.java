
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="analisarTransacaoTelecomResult" type="{http://tempuri.org/}WsResultadoAnalise" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarTransacaoTelecomResult"
})
@XmlRootElement(name = "analisarTransacaoTelecomResponse")
public class AnalisarTransacaoTelecomResponse {

    protected WsResultadoAnalise analisarTransacaoTelecomResult;

    /**
     * Gets the value of the analisarTransacaoTelecomResult property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultadoAnalise }
     *     
     */
    public WsResultadoAnalise getAnalisarTransacaoTelecomResult() {
        return analisarTransacaoTelecomResult;
    }

    /**
     * Sets the value of the analisarTransacaoTelecomResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultadoAnalise }
     *     
     */
    public void setAnalisarTransacaoTelecomResult(WsResultadoAnalise value) {
        this.analisarTransacaoTelecomResult = value;
    }

}
