
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsHistorico2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsHistorico2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Comprador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnderecoEntrega" type="{http://tempuri.org/}WsEndereco" minOccurs="0"/>
 *         &lt;element name="CodigoPedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataPedido" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="StatusPedido" type="{http://tempuri.org/}Status"/>
 *         &lt;element name="Score" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Total" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Risco" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Empresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Comentario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsHistorico2", propOrder = {
    "comprador",
    "enderecoEntrega",
    "codigoPedido",
    "dataPedido",
    "statusPedido",
    "score",
    "total",
    "risco",
    "empresa",
    "comentario"
})
public class WsHistorico2 {

    @XmlElement(name = "Comprador")
    protected String comprador;
    @XmlElement(name = "EnderecoEntrega")
    protected WsEndereco enderecoEntrega;
    @XmlElement(name = "CodigoPedido")
    protected String codigoPedido;
    @XmlElement(name = "DataPedido", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataPedido;
    @XmlElement(name = "StatusPedido", required = true)
    @XmlSchemaType(name = "string")
    protected Status statusPedido;
    @XmlElement(name = "Score")
    protected int score;
    @XmlElement(name = "Total")
    protected int total;
    @XmlElement(name = "Risco")
    protected int risco;
    @XmlElement(name = "Empresa")
    protected String empresa;
    @XmlElement(name = "Comentario")
    protected String comentario;

    /**
     * Gets the value of the comprador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComprador() {
        return comprador;
    }

    /**
     * Sets the value of the comprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComprador(String value) {
        this.comprador = value;
    }

    /**
     * Gets the value of the enderecoEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link WsEndereco }
     *     
     */
    public WsEndereco getEnderecoEntrega() {
        return enderecoEntrega;
    }

    /**
     * Sets the value of the enderecoEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsEndereco }
     *     
     */
    public void setEnderecoEntrega(WsEndereco value) {
        this.enderecoEntrega = value;
    }

    /**
     * Gets the value of the codigoPedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPedido() {
        return codigoPedido;
    }

    /**
     * Sets the value of the codigoPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPedido(String value) {
        this.codigoPedido = value;
    }

    /**
     * Gets the value of the dataPedido property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataPedido() {
        return dataPedido;
    }

    /**
     * Sets the value of the dataPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataPedido(XMLGregorianCalendar value) {
        this.dataPedido = value;
    }

    /**
     * Gets the value of the statusPedido property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatusPedido() {
        return statusPedido;
    }

    /**
     * Sets the value of the statusPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatusPedido(Status value) {
        this.statusPedido = value;
    }

    /**
     * Gets the value of the score property.
     * 
     */
    public int getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     */
    public void setScore(int value) {
        this.score = value;
    }

    /**
     * Gets the value of the total property.
     * 
     */
    public int getTotal() {
        return total;
    }

    /**
     * Sets the value of the total property.
     * 
     */
    public void setTotal(int value) {
        this.total = value;
    }

    /**
     * Gets the value of the risco property.
     * 
     */
    public int getRisco() {
        return risco;
    }

    /**
     * Sets the value of the risco property.
     * 
     */
    public void setRisco(int value) {
        this.risco = value;
    }

    /**
     * Gets the value of the empresa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Sets the value of the empresa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpresa(String value) {
        this.empresa = value;
    }

    /**
     * Gets the value of the comentario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * Sets the value of the comentario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComentario(String value) {
        this.comentario = value;
    }

}
