
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsAnaliseTodasSublojas3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAnaliseTodasSublojas3">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsAnaliseTodasSublojas2">
 *       &lt;sequence>
 *         &lt;element name="Fila" type="{http://tempuri.org/}WsFila" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAnaliseTodasSublojas3", propOrder = {
    "fila"
})
public class WsAnaliseTodasSublojas3
    extends WsAnaliseTodasSublojas2
{

    @XmlElement(name = "Fila")
    protected WsFila fila;

    /**
     * Gets the value of the fila property.
     * 
     * @return
     *     possible object is
     *     {@link WsFila }
     *     
     */
    public WsFila getFila() {
        return fila;
    }

    /**
     * Sets the value of the fila property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsFila }
     *     
     */
    public void setFila(WsFila value) {
        this.fila = value;
    }

}
