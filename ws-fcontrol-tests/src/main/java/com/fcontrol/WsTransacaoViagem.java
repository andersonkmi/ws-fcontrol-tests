
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsTransacaoViagem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsTransacaoViagem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoPedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataCompra" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CanalVenda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DadosUsuario" type="{http://tempuri.org/}WsUsuarioViagem" minOccurs="0"/>
 *         &lt;element name="DadosComprador" type="{http://tempuri.org/}WsCompradorViagem" minOccurs="0"/>
 *         &lt;element name="Pagamentos" type="{http://tempuri.org/}ArrayOfWsPagamentoViagem" minOccurs="0"/>
 *         &lt;element name="Bilhetes" type="{http://tempuri.org/}ArrayOfWsBilheteViagem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsTransacaoViagem", propOrder = {
    "codigoPedido",
    "dataCompra",
    "canalVenda",
    "observacao",
    "dadosUsuario",
    "dadosComprador",
    "pagamentos",
    "bilhetes"
})
@XmlSeeAlso({
    WsTransacaoViagem2 .class
})
public class WsTransacaoViagem {

    @XmlElement(name = "CodigoPedido")
    protected String codigoPedido;
    @XmlElement(name = "DataCompra", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCompra;
    @XmlElement(name = "CanalVenda")
    protected String canalVenda;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(name = "DadosUsuario")
    protected WsUsuarioViagem dadosUsuario;
    @XmlElement(name = "DadosComprador")
    protected WsCompradorViagem dadosComprador;
    @XmlElement(name = "Pagamentos")
    protected ArrayOfWsPagamentoViagem pagamentos;
    @XmlElement(name = "Bilhetes")
    protected ArrayOfWsBilheteViagem bilhetes;

    /**
     * Gets the value of the codigoPedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPedido() {
        return codigoPedido;
    }

    /**
     * Sets the value of the codigoPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPedido(String value) {
        this.codigoPedido = value;
    }

    /**
     * Gets the value of the dataCompra property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCompra() {
        return dataCompra;
    }

    /**
     * Sets the value of the dataCompra property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCompra(XMLGregorianCalendar value) {
        this.dataCompra = value;
    }

    /**
     * Gets the value of the canalVenda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanalVenda() {
        return canalVenda;
    }

    /**
     * Sets the value of the canalVenda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanalVenda(String value) {
        this.canalVenda = value;
    }

    /**
     * Gets the value of the observacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Sets the value of the observacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Gets the value of the dadosUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link WsUsuarioViagem }
     *     
     */
    public WsUsuarioViagem getDadosUsuario() {
        return dadosUsuario;
    }

    /**
     * Sets the value of the dadosUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsUsuarioViagem }
     *     
     */
    public void setDadosUsuario(WsUsuarioViagem value) {
        this.dadosUsuario = value;
    }

    /**
     * Gets the value of the dadosComprador property.
     * 
     * @return
     *     possible object is
     *     {@link WsCompradorViagem }
     *     
     */
    public WsCompradorViagem getDadosComprador() {
        return dadosComprador;
    }

    /**
     * Sets the value of the dadosComprador property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsCompradorViagem }
     *     
     */
    public void setDadosComprador(WsCompradorViagem value) {
        this.dadosComprador = value;
    }

    /**
     * Gets the value of the pagamentos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsPagamentoViagem }
     *     
     */
    public ArrayOfWsPagamentoViagem getPagamentos() {
        return pagamentos;
    }

    /**
     * Sets the value of the pagamentos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsPagamentoViagem }
     *     
     */
    public void setPagamentos(ArrayOfWsPagamentoViagem value) {
        this.pagamentos = value;
    }

    /**
     * Gets the value of the bilhetes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsBilheteViagem }
     *     
     */
    public ArrayOfWsBilheteViagem getBilhetes() {
        return bilhetes;
    }

    /**
     * Sets the value of the bilhetes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsBilheteViagem }
     *     
     */
    public void setBilhetes(ArrayOfWsBilheteViagem value) {
        this.bilhetes = value;
    }

}
