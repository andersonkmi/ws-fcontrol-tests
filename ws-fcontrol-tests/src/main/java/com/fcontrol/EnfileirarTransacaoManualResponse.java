
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enfileirarTransacaoManualResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="mensagemRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enfileirarTransacaoManualResult",
    "mensagemRetorno"
})
@XmlRootElement(name = "enfileirarTransacaoManualResponse")
public class EnfileirarTransacaoManualResponse {

    protected boolean enfileirarTransacaoManualResult;
    protected String mensagemRetorno;

    /**
     * Gets the value of the enfileirarTransacaoManualResult property.
     * 
     */
    public boolean isEnfileirarTransacaoManualResult() {
        return enfileirarTransacaoManualResult;
    }

    /**
     * Sets the value of the enfileirarTransacaoManualResult property.
     * 
     */
    public void setEnfileirarTransacaoManualResult(boolean value) {
        this.enfileirarTransacaoManualResult = value;
    }

    /**
     * Gets the value of the mensagemRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagemRetorno() {
        return mensagemRetorno;
    }

    /**
     * Sets the value of the mensagemRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagemRetorno(String value) {
        this.mensagemRetorno = value;
    }

}
