
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsPagamento2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsPagamento2">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}WsPagamento">
 *       &lt;sequence>
 *         &lt;element name="Nsu" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsPagamento2", propOrder = {
    "nsu"
})
public class WsPagamento2
    extends WsPagamento
{

    @XmlElement(name = "Nsu", required = true, type = Long.class, nillable = true)
    protected Long nsu;

    /**
     * Gets the value of the nsu property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNsu() {
        return nsu;
    }

    /**
     * Sets the value of the nsu property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNsu(Long value) {
        this.nsu = value;
    }

}
