
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enfileirarTransacaoViagem3Result" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enfileirarTransacaoViagem3Result"
})
@XmlRootElement(name = "enfileirarTransacaoViagem3Response")
public class EnfileirarTransacaoViagem3Response {

    protected WsResultado enfileirarTransacaoViagem3Result;

    /**
     * Gets the value of the enfileirarTransacaoViagem3Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getEnfileirarTransacaoViagem3Result() {
        return enfileirarTransacaoViagem3Result;
    }

    /**
     * Sets the value of the enfileirarTransacaoViagem3Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setEnfileirarTransacaoViagem3Result(WsResultado value) {
        this.enfileirarTransacaoViagem3Result = value;
    }

}
