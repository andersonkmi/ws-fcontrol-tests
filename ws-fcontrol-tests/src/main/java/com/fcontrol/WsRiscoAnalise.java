
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsRiscoAnalise complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsRiscoAnalise">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Risco" type="{http://tempuri.org/}Opiniao"/>
 *         &lt;element name="Score" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Regras" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="HistoricoLoja" type="{http://tempuri.org/}ArrayOfWsHistorico" minOccurs="0"/>
 *         &lt;element name="HistoricoOutrasLoja" type="{http://tempuri.org/}ArrayOfWsHistorico" minOccurs="0"/>
 *         &lt;element name="HistoricoEntrega" type="{http://tempuri.org/}ArrayOfWsHistorico" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsRiscoAnalise", propOrder = {
    "risco",
    "score",
    "regras",
    "historicoLoja",
    "historicoOutrasLoja",
    "historicoEntrega"
})
public class WsRiscoAnalise {

    @XmlElement(name = "Risco", required = true)
    @XmlSchemaType(name = "string")
    protected Opiniao risco;
    @XmlElement(name = "Score")
    protected int score;
    @XmlElement(name = "Regras")
    protected ArrayOfString regras;
    @XmlElement(name = "HistoricoLoja")
    protected ArrayOfWsHistorico historicoLoja;
    @XmlElement(name = "HistoricoOutrasLoja")
    protected ArrayOfWsHistorico historicoOutrasLoja;
    @XmlElement(name = "HistoricoEntrega")
    protected ArrayOfWsHistorico historicoEntrega;

    /**
     * Gets the value of the risco property.
     * 
     * @return
     *     possible object is
     *     {@link Opiniao }
     *     
     */
    public Opiniao getRisco() {
        return risco;
    }

    /**
     * Sets the value of the risco property.
     * 
     * @param value
     *     allowed object is
     *     {@link Opiniao }
     *     
     */
    public void setRisco(Opiniao value) {
        this.risco = value;
    }

    /**
     * Gets the value of the score property.
     * 
     */
    public int getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     */
    public void setScore(int value) {
        this.score = value;
    }

    /**
     * Gets the value of the regras property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getRegras() {
        return regras;
    }

    /**
     * Sets the value of the regras property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setRegras(ArrayOfString value) {
        this.regras = value;
    }

    /**
     * Gets the value of the historicoLoja property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsHistorico }
     *     
     */
    public ArrayOfWsHistorico getHistoricoLoja() {
        return historicoLoja;
    }

    /**
     * Sets the value of the historicoLoja property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsHistorico }
     *     
     */
    public void setHistoricoLoja(ArrayOfWsHistorico value) {
        this.historicoLoja = value;
    }

    /**
     * Gets the value of the historicoOutrasLoja property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsHistorico }
     *     
     */
    public ArrayOfWsHistorico getHistoricoOutrasLoja() {
        return historicoOutrasLoja;
    }

    /**
     * Sets the value of the historicoOutrasLoja property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsHistorico }
     *     
     */
    public void setHistoricoOutrasLoja(ArrayOfWsHistorico value) {
        this.historicoOutrasLoja = value;
    }

    /**
     * Gets the value of the historicoEntrega property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsHistorico }
     *     
     */
    public ArrayOfWsHistorico getHistoricoEntrega() {
        return historicoEntrega;
    }

    /**
     * Sets the value of the historicoEntrega property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsHistorico }
     *     
     */
    public void setHistoricoEntrega(ArrayOfWsHistorico value) {
        this.historicoEntrega = value;
    }

}
