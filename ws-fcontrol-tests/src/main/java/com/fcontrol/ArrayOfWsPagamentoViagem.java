
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsPagamentoViagem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsPagamentoViagem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsPagamentoViagem" type="{http://tempuri.org/}WsPagamentoViagem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsPagamentoViagem", propOrder = {
    "wsPagamentoViagem"
})
public class ArrayOfWsPagamentoViagem {

    @XmlElement(name = "WsPagamentoViagem", nillable = true)
    protected List<WsPagamentoViagem> wsPagamentoViagem;

    /**
     * Gets the value of the wsPagamentoViagem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsPagamentoViagem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsPagamentoViagem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsPagamentoViagem }
     * 
     * 
     */
    public List<WsPagamentoViagem> getWsPagamentoViagem() {
        if (wsPagamentoViagem == null) {
            wsPagamentoViagem = new ArrayList<WsPagamentoViagem>();
        }
        return this.wsPagamentoViagem;
    }

}
