
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadosGeralSubLoja3Result" type="{http://tempuri.org/}ArrayOfWsAnalise3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadosGeralSubLoja3Result"
})
@XmlRootElement(name = "capturarResultadosGeralSubLoja3Response")
public class CapturarResultadosGeralSubLoja3Response {

    protected ArrayOfWsAnalise3 capturarResultadosGeralSubLoja3Result;

    /**
     * Gets the value of the capturarResultadosGeralSubLoja3Result property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsAnalise3 }
     *     
     */
    public ArrayOfWsAnalise3 getCapturarResultadosGeralSubLoja3Result() {
        return capturarResultadosGeralSubLoja3Result;
    }

    /**
     * Sets the value of the capturarResultadosGeralSubLoja3Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsAnalise3 }
     *     
     */
    public void setCapturarResultadosGeralSubLoja3Result(ArrayOfWsAnalise3 value) {
        this.capturarResultadosGeralSubLoja3Result = value;
    }

}
