
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsTransacaoTroca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsTransacaoTroca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Usuario" type="{http://tempuri.org/}WsUsuarioTroca" minOccurs="0"/>
 *         &lt;element name="NumeroChamado" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="Linha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Manifestacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GrupoManifestacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProdutoAssunto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoManifestacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataAbertura" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataPrevista" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataConclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ExigeColetaDestino" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ResponsavelAbertura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReAnalise" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Pessoa" type="{http://tempuri.org/}WsPessoaTroca" minOccurs="0"/>
 *         &lt;element name="ProdutosReclamados" type="{http://tempuri.org/}ArrayOfWsProdutoTroca" minOccurs="0"/>
 *         &lt;element name="Ressarcimento" type="{http://tempuri.org/}WsRessarcimentoTroca" minOccurs="0"/>
 *         &lt;element name="EmHomologacao" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsTransacaoTroca", propOrder = {
    "usuario",
    "numeroChamado",
    "linha",
    "manifestacao",
    "grupoManifestacao",
    "produtoAssunto",
    "tipoManifestacao",
    "dataAbertura",
    "dataPrevista",
    "dataConclusao",
    "exigeColetaDestino",
    "responsavelAbertura",
    "reAnalise",
    "pessoa",
    "produtosReclamados",
    "ressarcimento",
    "emHomologacao"
})
public class WsTransacaoTroca {

    @XmlElement(name = "Usuario")
    protected WsUsuarioTroca usuario;
    @XmlElement(name = "NumeroChamado")
    protected long numeroChamado;
    @XmlElement(name = "Linha")
    protected String linha;
    @XmlElement(name = "Manifestacao")
    protected String manifestacao;
    @XmlElement(name = "GrupoManifestacao")
    protected String grupoManifestacao;
    @XmlElement(name = "ProdutoAssunto")
    protected String produtoAssunto;
    @XmlElement(name = "TipoManifestacao")
    protected String tipoManifestacao;
    @XmlElement(name = "DataAbertura", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAbertura;
    @XmlElement(name = "DataPrevista", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataPrevista;
    @XmlElement(name = "DataConclusao", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataConclusao;
    @XmlElement(name = "ExigeColetaDestino")
    protected boolean exigeColetaDestino;
    @XmlElement(name = "ResponsavelAbertura")
    protected String responsavelAbertura;
    @XmlElement(name = "ReAnalise")
    protected boolean reAnalise;
    @XmlElement(name = "Pessoa")
    protected WsPessoaTroca pessoa;
    @XmlElement(name = "ProdutosReclamados")
    protected ArrayOfWsProdutoTroca produtosReclamados;
    @XmlElement(name = "Ressarcimento")
    protected WsRessarcimentoTroca ressarcimento;
    @XmlElement(name = "EmHomologacao")
    protected boolean emHomologacao;

    /**
     * Gets the value of the usuario property.
     * 
     * @return
     *     possible object is
     *     {@link WsUsuarioTroca }
     *     
     */
    public WsUsuarioTroca getUsuario() {
        return usuario;
    }

    /**
     * Sets the value of the usuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsUsuarioTroca }
     *     
     */
    public void setUsuario(WsUsuarioTroca value) {
        this.usuario = value;
    }

    /**
     * Gets the value of the numeroChamado property.
     * 
     */
    public long getNumeroChamado() {
        return numeroChamado;
    }

    /**
     * Sets the value of the numeroChamado property.
     * 
     */
    public void setNumeroChamado(long value) {
        this.numeroChamado = value;
    }

    /**
     * Gets the value of the linha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinha() {
        return linha;
    }

    /**
     * Sets the value of the linha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinha(String value) {
        this.linha = value;
    }

    /**
     * Gets the value of the manifestacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManifestacao() {
        return manifestacao;
    }

    /**
     * Sets the value of the manifestacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManifestacao(String value) {
        this.manifestacao = value;
    }

    /**
     * Gets the value of the grupoManifestacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupoManifestacao() {
        return grupoManifestacao;
    }

    /**
     * Sets the value of the grupoManifestacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupoManifestacao(String value) {
        this.grupoManifestacao = value;
    }

    /**
     * Gets the value of the produtoAssunto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdutoAssunto() {
        return produtoAssunto;
    }

    /**
     * Sets the value of the produtoAssunto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdutoAssunto(String value) {
        this.produtoAssunto = value;
    }

    /**
     * Gets the value of the tipoManifestacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoManifestacao() {
        return tipoManifestacao;
    }

    /**
     * Sets the value of the tipoManifestacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoManifestacao(String value) {
        this.tipoManifestacao = value;
    }

    /**
     * Gets the value of the dataAbertura property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAbertura() {
        return dataAbertura;
    }

    /**
     * Sets the value of the dataAbertura property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAbertura(XMLGregorianCalendar value) {
        this.dataAbertura = value;
    }

    /**
     * Gets the value of the dataPrevista property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataPrevista() {
        return dataPrevista;
    }

    /**
     * Sets the value of the dataPrevista property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataPrevista(XMLGregorianCalendar value) {
        this.dataPrevista = value;
    }

    /**
     * Gets the value of the dataConclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataConclusao() {
        return dataConclusao;
    }

    /**
     * Sets the value of the dataConclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataConclusao(XMLGregorianCalendar value) {
        this.dataConclusao = value;
    }

    /**
     * Gets the value of the exigeColetaDestino property.
     * 
     */
    public boolean isExigeColetaDestino() {
        return exigeColetaDestino;
    }

    /**
     * Sets the value of the exigeColetaDestino property.
     * 
     */
    public void setExigeColetaDestino(boolean value) {
        this.exigeColetaDestino = value;
    }

    /**
     * Gets the value of the responsavelAbertura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponsavelAbertura() {
        return responsavelAbertura;
    }

    /**
     * Sets the value of the responsavelAbertura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponsavelAbertura(String value) {
        this.responsavelAbertura = value;
    }

    /**
     * Gets the value of the reAnalise property.
     * 
     */
    public boolean isReAnalise() {
        return reAnalise;
    }

    /**
     * Sets the value of the reAnalise property.
     * 
     */
    public void setReAnalise(boolean value) {
        this.reAnalise = value;
    }

    /**
     * Gets the value of the pessoa property.
     * 
     * @return
     *     possible object is
     *     {@link WsPessoaTroca }
     *     
     */
    public WsPessoaTroca getPessoa() {
        return pessoa;
    }

    /**
     * Sets the value of the pessoa property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPessoaTroca }
     *     
     */
    public void setPessoa(WsPessoaTroca value) {
        this.pessoa = value;
    }

    /**
     * Gets the value of the produtosReclamados property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsProdutoTroca }
     *     
     */
    public ArrayOfWsProdutoTroca getProdutosReclamados() {
        return produtosReclamados;
    }

    /**
     * Sets the value of the produtosReclamados property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsProdutoTroca }
     *     
     */
    public void setProdutosReclamados(ArrayOfWsProdutoTroca value) {
        this.produtosReclamados = value;
    }

    /**
     * Gets the value of the ressarcimento property.
     * 
     * @return
     *     possible object is
     *     {@link WsRessarcimentoTroca }
     *     
     */
    public WsRessarcimentoTroca getRessarcimento() {
        return ressarcimento;
    }

    /**
     * Sets the value of the ressarcimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsRessarcimentoTroca }
     *     
     */
    public void setRessarcimento(WsRessarcimentoTroca value) {
        this.ressarcimento = value;
    }

    /**
     * Gets the value of the emHomologacao property.
     * 
     */
    public boolean isEmHomologacao() {
        return emHomologacao;
    }

    /**
     * Sets the value of the emHomologacao property.
     * 
     */
    public void setEmHomologacao(boolean value) {
        this.emHomologacao = value;
    }

}
