
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="criarUsuario3Result" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "criarUsuario3Result"
})
@XmlRootElement(name = "criarUsuario3Response")
public class CriarUsuario3Response {

    protected WsResultado criarUsuario3Result;

    /**
     * Gets the value of the criarUsuario3Result property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getCriarUsuario3Result() {
        return criarUsuario3Result;
    }

    /**
     * Sets the value of the criarUsuario3Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setCriarUsuario3Result(WsResultado value) {
        this.criarUsuario3Result = value;
    }

}
