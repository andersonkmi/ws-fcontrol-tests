
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsAnaliseTodasSublojas complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsAnaliseTodasSublojas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsAnaliseTodasSublojas" type="{http://tempuri.org/}WsAnaliseTodasSublojas" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsAnaliseTodasSublojas", propOrder = {
    "wsAnaliseTodasSublojas"
})
public class ArrayOfWsAnaliseTodasSublojas {

    @XmlElement(name = "WsAnaliseTodasSublojas", nillable = true)
    protected List<WsAnaliseTodasSublojas> wsAnaliseTodasSublojas;

    /**
     * Gets the value of the wsAnaliseTodasSublojas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsAnaliseTodasSublojas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsAnaliseTodasSublojas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsAnaliseTodasSublojas }
     * 
     * 
     */
    public List<WsAnaliseTodasSublojas> getWsAnaliseTodasSublojas() {
        if (wsAnaliseTodasSublojas == null) {
            wsAnaliseTodasSublojas = new ArrayList<WsAnaliseTodasSublojas>();
        }
        return this.wsAnaliseTodasSublojas;
    }

}
