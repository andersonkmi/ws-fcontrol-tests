
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsBilheteViagem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsBilheteViagem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsBilheteViagem" type="{http://tempuri.org/}WsBilheteViagem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsBilheteViagem", propOrder = {
    "wsBilheteViagem"
})
public class ArrayOfWsBilheteViagem {

    @XmlElement(name = "WsBilheteViagem", nillable = true)
    protected List<WsBilheteViagem> wsBilheteViagem;

    /**
     * Gets the value of the wsBilheteViagem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsBilheteViagem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsBilheteViagem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsBilheteViagem }
     * 
     * 
     */
    public List<WsBilheteViagem> getWsBilheteViagem() {
        if (wsBilheteViagem == null) {
            wsBilheteViagem = new ArrayList<WsBilheteViagem>();
        }
        return this.wsBilheteViagem;
    }

}
