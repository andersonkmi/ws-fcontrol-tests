
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsHistorico2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsHistorico2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsHistorico2" type="{http://tempuri.org/}WsHistorico2" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsHistorico2", propOrder = {
    "wsHistorico2"
})
public class ArrayOfWsHistorico2 {

    @XmlElement(name = "WsHistorico2", nillable = true)
    protected List<WsHistorico2> wsHistorico2;

    /**
     * Gets the value of the wsHistorico2 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsHistorico2 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsHistorico2().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsHistorico2 }
     * 
     * 
     */
    public List<WsHistorico2> getWsHistorico2() {
        if (wsHistorico2 == null) {
            wsHistorico2 = new ArrayList<WsHistorico2>();
        }
        return this.wsHistorico2;
    }

}
