
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsProduto4 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsProduto4">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsProduto4" type="{http://tempuri.org/}WsProduto4" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsProduto4", propOrder = {
    "wsProduto4"
})
public class ArrayOfWsProduto4 {

    @XmlElement(name = "WsProduto4", nillable = true)
    protected List<WsProduto4> wsProduto4;

    /**
     * Gets the value of the wsProduto4 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsProduto4 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsProduto4().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsProduto4 }
     * 
     * 
     */
    public List<WsProduto4> getWsProduto4() {
        if (wsProduto4 == null) {
            wsProduto4 = new ArrayList<WsProduto4>();
        }
        return this.wsProduto4;
    }

}
