
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsCartaoTelecom complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCartaoTelecom">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NomeBancoEmissor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroTentativasValidacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataValidadeCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NomeTitularCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCartaoTelecom", propOrder = {
    "nomeBancoEmissor",
    "numeroCartao",
    "numeroTentativasValidacao",
    "dataValidadeCartao",
    "nomeTitularCartao"
})
public class WsCartaoTelecom {

    @XmlElement(name = "NomeBancoEmissor")
    protected String nomeBancoEmissor;
    @XmlElement(name = "NumeroCartao")
    protected String numeroCartao;
    @XmlElement(name = "NumeroTentativasValidacao")
    protected int numeroTentativasValidacao;
    @XmlElement(name = "DataValidadeCartao")
    protected String dataValidadeCartao;
    @XmlElement(name = "NomeTitularCartao")
    protected String nomeTitularCartao;

    /**
     * Gets the value of the nomeBancoEmissor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeBancoEmissor() {
        return nomeBancoEmissor;
    }

    /**
     * Sets the value of the nomeBancoEmissor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeBancoEmissor(String value) {
        this.nomeBancoEmissor = value;
    }

    /**
     * Gets the value of the numeroCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCartao() {
        return numeroCartao;
    }

    /**
     * Sets the value of the numeroCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCartao(String value) {
        this.numeroCartao = value;
    }

    /**
     * Gets the value of the numeroTentativasValidacao property.
     * 
     */
    public int getNumeroTentativasValidacao() {
        return numeroTentativasValidacao;
    }

    /**
     * Sets the value of the numeroTentativasValidacao property.
     * 
     */
    public void setNumeroTentativasValidacao(int value) {
        this.numeroTentativasValidacao = value;
    }

    /**
     * Gets the value of the dataValidadeCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataValidadeCartao() {
        return dataValidadeCartao;
    }

    /**
     * Sets the value of the dataValidadeCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataValidadeCartao(String value) {
        this.dataValidadeCartao = value;
    }

    /**
     * Gets the value of the nomeTitularCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeTitularCartao() {
        return nomeTitularCartao;
    }

    /**
     * Sets the value of the nomeTitularCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeTitularCartao(String value) {
        this.nomeTitularCartao = value;
    }

}
