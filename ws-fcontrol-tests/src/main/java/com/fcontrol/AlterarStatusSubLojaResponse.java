
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="alterarStatusSubLojaResult" type="{http://tempuri.org/}WsResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "alterarStatusSubLojaResult"
})
@XmlRootElement(name = "alterarStatusSubLojaResponse")
public class AlterarStatusSubLojaResponse {

    protected WsResultado alterarStatusSubLojaResult;

    /**
     * Gets the value of the alterarStatusSubLojaResult property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultado }
     *     
     */
    public WsResultado getAlterarStatusSubLojaResult() {
        return alterarStatusSubLojaResult;
    }

    /**
     * Sets the value of the alterarStatusSubLojaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultado }
     *     
     */
    public void setAlterarStatusSubLojaResult(WsResultado value) {
        this.alterarStatusSubLojaResult = value;
    }

}
