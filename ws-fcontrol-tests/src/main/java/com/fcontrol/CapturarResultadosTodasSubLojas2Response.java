
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capturarResultadosTodasSubLojas2Result" type="{http://tempuri.org/}ArrayOfWsAnaliseTodasSublojas2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "capturarResultadosTodasSubLojas2Result"
})
@XmlRootElement(name = "capturarResultadosTodasSubLojas2Response")
public class CapturarResultadosTodasSubLojas2Response {

    protected ArrayOfWsAnaliseTodasSublojas2 capturarResultadosTodasSubLojas2Result;

    /**
     * Gets the value of the capturarResultadosTodasSubLojas2Result property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWsAnaliseTodasSublojas2 }
     *     
     */
    public ArrayOfWsAnaliseTodasSublojas2 getCapturarResultadosTodasSubLojas2Result() {
        return capturarResultadosTodasSubLojas2Result;
    }

    /**
     * Sets the value of the capturarResultadosTodasSubLojas2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWsAnaliseTodasSublojas2 }
     *     
     */
    public void setCapturarResultadosTodasSubLojas2Result(ArrayOfWsAnaliseTodasSublojas2 value) {
        this.capturarResultadosTodasSubLojas2Result = value;
    }

}
