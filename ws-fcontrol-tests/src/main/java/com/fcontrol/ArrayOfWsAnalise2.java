
package com.fcontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWsAnalise2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWsAnalise2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WsAnalise2" type="{http://tempuri.org/}WsAnalise2" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWsAnalise2", propOrder = {
    "wsAnalise2"
})
public class ArrayOfWsAnalise2 {

    @XmlElement(name = "WsAnalise2", nillable = true)
    protected List<WsAnalise2> wsAnalise2;

    /**
     * Gets the value of the wsAnalise2 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsAnalise2 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWsAnalise2().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsAnalise2 }
     * 
     * 
     */
    public List<WsAnalise2> getWsAnalise2() {
        if (wsAnalise2 == null) {
            wsAnalise2 = new ArrayList<WsAnalise2>();
        }
        return this.wsAnalise2;
    }

}
