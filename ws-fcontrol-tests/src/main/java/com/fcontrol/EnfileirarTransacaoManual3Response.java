
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enfileirarTransacaoManual3Result" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="mensagemRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enfileirarTransacaoManual3Result",
    "mensagemRetorno"
})
@XmlRootElement(name = "enfileirarTransacaoManual3Response")
public class EnfileirarTransacaoManual3Response {

    protected boolean enfileirarTransacaoManual3Result;
    protected String mensagemRetorno;

    /**
     * Gets the value of the enfileirarTransacaoManual3Result property.
     * 
     */
    public boolean isEnfileirarTransacaoManual3Result() {
        return enfileirarTransacaoManual3Result;
    }

    /**
     * Sets the value of the enfileirarTransacaoManual3Result property.
     * 
     */
    public void setEnfileirarTransacaoManual3Result(boolean value) {
        this.enfileirarTransacaoManual3Result = value;
    }

    /**
     * Gets the value of the mensagemRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagemRetorno() {
        return mensagemRetorno;
    }

    /**
     * Sets the value of the mensagemRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagemRetorno(String value) {
        this.mensagemRetorno = value;
    }

}
