
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="analisarTransacaoResult" type="{http://tempuri.org/}WsResultadoAnalise" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarTransacaoResult"
})
@XmlRootElement(name = "analisarTransacaoResponse")
public class AnalisarTransacaoResponse {

    protected WsResultadoAnalise analisarTransacaoResult;

    /**
     * Gets the value of the analisarTransacaoResult property.
     * 
     * @return
     *     possible object is
     *     {@link WsResultadoAnalise }
     *     
     */
    public WsResultadoAnalise getAnalisarTransacaoResult() {
        return analisarTransacaoResult;
    }

    /**
     * Sets the value of the analisarTransacaoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsResultadoAnalise }
     *     
     */
    public void setAnalisarTransacaoResult(WsResultadoAnalise value) {
        this.analisarTransacaoResult = value;
    }

}
