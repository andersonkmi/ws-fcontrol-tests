
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WsCartaoViagem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCartaoViagem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NomeBancoEmissor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataValidadeCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NomeTitularCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CpfTitularCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoBinCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuatroUltimosDigitosCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnderecoFaturaCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CepFaturaCartao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataNascimentoTitular" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="EmailTitular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DddTelefoneTitular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroTelefoneTitular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DddCelularTitular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroCelularTitular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCartaoViagem", propOrder = {
    "nomeBancoEmissor",
    "numeroCartao",
    "dataValidadeCartao",
    "nomeTitularCartao",
    "cpfTitularCartao",
    "codigoBinCartao",
    "quatroUltimosDigitosCartao",
    "enderecoFaturaCartao",
    "cepFaturaCartao",
    "dataNascimentoTitular",
    "emailTitular",
    "dddTelefoneTitular",
    "numeroTelefoneTitular",
    "dddCelularTitular",
    "numeroCelularTitular"
})
public class WsCartaoViagem {

    @XmlElement(name = "NomeBancoEmissor")
    protected String nomeBancoEmissor;
    @XmlElement(name = "NumeroCartao")
    protected String numeroCartao;
    @XmlElement(name = "DataValidadeCartao")
    protected String dataValidadeCartao;
    @XmlElement(name = "NomeTitularCartao")
    protected String nomeTitularCartao;
    @XmlElement(name = "CpfTitularCartao")
    protected String cpfTitularCartao;
    @XmlElement(name = "CodigoBinCartao")
    protected String codigoBinCartao;
    @XmlElement(name = "QuatroUltimosDigitosCartao")
    protected String quatroUltimosDigitosCartao;
    @XmlElement(name = "EnderecoFaturaCartao")
    protected String enderecoFaturaCartao;
    @XmlElement(name = "CepFaturaCartao")
    protected String cepFaturaCartao;
    @XmlElement(name = "DataNascimentoTitular", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataNascimentoTitular;
    @XmlElement(name = "EmailTitular")
    protected String emailTitular;
    @XmlElement(name = "DddTelefoneTitular")
    protected String dddTelefoneTitular;
    @XmlElement(name = "NumeroTelefoneTitular")
    protected String numeroTelefoneTitular;
    @XmlElement(name = "DddCelularTitular")
    protected String dddCelularTitular;
    @XmlElement(name = "NumeroCelularTitular")
    protected String numeroCelularTitular;

    /**
     * Gets the value of the nomeBancoEmissor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeBancoEmissor() {
        return nomeBancoEmissor;
    }

    /**
     * Sets the value of the nomeBancoEmissor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeBancoEmissor(String value) {
        this.nomeBancoEmissor = value;
    }

    /**
     * Gets the value of the numeroCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCartao() {
        return numeroCartao;
    }

    /**
     * Sets the value of the numeroCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCartao(String value) {
        this.numeroCartao = value;
    }

    /**
     * Gets the value of the dataValidadeCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataValidadeCartao() {
        return dataValidadeCartao;
    }

    /**
     * Sets the value of the dataValidadeCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataValidadeCartao(String value) {
        this.dataValidadeCartao = value;
    }

    /**
     * Gets the value of the nomeTitularCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeTitularCartao() {
        return nomeTitularCartao;
    }

    /**
     * Sets the value of the nomeTitularCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeTitularCartao(String value) {
        this.nomeTitularCartao = value;
    }

    /**
     * Gets the value of the cpfTitularCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfTitularCartao() {
        return cpfTitularCartao;
    }

    /**
     * Sets the value of the cpfTitularCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfTitularCartao(String value) {
        this.cpfTitularCartao = value;
    }

    /**
     * Gets the value of the codigoBinCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoBinCartao() {
        return codigoBinCartao;
    }

    /**
     * Sets the value of the codigoBinCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoBinCartao(String value) {
        this.codigoBinCartao = value;
    }

    /**
     * Gets the value of the quatroUltimosDigitosCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuatroUltimosDigitosCartao() {
        return quatroUltimosDigitosCartao;
    }

    /**
     * Sets the value of the quatroUltimosDigitosCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuatroUltimosDigitosCartao(String value) {
        this.quatroUltimosDigitosCartao = value;
    }

    /**
     * Gets the value of the enderecoFaturaCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnderecoFaturaCartao() {
        return enderecoFaturaCartao;
    }

    /**
     * Sets the value of the enderecoFaturaCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnderecoFaturaCartao(String value) {
        this.enderecoFaturaCartao = value;
    }

    /**
     * Gets the value of the cepFaturaCartao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCepFaturaCartao() {
        return cepFaturaCartao;
    }

    /**
     * Sets the value of the cepFaturaCartao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCepFaturaCartao(String value) {
        this.cepFaturaCartao = value;
    }

    /**
     * Gets the value of the dataNascimentoTitular property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataNascimentoTitular() {
        return dataNascimentoTitular;
    }

    /**
     * Sets the value of the dataNascimentoTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataNascimentoTitular(XMLGregorianCalendar value) {
        this.dataNascimentoTitular = value;
    }

    /**
     * Gets the value of the emailTitular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailTitular() {
        return emailTitular;
    }

    /**
     * Sets the value of the emailTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailTitular(String value) {
        this.emailTitular = value;
    }

    /**
     * Gets the value of the dddTelefoneTitular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddTelefoneTitular() {
        return dddTelefoneTitular;
    }

    /**
     * Sets the value of the dddTelefoneTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddTelefoneTitular(String value) {
        this.dddTelefoneTitular = value;
    }

    /**
     * Gets the value of the numeroTelefoneTitular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTelefoneTitular() {
        return numeroTelefoneTitular;
    }

    /**
     * Sets the value of the numeroTelefoneTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTelefoneTitular(String value) {
        this.numeroTelefoneTitular = value;
    }

    /**
     * Gets the value of the dddCelularTitular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDddCelularTitular() {
        return dddCelularTitular;
    }

    /**
     * Sets the value of the dddCelularTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDddCelularTitular(String value) {
        this.dddCelularTitular = value;
    }

    /**
     * Gets the value of the numeroCelularTitular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCelularTitular() {
        return numeroCelularTitular;
    }

    /**
     * Sets the value of the numeroCelularTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCelularTitular(String value) {
        this.numeroCelularTitular = value;
    }

}
