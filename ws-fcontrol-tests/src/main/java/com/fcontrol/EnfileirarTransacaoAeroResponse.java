
package com.fcontrol;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enfileirarTransacaoAeroResult" type="{http://tempuri.org/}WsAeroResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enfileirarTransacaoAeroResult"
})
@XmlRootElement(name = "enfileirarTransacaoAeroResponse")
public class EnfileirarTransacaoAeroResponse {

    protected WsAeroResultado enfileirarTransacaoAeroResult;

    /**
     * Gets the value of the enfileirarTransacaoAeroResult property.
     * 
     * @return
     *     possible object is
     *     {@link WsAeroResultado }
     *     
     */
    public WsAeroResultado getEnfileirarTransacaoAeroResult() {
        return enfileirarTransacaoAeroResult;
    }

    /**
     * Sets the value of the enfileirarTransacaoAeroResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAeroResultado }
     *     
     */
    public void setEnfileirarTransacaoAeroResult(WsAeroResultado value) {
        this.enfileirarTransacaoAeroResult = value;
    }

}
